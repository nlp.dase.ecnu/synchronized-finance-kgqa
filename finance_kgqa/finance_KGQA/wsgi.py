"""
WSGI config for finance_KGQA project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "finance_KGQA.settings")

application = get_wsgi_application()

from kgqa.src.dict import *
from kgqa.src.graph import *
# 初始化字典加载
init_dicts()
# 连接数据库
connect_graph()

from kgqa.src.preload import *
preload_modules()