"""
URL configuration for finance_KGQA project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from kgqa import views

urlpatterns = [
    path("integration_query", views.integration_query),
    path("query", views.query),
    path("llm_query", views.llm_query),
    path("update", views.update),
    path("insert", views.insert),
    path("test", views.test),
    path("cyphers", views.cyphers),
    path("examples", views.get_examples),
    path("routine", views.routine),
    path("quarter_routine", views.quarter_routine),
    path("random", views.random_update_test),
]
