# finance_KGQA

## Preparation

### System configuration

Before reproducing this repository, please check whether your environment configuration is up to standard.
* python >= 3.9 (standard), python >= 3.11 (recommend)

### Dependency

To initialize the cold-start system, you should install all dependencies by running
```
pip install -r requirements.txt
```

### Download huggingface pretrained model

Due to the file size limitation of GitLab, the weight of a pretrained model is not uploaded. You should manually download the 'pytorch_model.bin' file from [here](https://huggingface.co/google-bert/bert-base-chinese/tree/main) and place it at kgqa/resources/bert-base-chinese directory.

### Install knowledge graph: Nebula Graph (v3.5.0)

This repository requires intallation of Nebula Graph, and 'cql' in 'text-to-cql' comply with the syntax rules of Open Cypher in Nebula Graph (nebula open cypher), which is slightly different with original Open Cypher (e.g. cyphers in Neo4j).

To acquisite the grammar of nebula open cypher, you can access their [official website](https://docs.nebula-graph.com.cn/3.5.0/). To install nebula graph, follow the instruction in [this page](https://docs.nebula-graph.com.cn/3.5.0/2.quick-start/3.quick-start-on-premise/2.install-nebula-graph/).

* Make sure the version of nebula graph that you've downloaded is 3.5.0. (Perhaps a higher version is also compatible?)

### Initialize nebula graph with random-generated data

This section will instruct you building an accessible knowledge graph in nebula.

First, you can download data from [here(updated later)](https://www.baidu.com).

Next, you need to install nebula-importer, an official tool which can build knowledge graph fron csv files. The installation is tedious, be patient and follow the instruction in [here](https://docs.nebula-graph.com.cn/3.5.0/nebula-importer/use-importer/). You might need an appropriate Go language environment.

### Configuration

This is the last preparation before getting start. Remember the ip of your host where you installed the nebula graph, and modify kgqa/resources/config/nebula.json.

```
{
  "ip": "127.0.0.1",   # replace this ip with your host's ip
  "port": 9669,
  "username": "root",
  "password": "nebula",
  "spacename": "stockKGQA_TEST"
}
```

## Getting Start

Once all the preparations are complete, we are ready to start the system!

Run this command, and after about a minute, the system backend will start at 127.0.0.1:8001. To access fincance kgqa, use the api "test", namely visit 127.0.0.1:8001/test and you will see the interface of our system!
```
sh run_server.sh
```

Now you can use the 'heuristic' or '启发式' option on the interactive page and ask questions to the system! 

Note that the other two options will be unavailable until you deploy LLM backend.

## Getting start with LLM backend

If you just finish installing LLM system, you will need following the procedures below to start LLM service.
* Modify kgqa/resources/config/system_config.py.
```
# 大模型 cypher 意图与问题的相似度阈值
sim_threshold_llm = 0.6

# 启发式 cypher 意图与问题的相似度阈值
sim_threshold_heuristic = 0.7

# 大模型服务器 url
llm_server_url = 'http://127.0.0.1:8008'
```








