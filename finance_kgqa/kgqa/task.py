import time
import math
import random
from datetime import datetime, date, timedelta
from workalendar.asia import China

from kgqa.src.graph import execute_cypher

cal = China()

def daily_initialize(dst_date = None, src_date = None):
    """
    结合linux crontab命令，于每日9:00实现每日的股票、行业新节点插入，以及10年前的旧节点删除
    :param
        dst_data(datetime.date):
            要创建的新节点的日期，默认为系统时的今天，如果该日期不是交易日，则不会更新任何内容
        src_date(datetime.date):
            创建新节点时，根据上一个交易日src_date的数据对新节点的数值进行初始化。默认值为执行日期的上一日
    :return
        executing_time(float)
            返回新节点插入，旧节点删除所消耗的时间
        
    """
    if dst_date is None:
        dst_date = date.today()
    try:
        if not cal.is_working_day(dst_date):
            return "Update nothing, date-to-update is not a working day."
    except:
        pass
    if src_date is None:
        src_date = dst_date - timedelta(days = 1)
        try:
            while not cal.is_working_day(src_date):
                src_date = src_date - timedelta(days = 1)
        except:
            pass
    st = time.time()
    print("Executing daily insertion and deletion executing time at", datetime.now())
    for shortcut, tag in [("s", "stock"), ("t", "trade")]:
        # 首先查询当天所有数据
        base_cql = f"""MATCH ({shortcut}:{tag})-[h{shortcut}d:has_{tag}_data]->({shortcut}d:{tag}_data)
        WHERE {shortcut}d.{tag}_data.`date` == date('{str(src_date)}')
        RETURN id({shortcut}), properties({shortcut}d)"""
        date_to_insert = dst_date
        date_to_delete = dst_date - timedelta(days = 3650)

        result = execute_cypher("insert_query", base_cql)

        codes, properties_list = result[f"id({shortcut})"], result[f"properties({shortcut}d)"]

        keys = [key if key != "date" else "`date`" for key in properties_list[0].keys()]
        keys = ", ".join(keys)
        insert_vex_cql = f"INSERT VERTEX IF NOT EXISTS {tag}_data ({keys}) VALUES "
        insert_edge_cql = f"INSERT EDGE IF NOT EXISTS has_{tag}_data () VALUES "
        delete_cql = "DELETE VERTEX " 

        for code, properties in zip(codes, properties_list):
            # 插入新节点
            data_vid = "-".join([code, str(date_to_insert)])
            # 修正日期属性
            values = [str(value) if key != "date" else f"date('{str(date_to_insert)}')"
                       for key, value  in properties.items()]
            values = ", ".join(values)
            insert_vex_cql += f"\"{data_vid}\": ({values}),"
            # 创建新节点关联的边
            insert_edge_cql += f"\"{code}\" -> \"{data_vid}\":(),"
            # 删除旧节点以及相关联的边
            data_vid = "-".join([code, str(date_to_delete)])
            delete_cql += f"\"{data_vid}\","
        insert_vex_cql = insert_vex_cql.strip(",") + ";"
        insert_edge_cql = insert_edge_cql.strip(",") + ";"
        delete_cql = delete_cql.strip(",") + " WITH EDGE;"
        execute_cypher("insert", insert_vex_cql)
        execute_cypher("insert", insert_edge_cql)
        execute_cypher("delete", delete_cql)

    executing_time = time.time() - st
    print("Daily insertion and deletion executing time: ", executing_time)
    return executing_time

def quarterly_initialize(current_date = None):
    """
    结合linux crontab命令，于1，4，7，10月的1日00:00实现股票季度财务信息的新节点插入，以及10年前的旧节点删除
    :param
        src_date(datetime.date):
            新的季度的第一个日期，只能是(1-1,4-1,7-1,10-1)其中之一，根据该日期创建上一季度的财务信息，并删除十年前该季度对应的节点。默认值为当前日期
    :return
        executing_time(float)
            返回新节点插入，旧节点删除所消耗的时间
    :raise
        ValueError: 
            current date 不在 (1/1, 4/1, 7/1, 10/1) 之中
        
    """
    
    current_date = current_date if current_date is not None else date.today()
    if not (current_date.month % 3 == 1 and current_date.day == 1):
        print(f"Quarterly initialization should be applied on the first day of a new quarter, including (1/1, 4/1, 7/1, 10/1), but today is {current_date}")
        return 0
    # 获取上上一季度所有股票的财务信息，以创建上一季度的财务信息
    base_cql = f"""WITH CASE WHEN date('{str(current_date)}').month < 7 THEN (date('{str(current_date)}').month-1)/3+3 ELSE (date('{str(current_date)}').month-1)/3-1 END AS last_quarter,
    CASE WHEN date('{str(current_date)}').month < 7 THEN date('{str(current_date)}').year - 1 ELSE date('{str(current_date)}').year END AS last_year
    MATCH (s:stock)-[hsfi:has_stock_financial_information]->(sfi:stock_financial_information)
    WHERE sfi.stock_financial_information.quarter == last_quarter
    AND sfi.stock_financial_information.year == last_year
    RETURN id(s), properties(sfi)"""
    st = time.time()
    print("Executing quarterly insertion and deletion executing time at", datetime.now())

    result = execute_cypher("insert_query", base_cql)
    codes, properties_list = result["id(s)"], result["properties(sfi)"]

    keys = [key if key != "date" else "`date`" for key in properties_list[0].keys()]
    keys = ", ".join(keys)
    new_value = {
        "year": current_date.year - 1 if current_date.month < 4 else current_date.year,
        "quarter": 4 if current_date.month < 4 else (current_date.month-1) // 3,
        "half_year": 1 if current_date.month < 7 else 2,
    }
    new_value["date"] = f'date(\'{datetime(year = new_value["year"], month=new_value["quarter"] * 3 - 2, day=1).date()}\')'
    insert_vex_cql = f"INSERT VERTEX IF NOT EXISTS stock_financial_information ({keys}) VALUES "
    insert_edge_cql = f"INSERT EDGE IF NOT EXISTS has_stock_financial_information () VALUES "
    delete_cql = "DELETE VERTEX "

    for code, properties in zip(codes, properties_list):
        # 插入新节点
        data_vid = code + str(new_value["year"]) + "-" + str(new_value["quarter"])
        # 修正时间相关属性属性
        
        values = [str(value) if key not in new_value else str(new_value[key])
                    for key, value in properties.items()]
        values = ", ".join(values)
        insert_vex_cql += f"\"{data_vid}\": ({values}),"
        # 创建新节点关联的边
        insert_edge_cql += f"\"{code}\" -> \"{data_vid}\":(),"
        # 删除旧节点以及相关联的边
        data_vid = code + str(new_value["year"] - 10) + "-" + str(new_value["quarter"])
        delete_cql += f"\"{data_vid}\","

    insert_vex_cql = insert_vex_cql.strip(",") + ";"
    insert_edge_cql = insert_edge_cql.strip(",") + ";"
    delete_cql = delete_cql.strip(",") + " WITH EDGE;"
    print(insert_vex_cql[:1000])
    execute_cypher("insert", insert_vex_cql)
    execute_cypher("insert", insert_edge_cql)
    execute_cypher("delete", delete_cql)

    executing_time = time.time() - st
    print("Quarterly insertion and deletion executing time: ", executing_time)
    return executing_time

def random_update(names = None, max_allowed_cyphers = 500, bias = 1e-4):
    """
    测试时，用于对数据库全体股票数据stock_data信息进行随机更新的接口
    :param
        names(List[str]):
            指定需要更新的股票名称列表。如果为空，则对所有股票数据进行随机更新
        max_allowed_cyphers(int):
            一次在nebula graph里可以执行的最大语句数目。由于nebula graph不支持批量更新，需要限制单次最大执行数量。
        bias(float):
            用于更新股票数据的随机因子。更新时，会将股票的收盘价closing_price乘以均匀分布 U(1-bias,1+bias) 中的一个值，并对应地更新涨跌幅、最高价、最低价
    :return
        executing_time(float)
            返回随机更新消耗的时间
        
    """
    from kgqa.views import reviewer
    start_time = time.time()

    if names:
        base_cql = f"""MATCH (s:stock)-[hsd:has_stock_data]->(sd:stock_data)
        WHERE s.stock.name in {str(names)}
        AND sd.stock_data.`date` == date()
        RETURN id(sd), properties(sd)"""
    else:
        base_cql = f"""MATCH (sd:stock_data)
        WHERE sd.stock_data.`date` == date()
        RETURN id(sd), properties(sd)"""

    result = execute_cypher("insert_query", reviewer.date_wrapper(base_cql))
    codes, properties_list = result["id(sd)"], result["properties(sd)"]
    cypher_statements = []
    # print("请求：", args)
    for code, properties in zip(codes, properties_list):
        
        opening_price = float(properties["opening_price"].__repr__())
        closing_price = float(properties["closing_price"].__repr__())
        closing_price *= random.uniform(1 - bias, 1 + bias)

        maximum_price = max(float(properties["maximum_price"].__repr__()), closing_price)
        minimum_price = min(float(properties["minimum_price"].__repr__()), closing_price)
        range_ = 100 * (closing_price - opening_price) / opening_price
        raising_limit = range_ > 10

        cypher_statement = f"""UPDATE VERTEX ON stock_data \"{code}\" SET 
        closing_price = {closing_price},
        maximum_price = {maximum_price},
        minimum_price = {minimum_price},
        range = {range_},
        raising_limit = {raising_limit}
        YIELD
        closing_price,
        maximum_price,
        minimum_price,
        range,
        raising_limit
        """
        cypher_statements += [cypher_statement]
        if len(cypher_statements) >= max_allowed_cyphers:
            execute_cypher("update", ";".join(cypher_statements))
            cypher_statements = []
    if cypher_statements:
        execute_cypher("update", ";".join(cypher_statements))

    executing_time = time.time() - start_time
    # print(result)

    return executing_time
    # return HttpResponse(result, content_type="Application/Json")