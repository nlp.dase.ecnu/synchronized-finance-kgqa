from django.apps import AppConfig

class KgqaConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "kgqa"
