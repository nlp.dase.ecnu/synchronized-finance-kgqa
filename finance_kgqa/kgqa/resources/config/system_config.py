# 大模型 cypher 意图与问题的相似度阈值
sim_threshold_llm = 0.6

# 启发式 cypher 意图与问题的相似度阈值
sim_threshold_heuristic = 0.7

# 大模型服务器 url
llm_server_url = 'http://127.0.0.1:8008'