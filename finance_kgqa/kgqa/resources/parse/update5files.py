"""
该文件用于对解析中会用到的五个json文件进行生成/更新
    entities.json
        用于match步骤找到实体，包含match_special_keywords.json中的特殊关键词
    entities_map.json
        entities的反向，用于match步骤找到实体
    where_entities.json
        用于where步骤找到实体
    where_entities_map.json
        用于模糊匹配、别名、股票代码等到股票名称的匹配
    user_dict.txt
        用于jieba分词
"""

import csv
import pandas as pd
import json

EN2CH = {
    "stock": "股票",
    "chairman": "董事长",
    "stockholder": "股东",
    "trade": "行业",
    "public_offering_fund": "公募基金",
    "fund_manager": "基金经理人",
    "industry": "产业"
}


def add_suffix(dic, suffix, special_keywords_list=[]):
    """
    输入dic，将dic中对应suffix的所有value，添加suffix后缀。
    例如：行业: 能源, 金融服务 -> 行业: 能源, 金融服务, 能源行业, 金融服务行业
    :param dic: 输入的字典
    :param suffix: 想要加后缀的key名称
    :param special_keywords_list: 不需要添加后缀的特殊词
    :return:
        dic: 处理好后的dic
    """
    for key, value in dic.items():
        if key == suffix:
            temp = [k + suffix for k in value if k not in special_keywords_list]
            value += temp
    return dic


def update_where_entities(new_file_path, new_path, update_or_append='append', old_path=None):
    """
    根据实体字典，生成 where_entities.json 文件
    :param new_file_path: 实体json文件路径，存有各实体类别的实体名称
    :param new_path: 写入的json文件路径（where_entities.json）
    :param update_or_append: 更新或新增
    :param old_path: 新增需要提供原有json文件路径
    :return:
        None
    """
    with open(new_file_path, "r", encoding="utf-8") as pf:
        dic_new = json.load(pf)

    if update_or_append == 'append':
        assert old_path is not None
        with open(old_path, "r", encoding="utf-8") as pf:
            entities_dic = json.load(pf)
    elif update_or_append == 'update':
        entities_dic = {}
        for i in EN2CH.values():
            entities_dic[i] = []
    else:
        print('error')

    for i, j in EN2CH.items():
        for k in dic_new[i]:
            if k not in entities_dic[j]:
                entities_dic[j].append(k)
    entities_dic = add_suffix(entities_dic, '行业')
    entities_dic = add_suffix(entities_dic, '产业')

    with open(new_path, 'w', encoding='utf-8') as json_file:
        json.dump(entities_dic, json_file, ensure_ascii=False)


def update_entities(new_file_path, special_keywords_path, new_path, update_or_append='append', old_path=None):
    """
    根据实体字典+各实体类型的special_keywords，生成 entities.json 文件
    :param new_file_path: 实体json文件路径，存有各实体类别的实体名称
    :param special_keywords_path: 每个实体类别的特殊匹配词json文件路径，例如：资讯": ["资讯", "消息", "讯息", "新闻", "公告", ...]
    :param new_path: 写入的json文件路径（entities.json）
    :param update_or_append: 更新或新增
    :param old_path: 新增需要提供原有json文件路径
    :return:
        None
    """
    with open(new_file_path, "r", encoding="utf-8") as pf:
        dic_new = json.load(pf)

    with open(special_keywords_path, "r", encoding="utf-8") as pf:
        special_keywords_dic = json.load(pf)
        special_keywords_list = [item for sublist in special_keywords_dic.values() for item in sublist]

    if update_or_append == 'append':
        assert old_path is not None
        with open(old_path, "r", encoding="utf-8") as pf:
            entities_dic = json.load(pf)
    elif update_or_append == 'update':
        entities_dic = special_keywords_dic
    else:
        print('error')

    for i, j in EN2CH.items():
        for k in dic_new[i]:
            if k not in entities_dic[j]:
                entities_dic[j].append(k)

    entities_dic = add_suffix(entities_dic, '行业', special_keywords_list)
    entities_dic = add_suffix(entities_dic, '产业', special_keywords_list)

    with open(new_path, 'w', encoding='utf-8') as json_file:
        json.dump(entities_dic, json_file, ensure_ascii=False)


def update_entities_map(entities_path, entities_map_path):
    """
    由 entities.json 文件，生成相应的 entities_map.json 文件
    :param entities_path: entities.json 文件路径
    :param entities_map_path: entities_map.json 文件路径
    :return:
        None
    """
    with open(entities_path, 'r', encoding='utf-8') as json_file:
        original_dict = json.load(json_file)
    inverted_dict = {}
    for category in original_dict.keys():
        for i in original_dict[category]:
            inverted_dict[i] = category
    with open(entities_map_path, 'w', encoding='utf-8') as file:
        json.dump(inverted_dict, file, ensure_ascii=False)


def update_where_entities_map(where_entities_path, where_entities_map_path):
    """
    由 where_entities.json 文件，生成相应的 where_entities_map.json 文件
    :param where_entities_path: where_entities.json 文件路径
    :param where_entities_map_path: where_entities_map.json 文件路径
    :return:
    """
    with open(where_entities_path, 'r', encoding='utf-8') as json_file:
        original_dict = json.load(json_file)
    inverted_dict = {}
    for category in original_dict.keys():
        for i in original_dict[category]:
            inverted_dict[i] = i
    with open(where_entities_map_path, 'w', encoding='utf-8') as file:
        json.dump(inverted_dict, file, ensure_ascii=False)


def update_user_dict(new_file_path, new_path, update_or_append='append', old_path=None):
    """
    将所有的实体名更新到user_dict.txt，对其中的所有实体名按照字符串从长到短排序。
    :param new_file_path: 实体名json文件
    :param new_path: 写入的user_dict.txt路径
    :param update_or_append: 更新或新增
    :param old_path: 新增需要提供原有json文件路径
    :return:
        None
    """
    with open(new_file_path, "r", encoding="utf-8") as pf:
        dic_new = json.load(pf)

    if update_or_append == 'append':
        assert old_path is not None
        with open(old_path, "r", encoding="utf-8") as f:
            words = [line.strip() for line in f.readlines()]
    elif update_or_append == 'update':
        words = []

    for k, v in dic_new.items():
        for i in v:
            if i not in words:
                words.append(i)

    words = sorted(words, key=len, reverse=True)
    with open(new_path, "w", encoding="utf-8") as f:
        for word in words:
            f.write(word + "\n")


def update_user_dict_from_entities(entities_path, properties_path, new_path):
    """
    从 entities.json 文件和 properties.json 文件更新 user_dict.txt
    :param entities_path: entities.json 的文件路径
    :param properties_path: properties.json 的文件路径
    :param new_path: 写入的 user_dict.txt 文件路径
    :return:
        None
    """
    with open(entities_path, "r", encoding="utf-8") as pf:
        dic_new = json.load(pf)
    with open(properties_path, "r", encoding="utf-8") as pf:
        dic_new2 = json.load(pf)
    words = []
    for k, v in dic_new.items():
        for i in v:
            if i not in words:
                words.append(i.upper())
    for k, v in dic_new2.items():
        for i in v:
            if i not in words:
                words.append(i.upper())

    # 对列表中的所有字符串，按照从长到短排序
    words = sorted(words, key=len, reverse=True)
    with open(new_path, "w", encoding="utf-8") as f:
        for word in words:
            f.write(word + "\n")


def add_stockNameCode_to_dict(path):
    """
    给当前json文件添加股票代码
    :param path: 输入文件路径
    :return:
        None
    """
    extend_data = []
    with open('stock.csv', 'r', encoding="utf-8") as file:
        # 创建 CSV 读取器
        csv_reader = csv.reader(file)
        for row in csv_reader:
            extend_data.append(row[1].upper())
            extend_data.append(row[1].lower())
            extend_data.append(row[1][2:])
            extend_data.append(row[2].upper())
            extend_data.append(row[2].upper())
    with open(path, 'r', encoding="utf-8") as file:
        data = json.load(file)
    for d in extend_data:
        if d not in data["股票"]:
            data["股票"].append(d)
    data["股票"] = list(set(data["股票"]))
    print(data)
    with open(path, 'w', encoding="utf-8") as file:
        json.dump(data, file, ensure_ascii=False)


def add_stockNameCode_to_map_dict(path, type):
    """
    给当前map文件添加股票代码
    :param path: 输入文件路径
    :param type: "entities_map" or "where_entities_map"
    :return:
        None
    """
    extend_data = {}
    with open('stock.csv', 'r', encoding="utf-8") as file:
        # 创建 CSV 读取器
        csv_reader = csv.reader(file)
        for row in csv_reader:
            if type == "entities_map":
                extend_data[row[1][2:]] = "股票"
                extend_data[row[1].upper()] = "股票"
                extend_data[row[1].lower()] = "股票"
            elif type == "where_entities_map":
                extend_data[row[1][2:]] = row[2].upper()
                extend_data[row[1].upper()] = row[2].upper()
                extend_data[row[1].lower()] = row[2].upper()

    with open(path, 'r', encoding="utf-8") as file:
        data = json.load(file)
    for k, v in extend_data.items():
        data[k] = v
    print(data)
    with open(path, 'w', encoding="utf-8") as file:
        json.dump(data, file, ensure_ascii=False)


if __name__ == '__main__':
    update_where_entities(new_file_path='en.json', new_path='where_entities.json', update_or_append='update')
    update_entities(new_file_path='en.json', new_path='entities.json', update_or_append='update',
                    special_keywords_path='match_special_keywords.json')
    update_user_dict(new_file_path='en.json', new_path='user_dict.txt', update_or_append='update')
    update_entities_map(entities_path='entities.json', entities_map_path='entities_map.json')
    update_where_entities_map(where_entities_path='where_entities.json',
                              where_entities_map_path='where_entities_map.json')

    update_user_dict_from_entities('entities.json', 'properties.json', 'user_dict.txt')

    add_stockNameCode_to_dict('entities.json')
    add_stockNameCode_to_map_dict('entities_map.json', "entities_map")
    add_stockNameCode_to_dict('where_entities.json')
    add_stockNameCode_to_map_dict('where_entities_map.json', "where_entities_map")
