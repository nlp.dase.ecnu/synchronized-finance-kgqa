## 模型参数下载地址
该模块用于使用bert-semantic-similarity进行生成cypher语句列表的排序；

如需使用该模块，请下载bert-base-multilingual-uncased模型参数文件：
pytorch_model.bin

https://huggingface.co/bert-base-multilingual-uncased/blob/main/pytorch_model.bin
