"""
本文件存放 Django 接口调用函数。包括集成系统查询、启发式系统查询、大模型系统查询、数据更新、数据插入等
"""

import random
import threading
import time
import json
import tqdm
import datetime
import traceback

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
import requests

from kgqa.task import daily_initialize, random_update, quarterly_initialize
from kgqa.src.parser import Parser
from kgqa.src.dict import *
from kgqa.src.graph import *
from kgqa.src.preload import get_parser, get_llm_parser, get_intent_query_similarity_model, get_query_preprocess_model
from kgqa.src.update_json_files import update_json
from kgqa.src.utils.review import SelfReviewer, get_today
from kgqa.src.utils.query_cypher_postprocess import QueryCypherPostprocessModel
from kgqa.resources.config.system_config import sim_threshold_heuristic, sim_threshold_llm, llm_server_url

reviewer = SelfReviewer()
query_cypher_postprocess_model = QueryCypherPostprocessModel()

# 使用的nebula graph里的空间名称Space Name, 在此处修改名称以保持与nebula graph一致
# prefix = "USE stockKGQA_TEST;"


def integration_query(request):
    """
    综合大模型和启发式算法，根据用户的问题实现知识图谱问答。
    流程：计算大模型生成的 cypher 意图和用户问题的相似度，高于阈值则返回，否则计算启发式算法生成的 cypher 意图和用户问题的相似度，高于阈值则返回，否则根据抽取出的实体查询，若没有实体则返回默认的保底 cypher
    :param:
        request (request):
            请求数据，用于获取用户问题文本。
            example: request.args.get("query")
    :return:
        answer (json):
            问题的答案。
            example: {"content": {"name": "国泰君安", "closing_price": "1.45631", ...}}
    """
    start_time = time.time()
    args = json.loads(request.body)
    print("请求：", args)
    query = args["query"]
    intent_query_similarity_model = get_intent_query_similarity_model()
    llm_parser = get_llm_parser()
    parser = get_parser()
    query_preprocess_model = get_query_preprocess_model()
    query = query_preprocess_model.preprocess(query)  # 对于query的预处理步骤（中文数字转换，日期转换）
    llm_time = 0
    heuristic_time = 0
    entity_query_time = 0
    default_query_time = 0
    llm_sim = 0
    heuristic_sim = 0

    target_cypher, res, intent, llm_sim, llm_time, parsing_time, executing_time = llm_query_for_integration(query, llm_parser,
                                                                                                            intent_query_similarity_model, sim_threshold_llm=sim_threshold_llm)
    result_source = "llm"
    if not target_cypher:
        target_cypher, res, intent, heuristic_sim, heuristic_time, parsing_time, executing_time = heuristic_query_for_integration(query, parser,
                                                                                                                                  intent_query_similarity_model,
                                                                                                                                  sim_threshold_heuristic=sim_threshold_heuristic)
        result_source = "heuristic"
        if not target_cypher:
            entity_dict = parser.find_all_entities2dict(query)
            target_cypher, res, intent, entity_query_sim, entity_query_time, parsing_time, executing_time = entity_query_for_integration(entity_dict,
                                                                                                                                         parser)
            result_source = "entity"
            if not target_cypher:
                target_cypher, res, intent, default_query_sim, default_query_time, parsing_time, executing_time = default_query_for_integration(
                    parser)
                result_source = "default"
    total_time = time.time() - start_time
    obj = JsonResponse({
        "result_source": result_source,
        "executing_cypher": target_cypher,
        "content": res,
        "intent": intent,
        "llm_sim": llm_sim,
        "heuristic_sim": heuristic_sim,
        "llm_time": llm_time,
        "heuristic_time": heuristic_time,
        "entity_query_time": entity_query_time,
        "default_query_time": default_query_time,
        "total_time": total_time
    }, safe=False)
    obj['Access-Control-Allow-Origin'] = '*'
    return obj


def llm_query_for_integration(query, llm_parser, intent_query_similarity_model, sim_threshold_llm):
    """
    根据用户问题，调用大语言模型系统，返回 cypher，查询结果，意图等信息。
    :param:
        query (string):
            cypher 语句
            example: "MATCH (s:stock) WHERE s.stock.name == '国泰君安' RETURN s.stock.name, s.stock.province"
        llm_parser (LLM_parser):
            大语言模型解析模块
        intent_query_similarity_model (IntentQuerySimilarityModel):
            意图与问题相似度计算模块
        sim_threshold_llm (float):
            llm 意图与问题相似度阈值
    :return:
        target_cypher (string):
            输出 cypher 语句
            example: "match (n:stock) where n.stock.name=='国泰君安' return n.stock"
        res (dict):
            查询结果
            example: {"name": "国泰君安", "closing_price": "1.45631", ...}
        intent (dict):
            识别出的意图
            example: {'conditions': ['股票财务信息更新季度等于2.00', '行业名称等于零售', '股票财务信息更新年份等于2023.00'], 'returns': ['股票财务信息更新年份', '股票名称', '股票财务信息利润', '股票财务信息更新季度']}
        sim (float):
            相似度值
            example: 0.73541
        llm_time (string):
            llm 总用时
            example: "0.82791s"
        parsing_time (string):
            llm 解析用时
            example: "0.62791s"
        executing_time (string):
            cypher 语句执行用时
            example: "0.20000s"
    """

    start_time = time.time()
    target_cypher = ""
    res = {"None": ["暂无查询结果"]}
    intent = {}
    sim = 0
    parsing_time = 0
    executing_time = 0
    try:
        prompt, tags, edgeTypes = llm_parser.gen_prompt(query)
        server_url = llm_server_url  # Update with your server address
        message_data = {'query': prompt}  # Chinese message
        start_time_parse = time.time()
        target_cypher = send_post_request(server_url, message_data)
        parsing_time = time.time() - start_time_parse
        target_cypher = query_cypher_postprocess_model.postprocess(query, target_cypher)  # 保持问题和cypher的日期统一
        target_cypher = reviewer.date_wrapper(target_cypher)  # 检查语句
        target_cypher = reviewer.append_default_limitation(target_cypher)
        print("#################LLM RESULT#################")
        print(target_cypher)
        intent = llm_parser.get_intent(target_cypher)  # 意图识别
        sim = intent_query_similarity_model.calculate_similarity_bert(query, intent)  # 计算问题和意图的相似度
        start_time_exe = time.time()
        if sim > sim_threshold_llm:  # 大模型输出 cypher 意图与问题相似度超出阈值，返回大模型的输出
            results = []
            res = exe_query(target_cypher, results)
            print('res', res)
        else:
            target_cypher = ""
            res = {"None": ["暂无查询结果"]}
            intent = {}
        executing_time = time.time() - start_time_exe
    except Exception as e:
        print(f"view.py llm_query_for_integration error: {e}")
        traceback.print_exc()
        target_cypher = ""
        res = {"None": ["暂无查询结果"]}
        intent = {}
        sim = 0
        parsing_time = 0
        executing_time = 0
    finally:
        llm_time = time.time() - start_time
        return target_cypher, res, intent, sim, llm_time, parsing_time, executing_time


def heuristic_query_for_integration(query, parser, intent_query_similarity_model, sim_threshold_heuristic):
    """
    根据用户问题，调用启发式系统，返回 cypher，查询结果，意图等信息。
    :param:
        query (string):
            cypher 语句
            example: "MATCH (s:stock) WHERE s.stock.name == '国泰君安' RETURN s.stock.name, s.stock.province"
        parser (Parser):
            启发式解析模块
        intent_query_similarity_model (IntentQuerySimilarityModel):
            意图与问题相似度计算模块
        sim_threshold_llm (float):
            llm 意图与问题相似度阈值
    :return:
        target_cypher (string):
            输出 cypher 语句
            example: "match (n:stock) where n.stock.name=='国泰君安' return n.stock"
        res (dict):
            查询结果
            example: {"name": "国泰君安", "closing_price": "1.45631", ...}
        intent (dict):
            识别出的意图
            example: {'conditions': ['股票财务信息更新季度等于2.00', '行业名称等于零售', '股票财务信息更新年份等于2023.00'], 'returns': ['股票财务信息更新年份', '股票名称', '股票财务信息利润', '股票财务信息更新季度']}
        sim (float):
            相似度值
            example: 0.73541
        heuristic_time (string):
            启发式系统总用时
            example: "0.82791s"
        parsing_time (string):
            启发式系统解析用时
            example: "0.62791s"
        executing_time (string):
            cypher 语句执行用时
            example: "0.20000s"
    """

    start_time = time.time()
    target_cypher = ""
    res = {"None": ["暂无查询结果"]}
    intent = {}
    sim = 0
    parsing_time = 0
    executing_time = 0
    try:
        start_time_parse = time.time()
        cypher_statements, keywords = parser.parse(query)
        results, flattened_cyphers = [], []
        for i in range(len(cypher_statements)):
            for cypher in cypher_statements[i]:
                # print(cypher)
                flattened_cyphers += [cypher]
        target_cypher = heuristic_selection(flattened_cyphers, keywords)
        parsing_time = time.time() - start_time_parse
        intent = parser.get_intent(target_cypher)
        sim = intent_query_similarity_model.calculate_similarity_bert(query, intent)  # 计算问题和意图的相似度
        start_time_exe = time.time()
        if sim > sim_threshold_heuristic:  # 大模型输出 cypher 意图与问题相似度超出阈值，返回大模型的输出
            res = exe_query(target_cypher, results)
        else:
            target_cypher = ""
            res = {"None": ["暂无查询结果"]}
            intent = {}
        executing_time = time.time() - start_time_exe
    except Exception as e:
        print(f"view.py heuristic_query_for_integration error: {e}")
        traceback.print_exc()
        target_cypher = ""
        res = {"None": ["暂无查询结果"]}
        intent = {}
        sim = 0
        parsing_time = 0
        executing_time = 0
    finally:
        heuristic_time = time.time() - start_time
        return target_cypher, res, intent, sim, heuristic_time, parsing_time, executing_time


def entity_query_for_integration(entity_dict, parser):
    """
    根据用户问题，调用实体解析，返回 cypher，查询结果，意图等信息。
    :param:
        entity_dict (dict):
            实体字典
        parser (Parser):
            启发式解析模块
    :return:
        target_cypher (string):
            输出 cypher 语句
            example: "match (n:stock) where n.stock.name=='国泰君安' return n.stock"
        res (dict):
            查询结果
            example: {"name": "国泰君安", "closing_price": "1.45631", ...}
        intent (dict):
            识别出的意图
            example: {'conditions': ['股票财务信息更新季度等于2.00', '行业名称等于零售', '股票财务信息更新年份等于2023.00'], 'returns': ['股票财务信息更新年份', '股票名称', '股票财务信息利润', '股票财务信息更新季度']}
        sim (float):
            相似度值
            example: 0.73541
        entity_query_time (string):
            实体解析总用时
            example: "0.82791s"
        parsing_time (string):
            实体解析用时
            example: "0.62791s"
        executing_time (string):
            cypher 语句执行用时
            example: "0.20000s"
    """

    start_time = time.time()
    target_cypher = ""
    res = {"None": ["暂无查询结果"]}
    intent = {}
    sim = 0
    parsing_time = 0
    executing_time = 0
    try:
        start_time_parse = time.time()
        target_cypher = ""
        match_clause = "MATCH "
        match_objs = []
        where_clause = "WHERE "
        where_objs = []
        return_clause = "RETURN "
        return_objs = []
        limit_clarse = "LIMIT 50"
        tag_num = 1
        for tag, entities in entity_dict.items():
            variate = "v" + str(tag_num)
            match_objs.append("(" + variate + ":" + tag + ")")
            return_objs.append(variate + "." + tag)
            where_objs.append(variate + "." + tag + ".name in " + str(entities))
            tag_num += 1
        if not match_objs:
            entity_query_time = time.time() - start_time
            return target_cypher, res, intent, sim, entity_query_time
        match_clause += ", ".join(match_objs)
        where_clause += " AND ".join(where_objs)
        return_clause += ", ".join(return_objs)
        target_cypher += match_clause + " " + where_clause + " " + return_clause + " " + limit_clarse
        parsing_time = time.time() - start_time_parse
        print("target_cypher: ", target_cypher)

        target_cypher = reviewer.date_wrapper(target_cypher)  # 检查语句
        intent = parser.get_intent(target_cypher)
        results = []
        start_time_exe = time.time()
        res = exe_query(target_cypher, results)
        executing_time = time.time() - start_time_exe
        print("res: ", res)
    except Exception as e:
        print(f"view.py entity_query_for_integration error: {e}")
        traceback.print_exc()
        target_cypher = ""
        res = {"None": ["暂无查询结果"]}
        intent = {}
        parsing_time = 0
        executing_time = 0
    finally:
        entity_query_time = time.time() - start_time
        return target_cypher, res, intent, sim, entity_query_time, parsing_time, executing_time


def default_query_for_integration(parser):
    """
    根据用户问题，调用默认解析，返回 cypher，查询结果，意图等信息。
    :param:
        parser (Parser):
            启发式解析模块
    :return:
        target_cypher (string):
            输出 cypher 语句
            example: "match (n:stock) where n.stock.name=='国泰君安' return n.stock"
        res (dict):
            查询结果
            example: {"name": "国泰君安", "closing_price": "1.45631", ...}
        intent (dict):
            识别出的意图
            example: {'conditions': ['股票财务信息更新季度等于2.00', '行业名称等于零售', '股票财务信息更新年份等于2023.00'], 'returns': ['股票财务信息更新年份', '股票名称', '股票财务信息利润', '股票财务信息更新季度']}
        sim (float):
            相似度值
            example: 0.73541
        default_query_time (string):
            默认解析总用时
            example: "0.82791s"
        parsing_time (string):
            默认解析用时
            example: "0.62791s"
        executing_time (string):
            cypher 语句执行用时
            example: "0.20000s"
    """

    start_time = time.time()
    target_cypher = ""
    res = {"None": ["暂无查询结果"]}
    intent = {}
    sim = 0
    parsing_time = 0
    executing_time = 0
    try:
        start_time_parse = time.time()
        target_cypher = "MATCH (s:stock)-[:has_stock_data]->(sd:stock_data) WHERE sd.stock_data.`date` == date('" + str(
            get_today()) + "') RETURN s.stock.name, sd.stock_data.closing_price AS closing_price ORDER BY closing_price LIMIT 50"
        target_cypher = reviewer.date_wrapper(target_cypher)  # 检查语句
        parsing_time = time.time() - start_time_parse
        intent = parser.get_intent(target_cypher)
        results = []
        start_time_exe = time.time()
        res = exe_query(target_cypher, results)
        executing_time = time.time() - start_time_exe
    except Exception as e:
        print(f"view.py default_query_for_integration error: {e}")
        traceback.print_exc()
        target_cypher = ""
        res = {"None": ["暂无查询结果"]}
        intent = {}
        parsing_time = 0
        executing_time = 0
    finally:
        default_query_time = time.time() - start_time
        return target_cypher, res, intent, sim, default_query_time, parsing_time, executing_time


def exe_query(target_cypher, results):
    """
    根据用户问题，调用实体解析，返回 cypher，查询结果，意图等信息。
    :param:
        target_cypher (string):
            输出 cypher 语句
            example: "match (n:stock) where n.stock.name=='国泰君安' return n.stock"
        results (list):
            查询结果
            example: []
    :return:
        res (string):
            过滤后的查询结果
            example: {"name": "国泰君安", "closing_price": "1.45631", ...}
    """

    res = {"None": ["暂无查询结果"]}
    try:
        query_threads = list()
        query_threads.append(
            threading.Thread(target=query_instance, args=("query", target_cypher, results))
        )
        for query_thread in query_threads:
            query_thread.start()
        # 等待多线程完成
        for query_thread in query_threads:
            query_thread.join()
        res, voted_idx = filter_results(results)
        if not res:
            res = {"None": ["暂无查询结果"]}
    except Exception as e:
        # 处理其他类型的异常
        print(f"view.py exe_query error: {e}")
        res = {"None": ["暂无查询结果"]}
    finally:
        return res


def query(request):
    """
    根据用户的问题实现知识图谱问答。
    :param:
        request (request):
            请求数据，包括问题文本。
            example: request.args.get("query")
    :return:
        answer (json):
            问题的答案。
            example: {"content": {"name": "国泰君安", "closing_price": "1.45631", ...}}
    """
    start_time = time.time()
    args = json.loads(request.body)
    print("请求：", args)
    query = args["query"]
    intent_query_similarity_model = get_intent_query_similarity_model()
    parser = get_parser()
    query_preprocess_model = get_query_preprocess_model()
    query = query_preprocess_model.preprocess(query)  # 对于query的预处理步骤（中文数字转换，日期转换）

    target_cypher, res, intent, heuristic_sim, heuristic_time, parsing_time, executing_time = heuristic_query_for_integration(query, parser,
                                                                                                                              intent_query_similarity_model,
                                                                                                                              sim_threshold_heuristic=0)
    result_source = "heuristic"
    if not target_cypher:
        entity_dict = parser.find_all_entities2dict(query)
        target_cypher, res, intent, entity_query_sim, entity_query_time, parsing_time, executing_time = entity_query_for_integration(entity_dict,
                                                                                                                                     parser)
        result_source = "entity"
        if not target_cypher:
            target_cypher, res, intent, default_query_sim, default_query_time, parsing_time, executing_time = default_query_for_integration(parser)
            result_source = "default"
    total_time = time.time() - start_time
    # obj = JsonResponse({
    #     "result_source": result_source,
    #     "executing_cypher": target_cypher,
    #     "content": res,
    #     "intent": intent,
    #     "heuristic_sim": heuristic_sim,
    #     "heuristic_time": heuristic_time,
    #     "entity_query_time": entity_query_time,
    #     "default_query_time": default_query_time,
    #     "total_time": total_time
    # }, safe=False)

    obj = JsonResponse({
        "parsing_time": parsing_time,
        "keywords": None,
        "executing_cypher": target_cypher,
        "executing_time": executing_time,
        "content": res,
        "intent": intent
    }, safe=False)

    obj['Access-Control-Allow-Origin'] = '*'
    return obj


# def query(request):
#     """
#     根据用户的问题实现知识图谱问答。
#     :param:
#         request ('request'):
#             请求数据，包括问题文本。
#             example: request.args.get("query")
#     :return:
#         answer ('json'):
#             问题的答案。
#             example: {"answer": {"name": "国泰君安“, "closing_price": "1.45631"}
#     """
#     # dicts = get_dicts()
#     # modules=get_modules()
#     args = json.loads(request.body)
#
#     print("请求：", args)
#
#     query = args["query"]
#     query_preprocess_model = get_query_preprocess_model()
#     query = query_preprocess_model.preprocess(query)  # 对于query的预处理步骤（中文数字转换，日期转换）
#
#     # parser=Parser(dicts)
#     parser = get_parser()
#     start_time = time.time()
#     # cypher_statements, keywords, intent=parser.parse(query)
#     cypher_statements, keywords = parser.parse(query)
#     parsing_time = time.time() - start_time
#     start_time = time.time()
#     results, flattened_cyphers = [], []
#
#     for i in range(len(cypher_statements)):
#         for cypher in cypher_statements[i]:
#             # print(cypher)
#             flattened_cyphers += [cypher]
#     target_cypher = heuristic_selection(flattened_cyphers, keywords)
#
#     intent = parser.get_intent(target_cypher)
#
#     query_threads = list()
#     query_threads.append(
#         threading.Thread(target=query_instance, args=("query", target_cypher, results))
#     )
#     for query_thread in query_threads:
#         query_thread.start()
#     # 等待多线程完成
#     for query_thread in query_threads:
#         query_thread.join()
#     # for i in range(len(cypher_statements)):
#     #     results.append([])
#     #     for cypher in cypher_statements[i]:
#     #         print(cypher)
#     #         result = execute_cypher("query", cypher)
#     #         results[i].append(result)
#
#     # print(results)
#
#     # 筛选结果
#     res, voted_idx = filter_results(results)
#     if not res:
#         res = {"None": ["暂无查询结果"]}
#     # print(res)
#     executing_time = time.time() - start_time
#     print("问答响应用时：", executing_time)
#     obj = JsonResponse({
#         "parsing_time": parsing_time,
#         "keywords": None,
#         "executing_cypher": target_cypher,
#         "executing_time": executing_time,
#         "content": res,
#         "intent": intent
#     }, safe=False)
#     obj['Access-Control-Allow-Origin'] = '*'
#     # print(f"###################{datetime.datetime.now()}####################")
#     return obj
#     # return HttpResponse(res, content_type="Application/Json")

def llm_query(request):
    """
    根据用户的问题实现 llm 知识图谱问答。
    :param:
        request (request):
            请求数据，包括问题文本。
            example: request.args.get("query")
    :return:
        answer (json):
            问题的答案。
            example: {"content": {"name": "国泰君安", "closing_price": "1.45631", ...}}
    """
    start_time = time.time()
    args = json.loads(request.body)
    print("请求：", args)
    query = args["query"]
    intent_query_similarity_model = get_intent_query_similarity_model()
    llm_parser = get_llm_parser()
    parser = get_parser()
    query_preprocess_model = get_query_preprocess_model()
    query = query_preprocess_model.preprocess(query)  # 对于query的预处理步骤（中文数字转换，日期转换）

    target_cypher, res, intent, llm_sim, llm_time, parsing_time, executing_time = llm_query_for_integration(query, llm_parser,
                                                                                                            intent_query_similarity_model, sim_threshold_llm=0)
    result_source = "llm"
    if not target_cypher:
        entity_dict = parser.find_all_entities2dict(query)
        target_cypher, res, intent, entity_query_sim, entity_query_time, parsing_time, executing_time = entity_query_for_integration(entity_dict,
                                                                                                                                     parser)
        result_source = "entity"
        if not target_cypher:
            target_cypher, res, intent, default_query_sim, default_query_time, parsing_time, executing_time = default_query_for_integration(parser)
            result_source = "default"
    total_time = time.time() - start_time
    # obj = JsonResponse({
    #     "result_source": result_source,
    #     "executing_cypher": target_cypher,
    #     "content": res,
    #     "intent": intent,
    #     "llm_sim": llm_sim,
    #     "llm_time": llm_time,
    #     "entity_query_time": entity_query_time,
    #     "default_query_time": default_query_time,
    #     "total_time": total_time
    # }, safe=False)

    obj = JsonResponse({
        "parsing_time": parsing_time,
        "keywords": None,
        "executing_cypher": target_cypher,
        "executing_time": executing_time,
        "content": res,
        "intent": intent
    }, safe=False)

    obj['Access-Control-Allow-Origin'] = '*'
    return obj


# def llm_query(request):
#     """
#     根据用户的问题实现知识图谱问答。
#     :param:
#         request ('request'):
#             请求数据，包括问题文本。
#             example: request.args.get("query")
#     :return:
#         answer ('json'):
#             问题的答案。
#             example: {"answer": {"name": "国泰君安“, "closing_price": "1.45631"}
#     """
#
#     parsing_time = 0
#     target_cypher = ''
#     executing_time = ''
#     result = []
#     intent = {}
#
#     # dicts = get_dicts()
#     # modules=get_modules()
#     args = json.loads(request.body)
#
#     print("请求：", args)
#
#     query = args["query"]
#     query_preprocess_model = get_query_preprocess_model()
#     query = query_preprocess_model.preprocess(query)  # 对于query的预处理步骤（中文数字转换，日期转换）
#
#     # parser=Parser(dicts)
#     llm_parser = get_llm_parser()
#     prompt, tags, edgeTypes = llm_parser.gen_prompt(query)
#
#     server_url = 'http://127.0.0.1:8008'  # Update with your server address
#     message_data = {'query': prompt}  # Chinese message
#
#     st = time.time()
#     target_cypher = send_post_request(server_url, message_data)
#     parsing_time = time.time() - st
#     print("#################LLM RESULT#################")
#     print(target_cypher)
#     # 检查语句
#     target_cypher = reviewer.date_wrapper(target_cypher)
#     target_cypher = reviewer.append_default_limitation(target_cypher)
#     target_cypher = query_cypher_postprocess_model.postprocess(query, target_cypher)  # 保持问题和cypher的日期统一
#
#     # 意图识别
#     intent = llm_parser.get_intent(target_cypher)
#
#     st = time.time()
#     results = []
#     query_threads = list()
#     query_threads.append(
#         threading.Thread(target=query_instance, args=("query", target_cypher, results))
#     )
#     for query_thread in query_threads:
#         query_thread.start()
#     # 等待多线程完成
#     for query_thread in query_threads:
#         query_thread.join()
#     executing_time = time.time() - st
#
#     result, _ = filter_results(results)
#     if not result:
#         result = {"None": ["暂无查询结果"]}
#
#     obj = JsonResponse({
#         "parsing_time": parsing_time,
#         "keywords": None,
#         "executing_cypher": target_cypher,
#         "executing_time": executing_time,
#         "content": result,
#         "intent": intent,
#         "tags": tags,
#         "edge_types": edgeTypes,
#         "prompt": prompt
#     }, safe=False)
#     obj['Access-Control-Allow-Origin'] = '*'
#     return obj


def update(request, max_allowed_cyphers=500):
    """
    更新节点或边
    :param:
        request (request):
            请求数据，包括更新类型、更新对象等。
        max_allowed_cyphers (int):
            最大允许同时更新的 cypher 数量
    :return:
        results (json):
            更新响应结果
    """

    start_time = time.time()

    json_data = json.loads(request.body)
    args_list = json_data["content"]
    # print("请求：", args)
    cypher_statements, results = [], []

    for args in args_list:
        type = args["type"]
        object = args["object"]
        vid = args["vid"]
        data = args["data"]

        cypher_statement = "UPDATE " + type + " ON " + object

        if type == "VERTEX":
            cypher_statement += " '" + vid + "'\n"
        elif type == "EDGE":
            cypher_statement += " '" + vid[0] + "' -> '" + vid[1] + "'\n"
        else:
            return HttpResponse("Error: wrong type!")

        cypher_statement += "SET "

        is_first_property = True
        for k, v in data.items():
            if not v:
                continue
            if is_first_property:
                is_first_property = False
            else:
                cypher_statement += ", "
            if isinstance(v, str):
                cypher_statement += k + " = '" + v + "'"
            else:
                cypher_statement += k + " = " + str(v) + ""

        cypher_statement += "\nYIELD "
        is_first_property = True
        for k, v in data.items():
            if is_first_property:
                is_first_property = False
            else:
                cypher_statement += ", "
            cypher_statement += k

        # print("cypher 语句：")
        # print(cypher_statement)

        cypher_statements += [cypher_statement]
        if len(cypher_statements) >= max_allowed_cyphers:
            result = execute_cypher("update", ";".join(cypher_statements))
            results += [result]
            cypher_statements = []
    if cypher_statements:
        result = execute_cypher("update", ";".join(cypher_statements))
        results += [result]

    print("更新响应用时：", time.time() - start_time)
    # print(result)

    return JsonResponse(results, safe=False)
    # return HttpResponse(result, content_type="Application/Json")


def random_update_test(request):
    assert request.method == "POST"
    print("Start random updating...")
    executing_time = random_update()
    print("Finish random updating.")
    obj = JsonResponse({
        "executing_time": executing_time
    }, safe=False)
    obj['Access-Control-Allow-Origin'] = '*'
    return obj


def insert(request):
    """
    插入节点或边
    :param:
        request (request):
            请求数据，包括插入类型、插入对象等。
    :return:
        result (json):
            插入响应结果
    """

    start_time = time.time()

    args = json.loads(request.body)
    print("请求：", args)
    type = args["type"]
    object = args["object"]
    vid = args["vid"]
    data = args["data"]

    # 更新解析用的字典
    update_json(args)

    properties_and_values = []
    for k, v in data.items():
        properties_and_values.append([k, str(v)])

    cypher_statement = "USE stockKGQA;\nINSERT " + type + " " + object + " ("

    for i in range(len(properties_and_values)):
        if i > 0:
            cypher_statement += ", "
        cypher_statement += properties_and_values[i][0]
    cypher_statement += ") VALUES"

    if type == "VERTEX":
        cypher_statement += " '" + vid + "':("
    elif type == "EDGE":
        cypher_statement += " '" + vid[0] + "' -> '" + vid[1] + "':("
    else:
        return HttpResponse("Error: wrong type!")

    for i in range(len(properties_and_values)):
        if i > 0:
            cypher_statement += ", "
        if isinstance(properties_and_values[i][1], str):
            cypher_statement += "'" + properties_and_values[i][1] + "'"
        else:
            cypher_statement += str(properties_and_values[i][1])
    cypher_statement += ")"

    print("cypher 语句：")
    print(cypher_statement)

    result = execute_cypher("insert", cypher_statement)

    print("插入响应用时：", time.time() - start_time)
    # print(result)

    return JsonResponse(result, safe=False)
    # return HttpResponse(result, content_type="Application/Json")


def test(request):
    if request.method == "GET":
        return render(request, 'qa.html')
    else:
        return JsonResponse({"content": "请求方法错误"})


def query_instance(operation, cypher, results_i):
    result = execute_cypher(operation, cypher)
    # print(result)
    results_i.append(result)


def cyphers(request):
    args = json.loads(request.body)
    print("请求：", args)

    query = args["query"]

    # parser=Parser(dicts)
    parser = get_parser()
    cypher_statements, keywords = parser.parse(query)
    print(keywords)
    flattened_cyphers = []
    for i in range(len(cypher_statements)):
        for cypher in cypher_statements[i]:
            # print(cypher)
            flattened_cyphers += [cypher]
    target_cypher = heuristic_selection(flattened_cyphers, keywords['conditions'])
    obj = JsonResponse({
        "cyphers": target_cypher
    }, safe=False)
    obj['Access-Control-Allow-Origin'] = '*'
    return obj


def filter_results(results):
    """
    通过投票过滤查询结果，如票数相同则随机一个
    :param:
        results (list):
            查询结果。
            example: [{"name": "国泰君安", "closing_price": "1.45631", ...}, {"name": "国泰君安", "closing_price": "1.45631", ...}]
    :return:
        voted_result (dict):
            过滤后的查询结果
            {"name": "国泰君安", "closing_price": "1.45631", ...}
        voted_index (int):
            投票结果
    """

    # print("results: ", results)

    # 将空结果转换为python可接收的“''”
    global __NULL__
    __NULL__ = ""

    if results == [] or results[0] is None:
        return {}, -1

    filter_dict = {}
    for res in results:
        res = dict(sorted(res.items()))
        if str(res) in filter_dict:
            filter_dict[str(res)] += 1
        else:
            filter_dict[str(res)] = 1
    # print("投票结果：")
    # print(filter_dict)

    # 找到投票数大于1的最大值，其结果作为最终返回值
    voted_result = {}
    max_voted_num = 1
    for key in filter_dict:
        if filter_dict[key] > max_voted_num:
            max_voted_num = filter_dict[key]
            voted_result = eval(key)

    # 投票数都为1，随机取一个结果作为最终返回值
    random_key = random.choice(list(filter_dict.keys()))
    # print("random_key: ", random_key)
    voted_result = eval(str(random_key))
    # print(voted_result)

    # 若查询结果都是“__null__”，则返回“[]”
    for key in voted_result:
        print(voted_result[key])
        noneFlag = False
        for i in range(len(voted_result[key])):
            if voted_result[key][i] is not None:
                noneFlag = True
                break
        if not noneFlag:
            voted_result[key] = []
    print(voted_result)
    print(results)
    voted_index = -1
    for i in range(len(results)):
        if all(voted_result[key] == results[i][key] for key in voted_result):
            voted_index = i
            break
    return voted_result, voted_index


def heuristic_selection(cyphers, keywords):
    """
    根据启发式系统生成cypher语句的规律，从多个cypher中选择最有可能正确的查询语句。通常选择长度最长的语句作为输出，遇到部分关键词则选择次长的语句作为输出
    :param:
        cyphers (list(str)):
            候选查询语句
        keywords (dict):
            关键词。通常由parser.parse()返回
    :return:
        cyphers[tgt_idx] (str):
            返回的单个可执行查询语句
    """
    
    cyphers = sorted(cyphers, key=lambda x: -len(x))
    tgt_idx = 0
    if not keywords:
        return cyphers[tgt_idx]
    conditions = keywords["conditions"]
    for condition in conditions:
        if ('<entity>和/，/,/ <entity>的<property>差多少' == condition['condition']
                and set(condition['varieties']['property']) & {"开盘价", "收盘价", "最高价",
                                                               "最低价", "涨跌幅", "涨幅", "跌幅", "成交量", "成交",
                                                               "涨停连板天数", "涨停",
                                                               "红三兵", "绩优股", "明星股", "价格", "现价"}):
            tgt_idx = 1
    if "下游" in keywords["entities"]:
        for i, cypher in enumerate(cyphers):
            if "-[a:affect]->" in cypher:
                tgt_idx = i
                break

    return cyphers[tgt_idx]


def send_post_request(url, data):
    headers = {'Content-type': 'application/json'}

    # Send POST request with JSON data
    response = requests.post(url, data=json.dumps(data), headers=headers)
    print(response)

    return json.loads(response.text)["result"]


def get_examples(request):
    """
    获取用于前端展示的几个股票例子
    :param:
        request (request):
            前端的POST请求，参数apply-random-update值为bool类型，决定获取example时是否发起对图谱的动态更新
    :return:
        obj (JsonResponse):
            返回JsonResponse，包含下面example_stocks里股票的当日部分数据
    """
    assert request.method == "POST"
    args = json.loads(request.body)
    apply_random_update = args["apply-random-update"]

    example_stocks = [
        "国泰君安",
        "中信证券",
        "中泰证券",
        "金山办公",
        "贵州茅台",
        "品茗科技",
        "莎普爱思",
        "招商银行",
        "中国平安",
        "中国石油",
        "中国银行",
        "建设银行"
    ]

    if apply_random_update:
        # update_thread = threading.Thread(target=random_update, args = (example_stocks,))
        update_thread = threading.Thread(target=random_update)
        update_thread.start()

    example_cql = f"""
    MATCH (s:stock)-[hsd:has_stock_data]->(sd:stock_data)
    WHERE s.stock.name in {str(example_stocks)}
    AND sd.stock_data.`date` == date()
    RETURN 
    s.stock.name AS `stock name`, 
    sd.stock_data.opening_price AS `opening price`,
    sd.stock_data.closing_price AS `closing price`,
    sd.stock_data.range AS `rise & fall`,
    sd.stock_data.maximum_price AS `maximum price`, 
    sd.stock_data.minimum_price AS `minimum price`"""
    example_cql = reviewer.date_wrapper(example_cql)

    st = time.time()
    result = execute_cypher("query", example_cql)
    executing_time = time.time() - st
    obj = JsonResponse({
        "executing_time": executing_time,
        "content": result,
    }, safe=False)
    obj['Access-Control-Allow-Origin'] = '*'
    return obj


def routine(request):
    """
    用于执行每日股票、行业新节点的插入以及10前旧节点的删除的api，功能主体见同目录下的task.pyl里的daily_initialize()
    :param:
        request (request):
            前端的POST请求，参数src_date值为str类型，创建新节点时复制src_date的旧节点数据，详见同目录下的task.pyl里的daily_initialize()
    :return:
        obj (JsonResponse):
            返回JsonResponse，包含新节点插入，旧节点删除的执行时间
    """
    assert request.method == "POST"
    args = json.loads(request.body)
    src_date, dst_date = None, None
    if "src_date" in args:
        src_date = datetime.datetime.strptime(args["src_date"], '%Y-%m-%d').date()
    if "dst_date" in args:
        dst_date = datetime.datetime.strptime(args["dst_date"], '%Y-%m-%d').date()
    
    executing_time = daily_initialize(dst_date, src_date)

    obj = JsonResponse({
        "executing_time": executing_time,
    }, safe=False)
    obj['Access-Control-Allow-Origin'] = '*'
    return obj


def quarter_routine(request):
    """
    用于执行股票季度财务信息新节点的插入以及10前旧节点的删除的api，功能主体见同目录下的task.pyl里的quarterly_initialize()
    :param:
        request (request):
            前端的POST请求，参数src_date值为str类型，创建新节点时会创建src_date的上一个季度对应的节点，详见同目录下的task.pyl里的quarterly_initialize()
    :return:
        obj (JsonResponse):
            返回JsonResponse，包含新节点插入，旧节点删除的执行时间
    """
    assert request.method == "POST"
    args = json.loads(request.body)
    if "src_date" in args:
        src_date = datetime.datetime.strptime(args["src_date"], '%Y-%m-%d').date()
        executing_time = quarterly_initialize(src_date)
    else:
        executing_time = quarterly_initialize()
    obj = JsonResponse({
        "executing_time": executing_time,
    }, safe=False)
    obj['Access-Control-Allow-Origin'] = '*'
    return obj
