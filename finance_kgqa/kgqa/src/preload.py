"""
预加载模块，包括启发式解析器 Parser，大模型解析器 LLM_parser，意图查询相似度模型 IntentQuerySimilarityModel，查询预处理模块 QueryPreprocessModel
"""

from kgqa.src.parser import Parser
from kgqa.src.llm_parser import LLM_parser
from kgqa.src.dict import *
from kgqa.src.utils.intent_query_similarity import IntentQuerySimilarityModel
from kgqa.src.utils.query_preprocess import QueryPreprocessModel

def preload_modules():
    """
    预加载模块
    :param:
    :return:
    """

    dicts = get_dicts()

    global parser
    parser = Parser(dicts)
    global llm_parser
    llm_parser = LLM_parser(dicts)
    global intent_query_similarity_model
    intent_query_similarity_model = IntentQuerySimilarityModel()
    global query_preprocess_model
    query_preprocess_model = QueryPreprocessModel(dicts)

def get_parser():
    """
    获取 Parser 模块
    :param:
    :return:
        parser (Parser):
            Parser 模块
    """
    return parser

def get_llm_parser():
    """
    获取 llm_parser 模块
    :param:
    :return:
        parser (LLM_parser):
            LLM_parser 模块
    """
    return llm_parser

def get_intent_query_similarity_model():
    """
    获取 IntentQuerySimilarityModel 模块
    :param:
    :return:
        intent_query_similarity_model (IntentQuerySimilarityModel):
            IntentQuerySimilarityModel 模块
    """
    return intent_query_similarity_model


def get_query_preprocess_model():
    """
    获取 QueryPreprocessModel 模块
    :param:
    :return:
        query_preprocess_model (QueryPreprocessModel):
            QueryPreprocessModel 模块
    """
    return query_preprocess_model