import os

from parser import Parser

basedir = os.path.dirname(__file__)

solved_query_path = os.path.join(basedir, '../resources/test/solved_query.txt')
unsolved_query_path = os.path.join(basedir, '../resources/test/unsolved_query.txt')
POC_query_path = os.path.join(basedir, '../resources/test/POC_query.txt')

with open(solved_query_path, 'r', encoding='utf-8') as f:
    solved_queries = f.readlines()
# with open(unsolved_query_path, 'r', encoding='utf-8') as f:
#     unsolved_queries = f.readlines()
# with open(POC_query_path, 'r', encoding='utf-8') as f:
#     POC_queries = f.readlines()

# 修改“[]”中的序号，对应语料query的序号
query = solved_queries[58]
# query = unsolved_queries[1]
# query = POC_queries[1]

psr = Parser()
cyphers = psr.parse(query)
# print('cypher statement: ', cyphers)
for cypher in cyphers:
    for i in range(len(cypher)):
        print(str(i)+". "+str(cypher[i]))
