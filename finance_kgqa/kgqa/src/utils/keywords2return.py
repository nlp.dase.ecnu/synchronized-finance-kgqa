import os
import re
import json

basedir = os.path.dirname(__file__)

class ReturnMapper(object):
    """
    用于生成cypher里RETURN语句的类型

    attributes:
        dependency(Tuple[]):
            dependency[0]:
                kgqa/resources/parse/return_map.json
                关键词到return语句的映射
            dependency[1]:
                kgqa/resources/parse/translate_zh2en.json
                图谱相关文本的中文到英文映射
            dependency[2]:
                kgqa/resources/parse/entities_map.json
                所有实体名称到规范化实体名称的映射，便于形成关键词
            dependency[3]:
                kgqa/resources/parse/tags_properties_map_en.json
                图谱每个tag到该tag的属性的映射

    """
    def __init__(self, dicts):
        self.dependencies = dicts["mapping"], dicts["translator"], dicts["ent_map"], dicts["pro_map"]

    # def __init__(
    #     self,
    #     mapping_path = os.path.join(basedir, "../../resources/parse/return_map.json"),
    #     trans_path = os.path.join(basedir, "../../resources/parse/translate_zh2en.json"),
    #     ent_map_path = os.path.join(basedir, "../../resources/parse/entities_map.json"),
    #     pro_map_path = os.path.join(basedir, "../../resources/parse/tags_properties_map_en.json")
    # ):
    #     with open(mapping_path, "r", encoding="utf8") as f:
    #         mapping = json.load(f)
    #     with open(trans_path, "r", encoding="utf8") as f:
    #         translator = json.load(f)
    #     with open(ent_map_path, "r", encoding="utf8") as f:
    #         ent_map = json.load(f)
    #     with open(pro_map_path, "r", encoding="utf8") as f:
    #         pro_map = json.load(f)
    #     self.dependencies = mapping, translator, ent_map, pro_map

    def keywords2return(self, returns, match_query):
        """
        根据之前生成的match语句，以及return相关的关键词，生成最后的return语句。具体可参考kgqa/resources/parse/return_map.json里的映射关系
        指代变量指kgqa/resources/parse/return_map.json中的x,y,z,m,n等会被替换为shortcut.tag形式的变量，其中m和n指代实体，x,y,z指代变量
        例如m.x可能会被换成sd.stock_data.closiong_price，而m.name会被换成s.stock.name，'name'不是指代变量
        :param
            returns(list[dict]):
                识别到的实体的列表，
            match_query(str):
                match语句
        :return
            return_cypher(List[str])
                返回最终的return语句。长度为一的列表，唯一的元素就是该return语句
            
        """
        mapping, translator, ent_map, pro_map = self.dependencies
        ret_cqls = []

        candidates = re.findall(r"\((.*?):(.*?)\)", match_query)
        candidates = {tag:shortcut for (shortcut, tag) in candidates}
                
        for ret in returns:
            ret_reg = ret["return"]
            ret_cql = mapping[ret_reg][6:]
            if "varieties" not in ret:
                continue
            
            varieties = ret["varieties"]
            patterns_map = None
            return_error = 0

            if "property" in varieties:
                if any(p not in translator for p in varieties["property"]):
                    continue
                variety_properties = [translator[p] for p in varieties["property"]]
                
                patterns = set(re.findall(r"(?<=\s|[,.?])[xyz](?=\s|[,.?]|\b)", ret_cql))
                patterns_kv = re.findall(r'\b[nm]\.(?:[xyz]|name)\b', ret_cql)
                patterns_map = dict()
                # return语句里指代变量个数等于property的个数
                if len(patterns) == len(variety_properties):
                    patterns2property = dict({"name":"name"})
                    for p, var in zip(patterns, variety_properties):
                        patterns2property[p] = var
                        # 更新return语句里指代变量
                        ret_cql = re.sub(f"(?<=\s|[,.?]){p}(?=\s|[,.?]|\\b)", var, ret_cql)
                    # 构建实体指代变量和属性变量(非指代)的映射
                    
                    for kv in patterns_kv:
                        k, v = kv.split(".")
                        if k not in patterns_map:
                            patterns_map[k] = [patterns2property[v]]
                        else:
                            patterns_map[k] += [patterns2property[v]]

            if "entity" in varieties or "tag" in varieties:
                entities = []
                if "entity" in varieties:
                    entities += varieties["entity"]
                if "tag" in varieties:
                    entities += varieties["tag"]
                patterns = re.findall(r"(?<=\s|[,.?])[nm](?=\s|[,.?]|\b)", ret_cql)
                # print(varieties["entity"])
                variety_entities = [translator[ent_map[ent]] for ent in entities]
                # print("################patterns################\n", patterns)
                # print("################entities################\n", variety_entities)
                for ent, pattern in zip(variety_entities, patterns):
                    # 如果关键字包括entity和property或是entity的值不在match里，需要对齐entity与property
                    if "property" in varieties and len(patterns_map) > 0:
                        if any(ent not in pro_map[p] for p in patterns_map[pattern]):
                            for candidate in candidates:
                                if all(candidate in pro_map[p] for p in patterns_map[pattern]):
                                    ent = candidate
                                    break
                    # 如果仍未找到合适的entity类型，则跳过该return关键词
                    if ent in candidates:
                        ret_cql = re.sub(f"(?<=\s|[,.?]){pattern}(?=\s|[,.?]|\\b)", f"{candidates[ent]}.{ent}", ret_cql)
                    else:
                        return_error += 1
                        break
            if return_error > 0:
                continue
            ret_cqls = self.return_update(ret_cqls, ret_cql.strip())
            
        if len(ret_cqls) == 0:
            # 如果无return信息，则返回match语句里的第一个类型的数据
            example_tag, example_shortcut = list(candidates.items())[0]
            return [f"RETURN {example_shortcut}.{example_tag}"]
            # raise ValueError("Empty return-keywords: cannot generate valid return-statements")
            # return ["RETURN NULL"]

        return_cypher = ["RETURN " + ",".join(ret_cqls)]
        return return_cypher
    
    def return_update(self, cqls, new_cql):
        """
        向当前的return语句添加要返回的内容，并判断新内容是否已经被包含，以免冗余
        :param
            cqls(list[str]):
                识当前return语句将要返回的内容，例如['s.stock.name', 'sd.stock_data.closing_price']
            new_cql(str):
                要添加的返回内容，例如'sd.stock_data.range'
        :return
            cqls(List[str])
                更新后return语句将要返回的内容
            
        """
        returns = new_cql.split(",")
        for ret in returns:
            if (ret not in cqls) and (ret.rsplit(".", 1)[0] not in cqls):
                cqls += [ret]
        return cqls

'''
def replace_lower(match_obj, rep):
    return f"{match_obj.group(1)}{rep}{match_obj.group(2)}"

def keywords2return(dependency, returns):
    
    prepro_reg = r"<([\u4e00-\u9fa5]+)([a-z]+)>"
    prepro_pat = r"<[\u4e00-\u9fa5]+([a-z]+)>"

    return_analytic_expressions = []
    for ret in returns:
        patterns_used = re.findall(prepro_reg, ret)
        for i in range(len(dependency)):
            example, ret_q = dependency.loc[i]
            # 先找到配置文件内伪正则表达式的指代字符
            pattern_keys = re.findall(prepro_pat, example)
            # 修改配置文件内伪正则表达式的内容，使其成为可用的正则表达式
            example = re.sub(prepro_reg, replace_lower, example)
            # 利用正则表达式，匹配输入内容
            # print(example, ret)
            pattern_values = re.findall(example, ret)
            # 要求ret里的pattern数量和正则表达式里的patterns数量相等，并且example与ret匹配
            if len(pattern_values) > 0 and len(pattern_keys) == len(patterns_used):
                # patter_values的类型是List[Tuple]，将其转化为单纯的List，方便计算其长度
                pattern_values = list(pattern_values[0])
                for k, v in zip(pattern_keys, pattern_values):
                    ret_q = re.sub("(?<=\s|[,.?]){}(?=\s|[,.?])?".format(k), v, ret_q)
                return_analytic_expressions += [ret_q]
    return return_analytic_expressions
'''
