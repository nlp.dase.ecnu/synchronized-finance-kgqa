"""
用于评估生成的cypher语句和ground-truth的cypher语句是否一致，
由于上述比较会对cypher语句进行拆解，
因此，在该项目中可以用作对cypher语句的意图识别
"""
import re
import sys
import os

current_directory = os.path.dirname(os.path.realpath(__file__))
sys.path.append(current_directory)
from match_evaluate import find_matches
from where_evaluate import find_wheres
from with_evaluate import find_withs
from return_evaluate import find_returns
import pandas as pd
from pprint import pprint


def text_parts(text):
    """
    对cypher语句按照match、where、with、return进行拆分
    :param text: 输入的cypher语句
    :return:
        parts字典
    """
    match_pattern = re.compile(r'match (.*?)(?=where|return|$)')
    where_pattern = re.compile(r'where (.*?)(?=with|return|$)')
    with_pattern = re.compile(r'with (.*?)(?=match|return|$)')
    return_pattern = re.compile(r'return (.*)$')

    match_part = [i.strip() for i in match_pattern.findall(text)]
    where_part = [i.strip() for i in where_pattern.findall(text)]
    with_part = [i.strip() for i in with_pattern.findall(text)]
    return_part = [i.strip() for i in return_pattern.findall(text)]

    parts = {
        'match': match_part,
        'where': where_part,
        'with': with_part,
        'return': return_part,
    }
    return parts


def split(text):
    """
    将输入的cypher语句按照特定格式进行拆分合并
    :param text: 输入的cypher语句
    :return:
        split字典
    """
    # print(text)
    text = text.lower()  # 小写化处理
    text = text.replace('\n', ' ')  # 去除换行
    text = text.replace('"', "'")  # 统一引号
    text = re.sub(r'\s+', ' ', text)  # 去除多个连续空格
    text = text.replace("( ", "(").replace(" )", ")").replace(". ", ".")  # 去除多余空格
    parts = text_parts(text)
    # print(parts)
    # read_dict(parts)

    match_splits, where_splits, with_splits, return_splits = [], [], [], []

    for part in parts['match']:
        match_split = find_matches(part)
        match_splits.append(match_split)
        # print(1)
    for part in parts['where']:
        where_split = find_wheres(part)
        where_splits.append(where_split)
        # print(2)
    for part in parts['with']:
        with_split = find_withs(part)
        with_splits.append(with_split)
        # print(3)
    for part in parts['return']:
        return_split = find_returns(part)
        return_splits.append(return_split)
        # print(4)
    split = {
        'match': match_splits,
        'where': where_splits,
        'with': with_splits,
        'return': return_splits,
    }
    # print(split)
    return split
