import re


def find_matches(text):
    """
    主方法：匹配出cypher语句中的match部分，并进行整理
    :param text:
    :return:
    """
    text = re.sub(r'\s+', ' ', text)  # 去除多个连续空格
    text = text.lower().replace('match ', '')  # 去除match字符
    matches = re.findall(r':[^\)\]]+', text)
    # print(matches)
    matches = sorted([i.replace(' ', '')[1:] for i in matches])
    # print(matches)
    splits = {
        'match_tags': matches
    }
    return splits


if __name__ == '__main__':
    gold = 'MATCH (t:trade)<-[bt:belong_to]-(s:stock)-[hsd:has_stock_data]->(sd:stock_data)'
    # gold = 'MATCH (pof:public_offering_fund)'
    # gold = 'MATCH (s:stock)<-[ico:is_chairman_of]-(cm:chairman)'
    # gold = 'MATCH (s:stock)-[bt:belong_to]->(t:trade)'
    # gold = 'MATCH (sd:stock_data)<-[hsd:has_stock_data]-(s:stock)-[bt:belong_to]->(t:trade)'

    splits = find_matches(gold)
    print(splits)
