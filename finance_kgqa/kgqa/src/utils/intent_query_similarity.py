"""
该文件包含IntentQuerySimilarityModel类。
用于对生成的cypher的意图与用户意图的相似度计算。
"""

import torch
from transformers import BertTokenizer, BertModel
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import CountVectorizer
import os
import json

basedir = os.path.dirname(__file__)


class IntentQuerySimilarityModel():
    """
    用于对生成的cypher的意图与用户意图的相似度计算的类
    """

    def __init__(self):
        model_path = os.path.join(basedir, '../../resources/bert-base-chinese')
        self.tokenizer = BertTokenizer.from_pretrained(model_path)
        self.model = BertModel.from_pretrained(model_path)

    def calculate_semantic_bert(self, sentence):
        """
        计算输入句子的 BERT 向量
        :param sentence: 输入句子
        :return:
            cls_vector: BERT 向量
        """
        inputs = self.tokenizer(sentence, return_tensors='pt', padding=True, truncation=True, max_length=512)
        with torch.no_grad():
            outputs = self.model(**inputs)
        cls_vector = outputs.last_hidden_state[0][0]
        return cls_vector

    def intent_dict2sentence(self, intent_dict):
        """
        将由cypher生成的意图字典拼接为句子
        :param intent_dict: 由cypher生成的意图字典
        :return:
            intent: 意图句子
        """
        intent_list = []
        for k, v in intent_dict.items():
            intent_list.extend(v)
        intent = ','.join(intent_list)
        return intent

    def calculate_similarity_bert(self, query, intent_dict):
        """
        使用bert计算用户query和cypher意图
        :param query: 用户query
        :param intent_dict: 传入的cypher意图字典
        :return:
            相似度
        """
        query_v = self.calculate_semantic_bert(query)
        intent = self.intent_dict2sentence(intent_dict)
        intent_v = self.calculate_semantic_bert(intent)
        sim = cosine_similarity(query_v.unsqueeze(0), intent_v.unsqueeze(0))[0][0]
        return float(sim)

    def calculate_similarity_wordbag(self, query, intent_dict):
        """
        使用wordbag计算用户query和cypher意图
        :param query: 用户query
        :param intent_dict: 传入的cypher意图字典
        :return:
            相似度
        """
        intent = self.intent_dict2sentence(intent_dict)
        vectorizer = CountVectorizer().fit_transform([query, intent])
        similarity_matrix = cosine_similarity(vectorizer)
        sim = similarity_matrix[0, 1]
        return float(sim)


if __name__ == '__main__':
    query = '今天最低价小于20且开盘价小于30的股票有哪些'
    intent_dict = {'conditions': ['2024-01-19', '股票数据最低价小于20.00', '股票数据开盘价小于30.00'],
                   'returns': ['股票名称']}

    intent_query_similarity_model = IntentQuerySimilarityModel()
    sim = intent_query_similarity_model.calculate_similarity_bert(query, intent_dict)
    print(sim)

    path = 'solved_query_llm.json'
    with open(path, 'r', encoding='utf-8') as f:
        datas = json.load(f)
    datas_new = []
    for data in datas[:]:
        query = data['question']
        intent_dict = data['intent']
        try:
            sim = intent_query_similarity_model.calculate_similarity_bert(query, intent_dict)
        except:
            print(query)
            print(intent_dict)
            sim = 0.0
        temp = {
            'query': query,
            'intent': intent_dict,
            'sim': sim
        }
        datas_new.append(temp)

    with open('solved_query_llm_sim.json', 'w', encoding='utf-8') as json_file:
        json.dump(datas_new, json_file, indent=2, ensure_ascii=False)

    with open('solved_query_llm_sim.json', 'r', encoding='utf-8') as f:
        datas = json.load(f)
    sims = [data['sim'] for data in datas]

    print(sorted(sims))
    print('max(sims)', max(sims))
    print('min(sims)', min(sims))
    print('sum(sims) / len(sims)', sum(sims) / len(sims))
