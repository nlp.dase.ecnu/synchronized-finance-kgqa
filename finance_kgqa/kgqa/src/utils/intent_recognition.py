"""
意图识别模块。根据 cypher 语句识别意图。
"""

import re
from pprint import pprint

import kgqa.src.utils.EM.evaluation_em as evalEM

intent_map = {"==": "等于", "<": "小于", ">": "大于", "<=": "小于等于", ">=": "大于等于", "STARTS WITH": "起始为", "ENDS WITH": "结尾为", "CONTAINS": "包含", "IS NOT": "不是"}

def intent_recognition_from_cypher(cypher, translateDict):
    """
    根据 cypher 语句识别意图。
    :param:
        cypher (string):
            cypher 语句
            example: "MATCH (s:stock) WHERE s.stock.name == '国泰君安' RETURN s.stock.name, s.stock.province"
        translateDict (dict):
            translate_en2zh 字典
    :return:
        intent_dict (dict):
            识别出的意图
            example: {'conditions': ['股票财务信息更新季度等于2.00', '行业名称等于零售', '股票财务信息更新年份等于2023.00'], 'returns': ['股票财务信息更新年份', '股票名称', '股票财务信息利润', '股票财务信息更新季度']}
    """

    cypher_structure = evalEM.split(cypher)
    pprint(cypher_structure)

    # 按照 where，return，with 三类 clause 识别意图
    intents = {"where": [], "return": [], "with": []}
    exist_temp = False
    for clause_type, clauses in cypher_structure.items():
        if clause_type == "where":
            for where_reps in clauses:
                for where_type, where_items in where_reps.items():
                    if where_type == "where_calculates":
                        for where_item in where_items:
                            print("where_item: ", where_item)
                            if "date" in where_item:
                                if "==" in where_item:
                                    result_dates = extract_dates_from_text(where_item)
                                    intents["where"] += result_dates
                                else:
                                    intents["where"].append("日期计算")
                                continue
                            for op in ["==", "<", ">", "<=", ">=", "STARTS WITH", "ENDS WITH", "CONTAINS", "IS NOT"]:
                                if " "+op+" " in where_item:
                                    where_words = where_item.split(" "+op+" ")
                                    print("where_words: ", where_words)
                                    if "." in where_words[0] and "." in where_words[1] and not is_int(where_words[1]) and not is_float(where_words[1]):
                                        where_tag_left, where_property_left = get_tag_property_from_where_word(where_words[0], translateDict)
                                        where_tag_right, where_property_right = get_tag_property_from_where_word(where_words[1], translateDict)
                                        intents["where"].append(where_tag_left + where_property_left + intent_map[op] + where_tag_right + where_property_right)
                                    else:
                                        where_tag, where_property = get_tag_property_from_where_word(where_words[0], translateDict)
                                        entity = where_words[1].replace("'", "").replace('"', "")
                                        intents["where"].append(where_tag + where_property + intent_map[op] + entity)
                                    break
        elif clause_type == "return":
            for return_reps in clauses:
                for return_type, return_items in return_reps.items():
                    if return_type == "return_tag_attrs":
                        for return_item in return_items:
                            if return_item != "":
                                return_tag, return_property = get_tag_property_from_returnOrWith_item(return_item, translateDict)
                                intents["return"].append(return_tag + return_property)
                    elif return_type == "return_orderby":
                        for return_item in return_items:
                            return_tag, return_property = get_tag_property_from_returnOrWith_item(return_item[0], translateDict)
                            intents["return"].append(return_tag + return_property)
                    elif return_type == "with_functions":
                        for return_item in return_items:
                            func = return_item[0]
                            obj = return_item[1]
                            if func.upper() in translateDict:
                                func_zh = translateDict[func.upper()]
                                intents["with"].append(func_zh)
                    elif return_type == "return_temps":
                        if "temp" in return_items:
                            exist_temp = True
        elif clause_type == "with":
            for with_reps in clauses:
                for with_type, with_items in with_reps.items():
                    if with_type == "with_tag_attrs":
                        for with_item in with_items:
                            with_tag, with_property = get_tag_property_from_returnOrWith_item(with_item, translateDict)
                            intents["with"].append(with_tag + with_property)
                    elif with_type == "with_functions":
                        for with_item in with_items:
                            func = with_item[0]
                            obj = with_item[1]
                            if func.upper() in translateDict:
                                func_zh = translateDict[func.upper()]
                                intents["with"].append(func_zh)

    # with 子句下的也归类为“返回意图”
    if exist_temp:
        intents["return"] += intents["with"]

    intent_dict = {"conditions": list(set(intents["where"])), "returns": list(set(intents["return"]))}
    print("意图识别：", intent_dict)
    return intent_dict


def extract_dates_from_text(text):
    """
    从文本中抽取日期
    :param:
        text (string):
            带抽取的文本
            example: "s.stock_data.`date` == date('2023-12-12')"
    :return:
        dates (string):
            抽取出的日期
            example: '2023-12-12'
    """

    # 匹配 YYYY-MM-DD 格式的日期
    pattern = r'\b\d{4}-\d{2}-\d{2}\b'

    # 使用 re.findall 获取所有匹配项
    dates = re.findall(pattern, text)

    return dates

def get_tag_property_from_where_word(where_word, translateDict):
    """
    将 where 子句中的 tag 和 property 转为中文
    若“WHERE s.stock.name ...”，则 where_word 为 “stock.name”，若“WHERE h.position_rate ...”，则 where_word 为 “position_rate”
    :param:
        where_word (string):
            待抽取的文本
            example: "s.stock.name"
        translateDict (dict):
            en2zh 翻译字典
    :return:
        tag_left_zh (string):
            翻译后的 tag
            example: '股票'
        property_left_zh (string):
            翻译后的 property
            example: '名称'
    """

    tag_property = where_word.split(".")
    if len(tag_property) == 2:
        where_tag = tag_property[0]
        where_property = tag_property[1]
    elif len(tag_property) == 1:
        where_tag = ""
        where_property = tag_property[0]
    else:
        where_tag = ""
        where_property = ""
    tag_left_zh = translateDict[where_tag] if where_tag != "" and where_tag in translateDict else ""
    property_left_zh = translateDict[where_property] if where_property != "" and where_property in translateDict else ""
    return tag_left_zh, property_left_zh


def get_tag_property_from_returnOrWith_item(returnOrWith_item, translateDict):
    """
    将 return 或 with 子句中的 tag 和 property 转为中文
    若“RETURN s.stock.name ...”，则 returnOrWith_item 为 “stock.name”，若“RETURN h.position_rate ...”，则 returnOrWith_item 为 “position_rate”
    :param:
        returnOrWith_item (string):
            待抽取的文本
            example: "s.stock.name"
        translateDict (dict):
            en2zh 翻译字典
    :return:
        tag_zh (string):
            翻译后的 tag
            example: '股票'
        property_zh (string):
            翻译后的 property
            example: '名称'
    """

    tag_property = returnOrWith_item.split(".")
    if len(tag_property) == 2:
        tag = tag_property[0]
        property = tag_property[1]
    elif len(tag_property) == 1:
        tag = tag_property[0]
        property = ""
    else:
        tag = ""
        property = ""
    tag_zh = translateDict[tag] if tag != "" and tag in translateDict else ""
    property_zh = translateDict[property] if property != "" and property in translateDict else ""
    return tag_zh, property_zh


def is_float(str):
    """
    判断是否为 float 类型
    :param:
        str (string):
            待判断的文本
            example: "2.3"
    :return:
        (bool):
            判断结果
            example: True
    """

    try:
        float(str)
        return True
    except ValueError:
        return False

def is_int(str):
    """
    判断是否为 int 类型
    :param:
        str (string):
            待判断的文本
            example: "2"
    :return:
        (bool):
            判断结果
            example: True
    """

    try:
        int(str)
        return True
    except ValueError:
        return False


# def intent_recognition_from_cypher(cypher, translateDict):
#     """
#     根据cypher语句识别意图
#     :param cypher: cypher语句
#     :param translateDict: en2zh字典
#     :return:
#     """
#
#     clauses = {}
#
#     cypher = cypher.replace("\n", " ")
#     cypher_words = cypher.split(" ")
#     # 分割各个子句
#     cur_clause_head = ""
#     cur_clause_words = []
#     for word in cypher_words:
#         if word in ["MATCH", "WHERE", "WITH", "RETURN", "LIMIT"]:
#             if cur_clause_head in clauses:
#                 clauses[cur_clause_head].append(" ".join(cur_clause_words))
#             else:
#                 clauses[cur_clause_head] = [" ".join(cur_clause_words)]
#             cur_clause_head = word
#             cur_clause_words = [word]
#             continue
#
#         cur_clause_words.append(word)
#     if cur_clause_head in clauses:
#         clauses[cur_clause_head].append(" ".join(cur_clause_words))
#     else:
#         clauses[cur_clause_head] = [" ".join(cur_clause_words)]
#
#     print("clauses: ", clauses)
#
#     # 识别条件意图
#     conditions = []
#     for i in range(len(clauses["WHERE"])):
#         condition_clauses = clauses["WHERE"][i][6:].split(" AND ")  # 从where...and...中提取出conditions子句
#         print("condition_clauses: ", condition_clauses)
#         for j in range(len(condition_clauses)):
#             condition_clause = condition_clauses[j]
#             condition_property = condition_clause.split()[0]
#             items = condition_property.split(".")
#             condition_prop = items[2]
#             if condition_prop in translateDict:
#                 cond_intent = translateDict[condition_prop]
#                 if "-" in condition_clause or "+" in condition_clause:  # 子句中包含“-”或“+”
#                     cond_intent += "差值"
#                 conditions.append(cond_intent)
#     conditions = list(set(conditions))
#
#     # 识别返回意图
#     returns = []
#     return_clause = clauses["RETURN"][0]
#     return_clause = return_clause.replace(",", " ")
#     return_items = return_clause.split()[1:]
#     print("return_items: ", return_items)
#     for i in range(len(return_items)):
#         if "." in return_items[i]:
#             returns.append(translateDict[return_items[i].split(".")[2]])
#         else:
#             with_clause = clauses["WITH"][-1]
#             with_reps = with_clause.split("WITH ")[0].split(",")
#             print("with_reps: ", with_reps)
#             for j in range(len(with_reps)):
#                 if return_items[i][0] in with_reps[j] and "AS" in with_reps[j]:
#                     with_items = with_reps[j].split()
#                     with_item_props = with_items[0].split(".")
#                     returns.append(translateDict[with_item_props[2]])
#     returns = list(set(returns))
#
#     intent_dict = {"condition_intent": conditions, "return_intent": returns}
#     print("意图识别：", intent_dict)
#
#     return intent_dict



# def intent_recognition(query, keywords, intentDict):
#     # 意图识别，根据 keywords 给出 where_parse.json 中的 key，并做槽填充
#     # print("keywords: ", keywords)
#
#     intent = []
#
#     returns = keywords['returns']
#     conditions = keywords['conditions']
#     base_situation = []
#
#     # 获取 keywords 中 returns 部分体现出的意图
#     for retu in returns:
#         if not retu['return'] in intentDict:
#             continue
#         # if intentDict[retu['return']] == "<entity>的基本情况" and len(returns) + len(conditions) > 1:
#         #     continue
#         if retu['return'] == "<entity>":  # 先记录下 <entity> 的意图，当没有其他意图当时候补充为 <entity>的基本情况
#             base_situation, _ = deduplicate(retu['varieties']['entity'][0], base_situation)
#         return_i = intentDict[retu['return']]
#         if 'varieties' in retu and 'property' in retu['varieties']:
#             for i in range(len(retu['varieties']['property'])):
#                 return_i = return_i.replace('<property>', retu['varieties']['property'][i], 1)
#         if 'varieties' in retu and 'entity' in retu['varieties']:
#             for i in range(len(retu['varieties']['entity'])):
#                 return_i = return_i.replace('<entity>', retu['varieties']['entity'][i], 1)
#         if 'varieties' in retu and 'number' in retu['varieties']:
#             for i in range(len(retu['varieties']['number'])):
#                 return_i = return_i.replace('<number>', retu['varieties']['number'][i], 1)
#         if 'varieties' in retu and 'tag' in retu['varieties']:
#             for i in range(len(retu['varieties']['tag'])):
#                 return_i = return_i.replace('<tag>', retu['varieties']['tag'][i], 1)
#
#         # if return_i == "<entity>":  # 先记录下 <entity> 的意图，当没有其他意图当时候补充为 <entity>的基本情况
#         #     base_situation, _ = deduplicate(return_i, base_situation)
#         intent, _ = deduplicate(return_i, intent)
#
#     # print("意图识别：", intent)
#
#     # 获取 keywords 中 conditions 部分体现出的意图
#     for cond in conditions:
#         # if intentDict[cond['condition']] == "<entity>的基本情况" and len(returns) + len(conditions) > 1:
#         #     continue
#         if cond['condition'] == "<entity>":  # 先记录下 <entity> 的意图，当没有其他意图当时候补充为 <entity>的基本情况
#             base_situation, _ = deduplicate(cond['varieties']['entity'][0], base_situation)
#         condition = intentDict[cond['condition']]
#         if 'varieties' in cond and 'property' in cond['varieties']:
#             for i in range(len(cond['varieties']['property'])):
#                 condition = condition.replace('<property>', cond['varieties']['property'][i], 1)
#         if 'varieties' in cond and 'entity' in cond['varieties']:
#             for i in range(len(cond['varieties']['entity'])):
#                 condition = condition.replace('<entity>', cond['varieties']['entity'][i], 1)
#         if 'varieties' in cond and 'number' in cond['varieties']:
#             for i in range(len(cond['varieties']['number'])):
#                 condition = condition.replace('<number>', cond['varieties']['number'][i], 1)
#         if 'varieties' in cond and 'time' in cond['varieties']:
#             for i in range(len(cond['varieties']['time'])):
#                 condition = condition.replace('<time>', cond['varieties']['time'][i], 1)
#         if 'varieties' in cond and 'timespan' in cond['varieties']:
#             if isinstance(cond['varieties']['timespan'], list):
#                 condition = condition.replace('<timespan>', cond['varieties']['timespan'][0], 1)
#             elif isinstance(cond['varieties']['timespan'], dict):
#                 condition = condition.replace('<timespan>', cond['varieties']['timespan']['num']+cond['varieties']['timespan']['unit'], 1)
#         if 'varieties' in cond and 'tag' in cond['varieties']:
#             for i in range(len(cond['varieties']['tag'])):
#                 condition = condition.replace('<tag>', cond['varieties']['tag'][i], 1)
#
#         # if condition == "<entity>":  # 先记录下 <entity> 的意图，当没有其他意图当时候补充为 <entity>的基本情况
#         #     base_situation, _ = deduplicate(condition, base_situation)
#         intent, _ = deduplicate(condition, intent)
#
#     # print(intent)
#
#     properties = keywords['properties']
#     # print(properties)
#     for prop in properties:
#         intent, _ = deduplicate(prop, intent)
#
#     # print(intent)
#
#     # 若 intent 列表中只存在 <entity> ，则将其换为 <entity>的基本情况
#     newIntent = []
#     for i in range(len(intent)):
#         if intent[i] in base_situation:
#             newIntent.append(intent[i] + "的基本情况")
#         else:
#             break
#     if len(newIntent) == len(intent):
#         intent = newIntent
#
#     # 没有识别出意图，但是有 <entity>的基本情况 ，则返回基本情况
#     if len(intent) == 0 and len(base_situation) > 0:
#         intent, _ = [bs + "的基本情况" for bs in base_situation]
#
#     # 如果到最后也没有识别出任何意图，则返回原问题
#     if intent == []:
#         intent.append(query)
#
#     print("意图识别：", intent)
#
#     return intent

# def deduplicate(item, curList):
#     # 去重函数，如果curList中存在包含item的字符串，则不添加item，否则添加
#     # print("item: ", item)
#     # print("curList: ", curList)
#
#     newList = []
#     need_append_flag = True
#     for item_in_list in curList:
#         if len(item) > len(item_in_list):
#             long_item = item
#             short_item = item_in_list
#         else:
#             long_item = item_in_list
#             short_item = item
#
#         repeat_flag = True  # 记录是否重复
#         for letter_in_short_item in short_item:
#             if not letter_in_short_item in long_item:   # short_item 中存在 long_item 没有的字符
#                 repeat_flag = False
#                 break
#         if repeat_flag: # 存在重复
#             # print("存在重复")
#             if long_item == item_in_list:   # long_item 在列表里，则append long_item，并标识不需要append item
#                 need_append_flag = False
#                 newList.append(long_item)
#                 continue
#         else:
#             # print("item_in_list: ", item_in_list)
#             newList.append(item_in_list)
#     if need_append_flag:
#         newList.append(item)
#     return newList, need_append_flag    # 新的list，是否添加了 item


if __name__ == "__main__":
    # keywords = {'entities': ['公募基金'],
    #             'properties': [], 'time': [],
    #             'conditions': [
    #                 {'condition': '<entity>和/，/,/ <entity>的<property>差多少',
    #                  'varieties': {'entity': ['中欧医疗健康混合c', '易方达蓝筹精选混合'], 'property': ['最新净值']}},
    #                 {'condition': '<entity>(和<entity>)+',
    #                  'varieties': {'entity': ['中欧医疗健康混合c', '易方达蓝筹精选混合']}}],
    #             'returns': [{'return': {}}]}
    # intent_recognition(keywords)

    cypher = "MATCH (pof2:public_offering_fund)-[h2:hold]->(s:stock)-[hd1:has_stock_data]->(sd1:stock_data) WHERE sd1.stock_data.opening_price == date('2023-12-01') AND pof2.public_offering_fund.name == '国泰君安' WITH sd1.stock_data.opening_price AS temp1 MATCH (pof2:public_offering_fund)-[h2:hold]->(s:stock)-[hd1:has_stock_data]->(sd1:stock_data) WHERE sd1.stock_data.opening_price == date('2023-12-01') AND pof2.public_offering_fund.name == '中信证券' WITH ABS(sd1.stock_data.opening_price - temp1) as abs_diff RETURN abs_diff"
    intent_recognition_from_cypher(cypher)