import os
import re
import pandas as pd
from collections import Counter, deque

basedir = os.path.dirname(__file__)

class MatchMapper(object):
    """
    用于生成cypher里MATCH语句的类型

    attributes:
        dependency(pd.dataframe):
            在类初始化时加载kgqa/resources/parse/match.csv文件，存储关键词和match实体的映射关系，以生成match语句

    """
    def __init__(self, dicts):
        self.dependency = dicts["match"]

    # def __init__(self, path = os.path.join(basedir, "../../resources/parse/match.csv")):
    #     df = pd.read_csv(path, encoding="utf8")
    #     df = df.fillna("-")
    #     self.dependency = df


    def keywords2match(self, entities, properties, time):
        """
        根据识别到的实体、属性、时间条件和它们的组合关系（match条件），生成tags列表。具体可参考resources/parse/match.csv里的映射关系
        :param
            entities(list[str]):
                识别到的实体的列表，
            properties(list[str]):
                识别到的属性的列表
            time(list[str]):
                识别到的时间条件的列表
        :return
            match_analytic_expressions(List[str])
                返回初步的match实体的列表，列表中每个元素代表一个match条件对应的tags。例如['(s:stock)', '(s:stock),(sd:stock_data)']
            
        """
        match_analytic_expressions = []
        dependency = self.dependency
        for i in range(len(dependency)):
            line = dependency.loc[i]
            if not self.conditional_match(entities, line.iloc[0]):
                continue
            if not self.conditional_match(properties, line.iloc[1]):
                continue
            if not self.conditional_match(time, line.iloc[2]):
                continue
            match_analytic_expressions += [line.iloc[3]]
        return match_analytic_expressions

    def conditional_match(self, query_keywords, match_keyword):
        """
        判断关键词是否符合自定义的match条件。具体可参考resources/parse/match.csv里的映射关系
        :param
            query_keywords(str):
                单个关键词
            match_keyword(str):
                单个match条件
        :return
            p1 or p2 or p3 or p4(bool):
                是否符合条件
            
        """
        # '-'表示任意匹配
        p1 = (match_keyword == '-')
        # 关键词有重叠
        p2 = (len(set(query_keywords) & set(match_keyword.split("/"))) > 0)
        p3 = (match_keyword in query_keywords)
        # <empty>仅匹配空列表
        p4 = (query_keywords == [] and match_keyword == "<empty>")
        return p1 or p2 or p3 or p4

    def extract_tags_for_match(self, match_queries):
        """
        抽取keywords2match输出的match语句里涉及的实体并进行一定程度的去重。具体而言，不同match条件里的重复实体会进行去重，同一个match条件
        里的重复实体会进行去重。
        例如['(s:stock)', '(s:stock),(sd:stock_data)']中有两个match条件对应的tags出现了stock实体，那么最后只会保留一个
        例如['(i1:industry),(i2:industry)']中有一个match条件对应的tags出现了两个industry实体，最后则会保留两个
        实体的重复数量仅保留单个match条件对应tags里重复的最大次数
        :param
            match_queries(list[str]):
                所有match条件对应的tags
        :return
            tags(List[str])
                返回查询链路会用到的所有tags，包含重复
            
        """
        multi_tags = [re.findall(r"\(.*?:(.*?)\)", match_query) for match_query in match_queries]
        counter = Counter()
        # 对于不同的match语句的重复值进行去重，同一match语句里的重复值不去重
        for tags in multi_tags:
            for k, v in Counter(tags).items():
                counter[k] = max(counter[k], v)
        tags = []
        for k, v in counter.items():
            tags += [k] * v
        return tags

    def tags2match(self, tags, entities_map, graph):
        """
        根据有序的tags和图谱的图结构生成最后的match语句
        :param
            tags(list[str]):
                经过链路补全后的有序tag列表
            entities_map(dict):
                将tag名称映射到graph里的索引
            graph(List[List]):
                图谱对应图结构的邻接矩阵
        :return
            tags(List[str])
                返回所有可能的match语句
            
        """
        # 给重复的值加上后缀
        counter = Counter()
        short_cuts = [tag.split(":")[0] for tag in tags]
        tag_names = [tag.split(":")[1] for tag in tags]

        for i, name in enumerate(tag_names):
            if counter[name] > 0:
                short_cuts[i] = f"{short_cuts[i]}{counter[name]}"
            counter[name] += 1

        tags = [f"{short_cuts[i]}:{tag_names[i]}" for i in range(len(tags))]

        # 根据tags生成对应cypher语句
        cqls = deque()
        cqls.append((f"({tags[0]})", 1))
        ret_cqls = []
        while cqls:
            cql, i = cqls.popleft()
            if i == len(tags):
                ret_cqls += ["MATCH " + cql]
                continue
            elif tag_names[i-1] == tag_names[i]:
                # 遇到自环，由于无法判断关系的起点与终点，因此都输出出来作为候选
                cqls.append((cql + "<" + graph[entities_map[tag_names[i-1]]][entities_map[tag_names[i]]] + f"({tags[i]})", i+1))
                cqls.append((cql + graph[entities_map[tag_names[i-1]]][entities_map[tag_names[i]]] + ">" + f"({tags[i]})", i+1))
            else:
                cqls.append((cql + graph[entities_map[tag_names[i-1]]][entities_map[tag_names[i]]] + f"({tags[i]})", i+1))

        return ret_cqls