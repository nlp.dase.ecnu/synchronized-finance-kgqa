import json
import heapq
import os
from copy import deepcopy
from collections import Counter

basedir = os.path.dirname(__file__)

class LinkBuilder(object):
    """
    用于根据查询所有需要的tags补充、并生成有序、可执行的tags列表
    kgqa/resources/parse/schema.json，存储所有的tag中英文名称，以及edge的起点、终点、中英文名称

    attributes:
        entities_map(dict):
            实体tag名称到索引的映射
        entities_map_inv(list):
            索引到实体tag名称的映射
        graph(List[List]):
            实体tag索引构成的邻接矩阵，矩阵中的值是edge的名称或者None

    """
    def __init__(self, dicts):
        entities, relations = dicts["schema_entities"], dicts["schema_relations"]
        N = len(entities)
        graph = [[None] * N for _ in range(N)]

        entities_map = [line.split() for line in entities]
        entities_map_chn = {item[0]: i for i, item in enumerate(entities_map)}
        entities_map_eng = {item[1]: i for i, item in enumerate(entities_map)}
        entities_map_inv = ["{}:{}".format(self.get_shortcut(item[1]), item[1]) for item in entities_map]
        entities_map = {**entities_map_chn, **entities_map_eng}

        for (edge, src, dst) in relations:
            _, edge_name_eng = edge.split()
            shortcut = self.get_shortcut(edge_name_eng)
            src_idx, dst_idx = entities_map[src], entities_map[dst]
            if src_idx != dst_idx:
                graph[src_idx][dst_idx] = "-[{}:{}]->".format(shortcut, edge_name_eng)
                graph[dst_idx][src_idx] = "<-[{}:{}]-".format(shortcut, edge_name_eng)
            else:
                # 自环无法确定方向
                graph[src_idx][dst_idx] = "-[{}:{}]-".format(shortcut, edge_name_eng)

        self.entities_map, self.entities_map_inv, self.graph = entities_map, entities_map_inv, graph

    # def __init__(self, schema_path = os.path.join(basedir,"../../resources/parse/schema.json")):
    #     # 获取所有实体、关系
    #     with open(schema_path, "r", encoding="utf-8") as f:
    #         entities, relations = json.load(f)
    #     N = len(entities)
    #     graph = [[None] * N for _ in range(N)]
    #
    #     entities_map = [line.split() for line in entities]
    #     entities_map_chn = {item[0]:i for i, item in enumerate(entities_map)}
    #     entities_map_eng = {item[1]:i for i, item in enumerate(entities_map)}
    #     entities_map_inv = ["{}:{}".format(self.get_shortcut(item[1]), item[1]) for item in entities_map]
    #     entities_map = {**entities_map_chn, **entities_map_eng}
    #
    #     for (edge, src, dst) in relations:
    #         _, edge_name_eng = edge.split()
    #         shortcut = self.get_shortcut(edge_name_eng)
    #         src_idx, dst_idx = entities_map[src], entities_map[dst]
    #         if src_idx != dst_idx:
    #             graph[src_idx][dst_idx] = "-[{}:{}]->".format(shortcut, edge_name_eng)
    #             graph[dst_idx][src_idx] = "<-[{}:{}]-".format(shortcut, edge_name_eng)
    #         else:
    #             # 自环无法确定方向
    #             graph[src_idx][dst_idx] = "-[{}:{}]-".format(shortcut, edge_name_eng)
    #
    #     self.entities_map, self.entities_map_inv, self.graph = entities_map, entities_map_inv, graph
        

    def get_shortcut(self, name):
        """
        根据tag的完整名称得到缩写，作为查询变量。缩写不能与return_map里的代指变量重复，例如m和n，以及不能与nebula graph的关键字重复
        :param
            name(str):
                tag的完整名称
        :return
            shortcut(str)
                缩写字符串
        """
        single_word_dic = {"chairman":"cm", "stockholder":"sholder", "information":"info", "industry":"indus", "originate_from":"ofr"}
        if name in single_word_dic:
            shortcut = single_word_dic[name]
        else:
            shortcut = "".join([word[0] for word in name.split("_")])
        return shortcut

    def astar_search_without_duplication(self, start, target_nodes):
        """
        在tags无重复的情况下调用A*算法以搜索一条完整的链路，不会遍历已经展开过的节点
        启发式估计函数为当前链路中还未涉及到tags里面的tag数量
        :param
            start(int):
                搜索起点索引，一般为tags里的第一个tag
            target_nodes(List[int]):
                tags的索引列表
        :return
            path(List[int]):
                补全后的有序tags索引列表
            
        """
        open_list = [(0, start, [])]  # (f_cost, current_node, path)
        
        while open_list:
            f_cost, current_node, path = heapq.heappop(open_list)
            
            path += [current_node]
            # 目标节点中还需遍历的节点个数
            h_cost = len(target_nodes - set(path))
            g_cost = len(path)
            
            if h_cost == 0:
                return path
            
            for neighbor, weight in enumerate(self.graph[current_node]):
                if weight and neighbor not in path:
                    f_cost = g_cost + h_cost
                    heapq.heappush(open_list, (f_cost, neighbor, deepcopy(path)))
        return None

    def astar_search_with_duplication(self, start, target_nodes):
        """
        在tags有重复的情况下调用A*算法以搜索一条完整的链路，会遍历已经展开过的节点
        启发式估计函数为tags里面的每个tag还需遍历的次数之和
        :param
            start(int):
                搜索起点索引，一般为tags里的第一个tag
            target_nodes(List[int]):
                tags的索引列表
        :return
            path(List[int]):
                补全后的有序tags索引列表
            
        """
        open_list = [(0, start, [])]  # (f_cost, current_node, path)
        target_counter = Counter(target_nodes)
        
        while open_list:
            f_cost, current_node, path = heapq.heappop(open_list)
            
            path += [current_node]
            node_counter = Counter(path)
            # 目标节点中，所有节点还需遍历的次数之和
            h_cost = sum(max(target_counter[e] - node_counter[e], 0) for e in target_counter)
            g_cost = len(path)
            if h_cost == 0:
                return path
            
            for neighbor, weight in enumerate(self.graph[current_node]):
                # 由于路径可重复，节点展开无需其它条件
                if weight:
                    f_cost = g_cost + h_cost
                    heapq.heappush(open_list, (f_cost, neighbor, deepcopy(path)))
        return None


    def link_completion_(self, tags):
        """
        链路补全，用于根据查询所有需要的tags补充、并生成有序、可执行的tags列表
        :param
            tags(List[str]):
                tags名称的列表
        :return
            completed_tags(List[str]):
                补全后的tags名称的列表
            
        """
        # 将tag转化为顶点的index
        target_nodes = [self.entities_map[tag] for tag in tags]
        # 用冒泡法求得最短的搜索路径
        shortest_path, shortest_len = None, 0XFFFFFFFF
        for start in set(target_nodes):
            # 根据需要求出的路径是否有重复判断使用哪种搜索，但这是未知的，所以两种方法都进行了尝试
            # 两种搜索方法的时间开销不是同一数量级，第一种方案的时间开销远小于第二种，V、E是图的点、边数，N是tags长度
            # 如果无重复，首先以无重复的情况进行搜索，时间复杂度上界N^2*(E/V)^(V)
            if len(target_nodes) == len(set(target_nodes)):
                path = self.astar_search_without_duplication(start, set(target_nodes))
                if path and len(path) < shortest_len:
                    shortest_len = len(path)
                    shortest_path = path
                    continue
            # 如果无重复的情况下无解或是有重复，则使用有重复情况的搜索，时间复杂度上界N^2*V^(NV)
            path = self.astar_search_with_duplication(start, target_nodes)
            if path and len(path) < shortest_len:
                shortest_len = len(path)
                shortest_path = path
                continue
        if not shortest_path:
            raise ValueError("Bad tags: Cannot build valid link for given tags")
        completed_tags = [self.entities_map_inv[vex] for vex in shortest_path]
        return completed_tags