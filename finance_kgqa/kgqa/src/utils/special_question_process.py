import re

class SpecialQuestionProcessor(object):
    """
    针对于特定模板的问题进行特定的处理，模板在kgqa/resources/parse/special_question_regular.json内。
    模板数据类型为dictionary(map)，是key-value类型的数据。
    模板规则：
    key为需要增加的特殊模板自然语言问题的伪正则表达式。其中可以有{entity}，{property}来匹配配置文件中所有的
    实体、属性。
    重点在于正则的捕获组建立，被()括住的内容会建立捕获组，捕获出的列表是按照key里正则()出现的顺序进行排序，详
    细见python的re模块的文档。
    value为新增加特殊模板自然语言问题对应的伪cypher语句。其中，cypher语句会出现{var0}的字样，var0代表key中建
    立的捕获组的第0个元素。
    例如key为"({entity})的?历史最高({property})"，那么它所建立的捕获组就有两个元素，第一个元素是({entity})位
    置对应的实体，在cypher语句中会被替换到var0的位置，第二个元素是({property})位置对应的属性，它会被替换到
    cypher语句中var1的位置。对于这个模板，问题"国泰君安的历史最高收盘价"建立的捕获组就是['国泰君安', '收盘价']
    他们会被换到cypher语句中var0,var1的位置。
    对于特殊字符，请务必带上花括号{}。{entity},{query},{var0},{var1}，这涉及到Python的字符串格式化
    关于python正则表达式，可参考以下教程：
    https://www.runoob.com/python3/python3-reg-expressions.html
    """
    def __init__(self, dicts) -> None:
        self.templates = dicts["special_question"]
        self.properties = dicts["properties"]
        self.entities = dicts["entities"]
        self.translate_zh2en = dicts["translate_zh2en"]
        
        self.reg_dic = {}
        self.reg_dic["entity"] = "|".join(self.entities)
        self.reg_dic["property"] = "|".join(self.properties)
        # 替换模板中的实体、属性等为真实的实体、属性
        self.initialize_regulars()

    def translate(self, word):
        return self.translate_zh2en[word] if word in self.translate_zh2en else word

    def initialize_regulars(self):
        new_dict = {}
        for reg, cypher in self.templates.items():
            reg = reg.format(**self.reg_dic)
            new_dict[reg] = cypher
        self.templates = new_dict
    
    def match(self, query):
        target_cypher = ""
        for reg, cypher in self.templates.items():
            repl = re.findall(reg, query)
            if repl:
                repl = repl[0]
                repl_var = {"var" + str(i) : self.translate(rep) for i, rep in enumerate(repl)}
                target_cypher = cypher.format(**repl_var)
                # 仅输出第一个匹配到的模板
                break
        return target_cypher