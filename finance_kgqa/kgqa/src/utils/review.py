import re
import datetime
from workalendar.asia import China

# TODAY = datetime.date.today()
def get_today():
    return datetime.date.today()

class SelfReviewer(object):
    """
    在启发式生成完整的cypher查询语句之后，根据全局的信息对查询语句进行优化的类型

    """
    def __init__(self):
        pass

    def forward(self, cyphers):
        for i in range(len(cyphers)):
            cyphers[i] = self.append_default_limitation(cyphers[i])
            cyphers[i] = self.orderby_reconstruction(cyphers[i])
            cyphers[i] = self.remove_return_redundancy(cyphers[i])
            cyphers[i] = self.match_redundancy_elimination(cyphers[i])
            cyphers[i] = self.temp_variate_saving(cyphers[i])
            cyphers[i] = self.append_default_time_condition(cyphers[i])
            cyphers[i] = self.date_wrapper(cyphers[i])
            cyphers[i] = self.append_default_time_return(cyphers[i])
        return cyphers

    def temp_variate_saving(self, cypher):
        """
        根据cypher里的with语句存储with后设计的所有临时变量。主要用于有多个with的情况，
        在第二次以及之后with的时候保存之前with的所有临时变量
        :param
            cypher(str):
                未处理的cypher
        :return
            cypher(str)
                处理后的cypher
        """
        temp_vars = re.findall(r"WITH (.*?) as ([a-zA-z_0-9]+)", cypher)
        if len(temp_vars) > 1:
            temp_vars_dict = []
            prev_temp_vars = temp_vars[0][1]
            for i in range(1, len(temp_vars)):
                prev_temp_vars = temp_vars[i][1] + " , " + prev_temp_vars
                temp_vars_dict += [(temp_vars[i][0], temp_vars[i][1], prev_temp_vars)]
            for k, v, nv in temp_vars_dict:
                cypher = re.sub(f"WITH {k} as {v}", f"WITH {k} AS {nv}", cypher)
        
        return cypher

    def orderby_reconstruction(self, cypher):
        """
        重构ORDER BY类型的语句
        :param
            cypher(str):
                未处理的cypher
        :return
            cypher(str)
                处理后的cypher
        """
        if "ORDER BY" not in cypher:
            return cypher
        other_statement, return_statement = cypher.rsplit("RETURN ", 1)
        return_statement, orderby_statement = return_statement.split("ORDER BY ")
        orderby_variate, other_statement_1 = orderby_statement.split(maxsplit = 1)

        alter_name = ''.join([cut[0].upper() for cut in re.split(r"[._]", orderby_variate)])
        if orderby_variate in return_statement:
            return_statement = "RETURN " + re.sub(orderby_variate, f"{orderby_variate} AS {alter_name}", return_statement)
        else:
            return_statement = "RETURN " + return_statement.strip() + ", " + f"{orderby_variate} AS {alter_name}\n"
        return other_statement + return_statement + "ORDER BY " + alter_name + " " + other_statement_1

    def match_redundancy_elimination(self, cypher):
        """
        删除所有match子语句中未被使用的tag和edge，以加快查询执行速度
        从match链路两端向里遍历tag，如果tag以及tag之后的edge没有在接下来的语句中被使用，
        则删除tag和相邻的悬挂边edge
        :param
            cypher(str):
                未处理的cypher
        :return
            cypher(str)
                处理后的cypher
        """
        # 从链路两端删除没用到的match实体
        sub_cyphers, return_statement = cypher.split("RETURN ")
        return_statement = "RETURN " + return_statement
        sub_cyphers = sub_cyphers.split("MATCH ")
        match_statements = []
        returns = re.findall(r"RETURN (.*?)[\b\s\n]", return_statement)[0]
        return_shortcuts = set([ret.split(".")[0] for ret in returns.split(",")])
        for sub_cypher in sub_cyphers:
            if not sub_cypher or sub_cypher == "\n":
                continue
            cypher_splits = sub_cypher.split("\n", 1)
            match_statement, other_statement = cypher_splits[0], cypher_splits[1]
            entities_relationships_groups = re.findall(r"(\(.*?:.*?\))|(-\[\w+:\w+\]->)|(<-\[\w+:\w+\]-)", match_statement)
            entities_relationships = []
            for group in entities_relationships_groups:
                for item in group:
                    if item:
                        entities_relationships += [item]

            ld, rd = 0, len(entities_relationships)
            for i, entities in enumerate(entities_relationships):
                if not entities.startswith("("):
                    continue
                shortcut = entities[1:-1].split(":")[0]
                if shortcut in return_shortcuts:
                    break
                if re.findall(f"(\W|\\b){shortcut}(\W|\\b)",other_statement) or (i + 1) >= len(entities_relationships):
                    break
                shortcut = entities_relationships[i+1].strip("<>-[]").split(":")[0]
                if re.findall(f"(\W|\\b){shortcut}(\W|\\b)",other_statement):
                    break
                ld += 2

            for i, entities in enumerate(entities_relationships[::-1]):
                if not entities.startswith("("):
                    continue
                shortcut = entities[1:-1].split(":")[0]
                if shortcut in return_shortcuts:
                    break
                if re.findall(f"(\W|\\b){shortcut}(\W|\\b)",other_statement) or (i + 1) >= len(entities_relationships):
                    break
                shortcut = entities_relationships[i+1].strip("<>-[]").split(":")[0]
                if re.findall(f"(\W|\\b){shortcut}(\W|\\b)",other_statement):
                    break
                rd -= 2

            match_statement = "MATCH " + ''.join(entities_relationships[ld:rd]).strip() + "\n" + other_statement.strip()
            match_statements += [match_statement]
            if returns == "temp" and match_statement.endswith("temp"):
                break
        return "\n".join(match_statements) + "\n" + return_statement
    
    def append_default_limitation(self, cypher):
        """
        如果cypher没有添加LIMIT，则添加默认LIMIT 50
        :param
            cypher(str):
                未处理的cypher
        :return
            cypher(str)
                处理后的cypher
        """
        if re.search("LIMIT \d+", cypher) is None:
            cypher += "\nLIMIT 50"
        return cypher
    
    def append_default_time_condition(self, cypher):
        """
        如果match中涉及到stock_data,trade_data等涉及到历史数据的tag，并且where中没有添加与之相关的时间条件，
        则添加默认的时间条件，.`date` = date() （今天）
        :param
            cypher(str):
                未处理的cypher
        :return
            cypher(str)
                处理后的cypher
        """
        sub_cyphers = cypher.split("MATCH ")
        new_cypher = ""
        for sub_sypher in sub_cyphers:
            if "WHERE " not in sub_sypher:
                if sub_sypher:
                    new_cypher += "MATCH " + sub_sypher
                continue
            sub_sypher = "MATCH " + sub_sypher
            match_statement, where_statement = sub_sypher.split("WHERE ")
            if ("stock_data" in match_statement or "trade_data" in match_statement) and ("date" not in where_statement):
                prefix = "sd.stock_data.`date` == date() AND " if "stock_data" in match_statement else "td.trade_data.`date` == date() AND "
                where_statement = prefix + where_statement
            if ("stock_financial_information" in match_statement) and ("year" not in where_statement):
                prefix = "sfi.stock_financial_information.year == date().year AND "
                where_statement = prefix + where_statement
            new_cypher += match_statement + "WHERE " + where_statement
        return new_cypher
    
    def append_default_time_return(self, cypher):
        """
        如果match中涉及到stock_data,trade_data等涉及到历史数据的tag，并且return中没有添加与之相关的时间条件，
        则添加默认的时间返回，RETURN sd.stock_data.`date` 
        :param
            cypher(str):
                未处理的cypher
        :return
            cypher(str)
                处理后的cypher
        """
        data_dic = {"sd": "stock_data", "td": "trade_data"}
        other_cypher0, return_cypher = cypher.split("RETURN ", 1)
        return_cypher, other_cypher1 = return_cypher.split("\n", 1)
        # print("RETURN CYPHER: ", return_cypher)
        for ret in return_cypher.split(","):
            shortcut = ret.split(".")[0]
            if shortcut in data_dic:
                date_ret = ".".join([shortcut, data_dic[shortcut], "`date`"])
                if date_ret not in return_cypher:
                    return_cypher = f"{date_ret}," + return_cypher
        return other_cypher0 + "RETURN " + return_cypher + "\n" + other_cypher1
    
    def remove_return_redundancy(self, cypher):
        """
        去除冗余的RETURN内容
        第一种冗余在于临时变量temp，此时会搜索cypher中与temp有关的WITH语句并把其计算方式保留下来，
        然后搜索RETURN的其它内容里是否有与WITH中的计算方式有重复的部分。
        例如RETURN temp, sd.sotck_data.closing_price，与temp有关的WITH语句是WITH avg(sd,sotck_data.closing_price) AS temp,
        则此时RETURN的sd.sotck_data.closing_price会被删除
        第二种冗余在于return_map.json中的代指变量未被替换，例如m.name，n.y等，如果不删除会导致查询语法错误
        :param
            cypher(str):
                未处理的cypher
        :return
            cypher(str)
                处理后的cypher
        """
        other_cypher0, return_cypher = cypher.split("RETURN ", 1)
        return_cypher, other_cypher1 = return_cypher.split("\n", 1)
        temp_vars = []
        returns = return_cypher.strip().split(",")
        for ret in returns:
            if ret.startswith("n.") or ret.startswith("m."):
                temp_vars += [ret]
            if ret.startswith("temp"):
                vars = re.findall(r"WITH .*?\((.*?)\) AS " + ret, cypher)
                temp_vars += [var.strip() for var in vars]
        for ret in returns:
            if ret in temp_vars:
                returns.remove(ret)
        return_cypher = ",".join(returns)
        return other_cypher0 + "RETURN " + return_cypher + "\n" + other_cypher1
    
    def date_wrapper(self, cypher):
        """
        处理所有的日期有关的条件
        匹配所有的形如date(),date()-1,date('2022-2-22')等日期条件，将其转化为规范化的日期，否则可能不会使用到日期上的索引
        对于所有的未明确的日期date()，向其中补充当天的日期
        对于所有的涉及到计算的date()-1，首先计算出具体日期，将其填回date中
        最后，所有的日期明确的date会向前回退，直到该日期是工作日位置
        :param
            cypher(str):
                未处理的cypher
        :return
            cypher(str)
                处理后的cypher
        """
        TODAY = get_today()

        def repl(match_obj):
            base_date, delta_date = match_obj.groups()

            if base_date:
                base_date = re.sub(r"(\s|')", "", base_date)
                # 补充残缺年份的日期
                nums = base_date.split("-")
                if len(nums) == 2:
                    month, day = nums
                    year = str(TODAY.year) if datetime.date(TODAY.year, int(month), int(day)) < TODAY else str(TODAY.year - 1)
                    base_date = str(year) + "-" + base_date
                base_date = datetime.datetime.strptime(base_date, "%Y-%m-%d").date()
            else:
                base_date = TODAY

            if delta_date:
                delta_date = int(delta_date)
                base_date -= datetime.timedelta(days = delta_date)

            try:
                while not cal.is_working_day(base_date):
                    base_date -= datetime.timedelta(days = 1)
            except:
                pass

            if base_date == datetime.date.today():
                now = datetime.datetime.now()
                if now.time() < datetime.time(9,30):
                    base_date -= datetime.timedelta(days=1)

            
            return f"date('{str(base_date)}')"
        
        cal = China()
        reg = re.compile(r"date\((\s?'(?:\d{4}-)?\d{1,2}-\d{1,2}')?\s?\)(?:\s*-\s*(\d+))?")

        return re.sub(reg, repl, cypher)

