from keywords2where import match_analytic_structure_for_where, splice_match_into_where_statement
from variates_alignment import extract_entities_in_match, align_entity_for_where, align_entity_for_return


def match_analytic_structure():
    """
    搜索解析式设计表以匹配解析式结构。本模块涉及变量对齐（e.g. s:stock 和 n.name 如何对齐 s 和 n）。
    :param:
        keywords ('dict'):
            用于匹配解析式设计表的关键词。
            example: {"entities": ["股票"], "properties": ["开盘价"], "time": ["今日"], "conditions": ["<property>高于<entity>", "今日"], "returns": ["的<entity>"]}
    :return:
        structures ('dict'):
            由解析式设计表解析出的 cypher 结构。
            example: {"match": ["MATCH (sd:stock_data)"],
                      "where": ["WHERE sd.name == <value>
                                 WITH sd.opening_price as temp
                                 MATCH (sd:stock_data)
                                 WHERE sd.opening_price >temp",
                                "WHERE sd.date == date()"],
                      "return": ["RETURN sd.name"],
                      "order_by": [],
                      "limit": []}
    """
    keywords = {"entities": ["股票"], "properties": ["开盘价"],
                "conditions": [{"condition": "<property>高于<entity>", "vareities": {"property": ["开盘价"], "entity": ["国泰君安"]}},
                               {"condition": "今日", "vareities": {}}],
                "returns": [{"return": "的<entity>", "vareities": {"entity": ["股票"]}}]}
    match_analytic_expression = 'MATCH (s:stock)-->(sd:stock_data)'
    return_analytic_expression = 'RETURN n.code, n.name'

    # 抽取 match 语句中的变量与实体
    entities_in_match = extract_entities_in_match(match_analytic_expression)

    # 匹配解析语句
    where_analytic_expressions = match_analytic_structure_for_where(keywords)
    # 若有 with，拼接 match 语句
    where_analytic_expressions = splice_match_into_where_statement(match_analytic_expression, where_analytic_expressions)

    # 对齐 where 语句中的变量
    where_potential_results=align_entity_for_where(entities_in_match, where_analytic_expressions, keywords)
    # 对齐 return 语句中的变量
    return_potential_results=align_entity_for_return(entities_in_match, return_analytic_expression, keywords)
    
    # 组合解析式结构
    structure={}
    structure['where']=where_potential_results
    structure['return']=return_potential_results
    
    print("structure: ", structure)

if __name__ == "__main__":
    match_analytic_structure()
