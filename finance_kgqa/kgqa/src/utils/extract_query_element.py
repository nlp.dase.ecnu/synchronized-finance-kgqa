"""
启发式第一步：匹配出query中包含的模板
"""

import os

import jieba
import re
import json
import time

basedir = os.path.dirname(__file__)


class ExtractQueryElementModel:
    """
    匹配出query中包含的模板类
    """

    def __init__(self, dicts):
        self.user_dict = os.path.join(basedir, "../../resources/parse/user_dict.txt")
        jieba.load_userdict(self.user_dict)
        self.entities_map = dicts["entities_map"]
        self.properties_map = dicts["properties_map"]
        self.properties = dicts["properties"]
        self.entities = dicts["entities"]
        self.times = dicts["times"]
        self.timespans = dicts["timespans"]
        self.where_entities_map = dicts["where_entities_map"]
        self.where_properties_map = dicts["where_properties_map"]
        self.where_regular = dicts["where_regular"]
        self.where_regular_special = dicts["where_regular_special"]
        self.return_regular = dicts["return_regular"]
        self.return_entities = dicts["return_entities"]
        self.tags = dicts["tags"]
        self.conjunction = dicts["conjunction"]
        self.bool_datas = dicts["bool"]
        self.negatives = dicts["negative"]

        # 定义属性、数值、字符串的正则表达式
        self.entity = "|".join(self.entities)
        self.property = "|".join(self.properties)
        self.rtime = "|".join(self.times)
        self.number = "\d+\.\d+%|\d+\.\d+|\d+%|几|\d+"
        self.string = ".+"
        self.timespan = "|".join(self.timespans)
        self.year = "\d{4}"
        self.month = "\d{1,2}"
        self.day = "\d{1,2}"
        self.return_entity = "|".join(self.return_entities)
        self.chinese_number = {"一": '1', "二": '2', "三": '3', "四": '4'}
        self.tag = "|".join(self.tags)
        self.conjunction = "|".join(self.conjunction)
        self.bool_data = "|".join(self.bool_datas)
        self.negative = "|".join(self.negatives)

        # 在init中先调用一次，能够大大减少后续的调用花费时间
        keywords = self.extract_query_element("开盘价高于国泰君安的股票")
        self.translate_zh2en = dicts["translate_zh2en"]

    # def __init__(self):
    #     self.user_dict = os.path.join(basedir, "../../resources/parse/user_dict.txt")
    #     self.jieba = jieba.load_userdict(self.user_dict)
    #     # 导入实体map以及属性map，遍历分词list判断是否存在实体/属性
    #     with open(os.path.join(basedir, "../../resources/parse/entities_map.json"), "r", encoding="utf-8") as ef, \
    #             open(os.path.join(basedir, "../../resources/parse/properties_map.json"), "r", encoding="utf-8") as pf:
    #         self.entities_map = json.load(ef)
    #         self.properties_map = json.load(pf)
    #     # 取出属性名列表
    #     # properties = ["注册资本", "上市板块", "成立日期", "省份", "开盘价", "收盘价", "最高价", "最低价", "涨跌幅", "成交量", "涨停连板天数", "涨停", "红三兵", "净利润", "存货周转率"]
    #     with open(os.path.join(basedir, "../../resources/parse/where_properties.json"), "r", encoding="utf-8") as pf:
    #         properties_data = json.load(pf)
    #         flat_list = [re.escape(item) for sublist in properties_data.values() for item in sublist]
    #         # self.properties = set(flat_list)
    #         self.properties = flat_list
    #     # 取出实体名列表
    #     with open(os.path.join(basedir, "../../resources/parse/where_entities.json"), "r", encoding="utf-8") as pf:
    #         entities_data = json.load(pf)
    #         flat_list = [re.escape(item) for sublist in entities_data.values() for item in sublist]
    #         # self.entities = set(flat_list)
    #         self.entities = flat_list
    #     # 取出时间列表
    #     with open(os.path.join(basedir, "../../resources/parse/times.json"), "r", encoding="utf-8") as pf:
    #         time_data = json.load(pf)
    #         flat_list = [item for sublist in time_data.values() for item in sublist]
    #         self.times = flat_list
    #     # 取出时间span列表
    #     with open(os.path.join(basedir, "../../resources/parse/timespans.json"), "r", encoding="utf-8") as pf:
    #         timespan_data = json.load(pf)
    #         flat_list = [item for sublist in timespan_data.values() for item in sublist]
    #         self.timespans = set(flat_list)
    #     # 取出实体和属性的模糊词映射
    #     with open(os.path.join(basedir, "../../resources/parse/where_entities_map.json"), "r", encoding="utf-8") as pf:
    #         # self.entities = set(flat_list)
    #         self.where_entities_map = json.load(pf)
    #     with open(os.path.join(basedir, "../../resources/parse/where_properties_map.json"), "r",
    #               encoding="utf-8") as pf:
    #         # self.entities = set(flat_list)
    #         self.where_properties_map = json.load(pf)
    #
    #     # 根据where字典进行正则表达式映射，判断是否属于该where表达式
    #     with open(os.path.join(basedir, "../../resources/parse/where_regular.json"), "r", encoding="utf-8") as wr:
    #         self.where_regular = json.load(wr)
    #
    #     with open(os.path.join(basedir, "../../resources/parse/where_regular_special.json"), "r",
    #               encoding="utf-8") as wrs:
    #         self.where_regular_special = json.load(wrs)
    #
    #     # 根据return字典进行正则表达式映射，判断是否属于该return表达式
    #     with open(os.path.join(basedir, "../../resources/parse/return_regular.json"), "r", encoding="utf-8") as rr:
    #         self.return_regular = json.load(rr)
    #
    #     # 读入return专用的entity字典
    #     with open(os.path.join(basedir, "../../resources/parse/where_entities.json"), "r", encoding="utf-8") as ren:
    #         self.return_entities_dict = json.load(ren)
    #         # self.return_entities = [item for sublist in self.return_entities_dict.values() for item in sublist]
    #         self.return_entities = [re.escape(item) for sublist in self.return_entities_dict.values() for item in
    #                                 sublist]
    #
    #     # 读入tag字典
    #     with open(os.path.join(basedir, "../../resources/parse/tags.json"), "r", encoding="utf-8") as pf:
    #         tag_data = json.load(pf)
    #         flat_list = [item for sublist in tag_data.values() for item in sublist]
    #         self.tags = flat_list
    #
    #     # 读入连接词字典
    #     with open(os.path.join(basedir, "../../resources/parse/conjunctions.json"), "r", encoding="utf-8") as pf:
    #         conjunction_data = json.load(pf)
    #         flat_list = [item for sublist in conjunction_data.values() for item in sublist]
    #         self.conjunction = flat_list
    #
    #     # 读入形态指标
    #     with open(os.path.join(basedir, "../../resources/parse/bool_properties.json"), "r", encoding="utf-8") as pf:
    #         bool_property_data = json.load(pf)
    #         flat_list = [item for sublist in bool_property_data.values() for item in sublist]
    #         self.bool_datas = flat_list
    #
    #     # 读入否定词
    #     with open(os.path.join(basedir, "../../resources/parse/negative_words.json"), "r", encoding="utf-8") as pf:
    #         negative_data = json.load(pf)
    #         flat_list = [item for sublist in negative_data.values() for item in sublist]
    #         self.negatives = flat_list
    #
    #     with open(os.path.join(basedir, "../../resources/parse/translate_zh2en.json"), 'r', encoding='utf-8') as file:
    #         self.translate_zh2en = json.load(file)
    #
    #     # 定义属性、数值、字符串的正则表达式
    #     self.entity = "|".join(self.entities)
    #     self.property = "|".join(self.properties)
    #     self.rtime = "|".join(self.times)
    #     self.number = "\d+\.\d+%|\d+\.\d+|\d+%|几|\d+"
    #     self.string = ".+"
    #     self.timespan = "|".join(self.timespans)
    #     self.year = "\d{4}"
    #     self.month = "\d{1,2}"
    #     self.day = "\d{1,2}"
    #     self.return_entity = "|".join(self.return_entities)
    #     self.chinese_number = {"一": '1', "二": '2', "三": '3', "四": '4'}
    #     self.tag = "|".join(self.tags)
    #     self.conjunction = "|".join(self.conjunction)
    #     self.bool_data = "|".join(self.bool_datas)
    #     self.negative = "|".join(self.negatives)
    #
    #     # 在init中先调用一次，能够大大减少后续的调用花费时间
    #     value = self.extract_query_element("开盘价高于国泰君安的股票")
    def is_redundance(self, condition, conditions):
        """
        判断condition在conditions中是不是冗余的
        :param condition: condition
        :param conditions: conditions
        :return:
            是否冗余
        """
        for con in conditions:
            if con['condition'] != condition['condition'] \
                    and con['varieties'] == condition['varieties'] \
                    and condition['condition'] in con['condition']:
                return True
        return False

    def conditions_filter(self, conditions):
        """
        对conditions里面的condition进行去重
        :param conditions: conditions列表
        :return:
            conditions_after: 去重后的conditions列表
        """
        conditions_after = []
        for condition in conditions:
            if not self.is_redundance(condition, conditions):
                conditions_after.append(condition)
        return conditions_after

    def chinese_to_arabic(self, chinese_num):
        """
        将中文数字和阿拉伯数字的映射
        :param chinese_num: 中文数字
        :return:
            阿拉伯数字
        """
        chinese_nums = {'零': '0', '一': '1', '二': '2', '三': '3', '四': '4',
                        '五': '5', '六': '6', '七': '7', '八': '8', '九': '9', '十': '10'}
        return chinese_nums[chinese_num]

    def timespan_split(self, timespan):
        """
        将timespan拆分为数字+单位的格式
        :param timespan: timespan
        :return:
            result: {'num': 数字, 'unit': 单位}
        """
        result = {}
        timespan = str(timespan)
        matches = re.findall(r'([一二三四五六七八九十\d]+)[\s]*(小时|分钟|天|日|周|月|年)', timespan)
        if matches:
            for match in matches:
                if match[0].isdigit():
                    num = match[0]
                else:
                    num = self.chinese_to_arabic(match[0])
                result['num'] = str(num)
                result['unit'] = match[1]
        return result

    def find_all_entities2dict(self, query):
        """
        找到query中的entities，并返回字典
        :param query: 用户query
        :return:
            entities_dict: {entity_type: [entity_name]}
        """
        pattern = "({entity})".format(entity=self.entity)
        matches = re.finditer(pattern, query)
        entities_dict = {}
        for match in matches:
            entity_name = self.where_entities_map[match.group(0)]
            if entity_name in self.entities_map:
                entity_type = self.entities_map[entity_name]
            else:
                continue
            entity_type = self.translate_zh2en[entity_type]
            if entity_type in entities_dict:
                if entity_name not in entities_dict[entity_type]:
                    entities_dict[entity_type].append(entity_name)
            else:
                entities_dict[entity_type] = [entity_name]
        return entities_dict

    def extract_query_element(self, query):
        """
        主方法：提取query中包含的启发式模板
        :param query: 用户query
        :return:
            keywords: {"entities": [], "properties": [], "time": [], "conditions": [], "returns": []}
        """
        # 将用户字典导入jieba分词并对 query 分词
        phrases = jieba.lcut(query)
        print(phrases)

        keywords = {"entities": [], "properties": [], "time": [], "conditions": [], "returns": []}

        # 匹配query中包含的entities和properties
        for word in phrases:
            if (self.entities_map.get(word, "") != "" and self.entities_map.get(word, "") not in keywords["entities"]):
                keywords["entities"].append(self.entities_map.get(word, ""))
            if (self.properties_map.get(word, "") != ""):
                keywords["properties"].append(word)

        matched_entities = {}
        matched_time = {}
        matched_bool = {}

        # 对query中的where启发式模板进行匹配
        for reg, data in self.where_regular.items():
            pattern = reg.format(entity=self.entity, property=self.property, string=self.string, number=self.number,
                                 time=self.rtime,
                                 timespan=self.timespan, \
                                 year=self.year, month=self.month, day=self.day, tag=self.tag,
                                 conjunction=self.conjunction, \
                                 bool=self.bool_data, negative=self.negative)
            matches = re.finditer(pattern, query)
            for match in matches:
                con = {"condition": data["condition"], "varieties": {}}
                if data["condition"] != "<entity>(和<entity>)+":
                    for variety, iters in data["variety"].items():
                        con["varieties"][variety] = []
                        for iter in iters:
                            val = match.group(iter)
                            if variety == "entity":
                                val = self.where_entities_map[match.group(iter)]
                                matched_entities[val] = matched_entities.get(val, 0) + 1
                            elif variety == "bool":
                                matched_bool[match.group(iter)] = matched_bool.get(match.group(iter), 0) + 1
                            elif variety == "time":
                                matched_time[match.group(iter)] = matched_time.get(match.group(iter), 0) + 1
                            elif variety == "property":
                                val = self.where_properties_map[match.group(iter)]
                            con["varieties"][variety].append(val)
                else:
                    con["varieties"]["entity"] = []
                    for e in match.groups():
                        if e != None:
                            val = self.where_entities_map[e]
                            matched_entities[val] = matched_entities.get(val, 0) + 1
                            con["varieties"]["entity"].append(val)
                keywords["conditions"].append(con)

        for reg, data in self.where_regular_special.items():
            pattern = reg.format(entity=self.entity, number=self.number, year=self.year, month=self.month, day=self.day, \
                                 bool=self.bool_data)
            matches = re.finditer(pattern, query)
            if data["condition"] == "<entity>":
                for match in matches:
                    val = self.where_entities_map[match.group(0)]
                    if matched_entities.get(val, 0) == 0:
                        con = {"condition": "<entity>", "varieties": {"entity": [val]}}
                        keywords["conditions"].append(con)
                    else:
                        matched_entities[val] -= 1
            elif data["condition"] == "<bool>":
                for match in matches:
                    if matched_entities.get(match.group(0), 0) == 0:
                        con = {"condition": "<bool>", "varieties": {"property": [match.group(0)]}}
                        keywords["conditions"].append(con)
                    else:
                        matched_entities[match.group(0)] -= 1
            else:
                for match in matches:
                    if matched_time.get(match.group(0), 0) == 0:
                        con = {"condition": data["condition"], "varieties": {}}
                        for variety, iters in data["variety"].items():
                            con["varieties"][variety] = []
                            for iter in iters:
                                val = match.group(iter)
                                if variety == "number":
                                    if val == "一" or val == "二" or val == "三" or val == "四":
                                        val = self.chinese_number[val]
                                con["varieties"][variety].append(val)
                        keywords["conditions"].append(con)
                    else:
                        matched_time[match.group(0)] -= 1

        # 对timespan进行拆分
        for condition_varieties in keywords['conditions']:
            for key in condition_varieties['varieties'].keys():
                if key == 'timespan':
                    value = condition_varieties['varieties']['timespan']
                    timespan_split_value = self.timespan_split(value)
                    if timespan_split_value != {}:
                        condition_varieties['varieties']['timespan'] = timespan_split_value
                    else:
                        condition_varieties['varieties']['timespan'] = value
                else:
                    continue

        # 对query中的return启发式模板进行匹配
        for reg, data in self.return_regular.items():
            pattern = reg.format(entity=self.return_entity, property=self.property, string=self.string,
                                 number=self.number, time=self.rtime,
                                 timespan=self.timespan,
                                 year=self.year, month=self.month, day=self.day, tag=self.tag)
            matches = re.finditer(pattern, query)
            for match in matches:
                con = {"return": data["condition"], "varieties": {}}
                for variety, iters in data["variety"].items():
                    con["varieties"][variety] = []
                    for iter in iters:
                        if variety == "property":
                            val = self.where_properties_map[match.group(iter)]
                        elif variety == "entity":
                            val = self.where_entities_map[match.group(iter)]
                        else:
                            val = match.group(iter)
                        # con["varieties"][variety].append(self.find_value_key(self.return_entities, match.group(iter)))
                        con["varieties"][variety].append(val)
                keywords["returns"].append(con)

        # 对conditions做后处理，去除重复元素
        keywords["conditions"] = (self.conditions_filter(keywords["conditions"]))
        # keywords["returns"] = (self.conditions_filter(keywords["returns"]))
        return keywords


if __name__ == '__main__':
    query = "2021年9月23日国泰君安和中信证券的开盘价和收盘价和3日平均成交额"
    extract_query_element_model = ExtractQueryElementModel()
    # query = "2021年9月23日国泰君安和中信证券的开盘价和收盘价和3日平均成交额"

    # query0 = "国泰君安的开盘价。"
    # print('query', query0)
    # value0 = extract_query_element_model.extract_query_element(query0)
    # print('解析结果：', value0)
    #
    # query1 = "国泰君安的开盘价情况"
    # print('query', query1)
    # value1 = extract_query_element_model.extract_query_element(query1)
    # print('解析结果：', value1)

    # query2 = "十天内涨停过的股票"
    # print('query', query2)
    # value2 = extract_query_element_model.extract_query_element(query2)
    # print('解析结果：', value2)

    # query3 = "国泰君安十天内涨跌幅"
    # print('query', query3)
    # value3 = extract_query_element_model.extract_query_element(query3)
    # print('解析结果：', value3)
    #
    # query3 = "国泰君安10天内涨跌幅"
    # print('query', query3)
    # value3 = extract_query_element_model.extract_query_element(query3)
    # print('解析结果：', value3)
    #
    # query4 = "国泰君安本月内涨跌幅"
    # print('query', query4)
    # value4 = extract_query_element_model.extract_query_element(query4)
    # print('解析结果：', value4)

    # query = '国泰君安和中信证券的3日累计涨幅'
    # print('query', query)
    # value = extract_query_element_model.extract_query_element(query)
    # print('解析结果：', value)

    query = '国泰君安的开盘价'
    print('query', query)
    value = extract_query_element_model.find_all_entities2dict(query)
    print('解析结果：', value)

    query = '国君的开盘价'
    print('query', query)
    value = extract_query_element_model.find_all_entities2dict(query)
    print('解析结果：', value)

    query = '国泰君安和中信证券的温度'
    print('query', query)
    value = extract_query_element_model.find_all_entities2dict(query)
    print('解析结果：', value)

    query = '中信证券和国君欧阳璐和人工智能的温度'
    print('query', query)
    value = extract_query_element_model.find_all_entities2dict(query)
    print('解析结果：', value)

    # print(json.dumps(value, indent=4, ensure_ascii=False))
