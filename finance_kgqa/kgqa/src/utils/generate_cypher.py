import re
import time
from itertools import combinations, permutations


class GenerateCypherModel:
    def __init__(self):
        pass

    def subset_of_where(self, where_list):
        all_subsets_all = list(permutations(where_list))
        return all_subsets_all

    def where_which_table(self, string):
        match = re.search(r'WHERE (.*?)\.', string)
        if match:
            result = match.group(1)
        return result

    def sort_where(self, structures_where, way=2):
        sents_list = []
        for sw in structures_where:
            sents = sw[0].split("\n")
            sents_list.append(sents)
        sorted_structures = []
        # way1: 找到关于时间的where，将其拼接到别的where语句中，组合成一个语句
        if way == 1:
            where_time = \
            [i for i in sents_list if len(i) == 1 and i[0].startswith('WHERE') and ('date' in i[0] or 'time' in i[0])][
                0][0]
            for i in range(len(sents_list)):
                for j in range(len(sents_list[i])):
                    if sents_list[i][j].startswith('WHERE') and (
                            'date' in sents_list[i][j] or 'time' in sents_list[i][j]):
                        continue
                    elif sents_list[i][j].startswith('WHERE'):
                        sorted_structures.append(sents_list[i][j])
                        sorted_structures.append(where_time)
                    else:
                        sorted_structures.append(sents_list[i][j])
            sorted_structures = ['\n'.join(sorted_structures)]
        # way2: 枚举所有可能的拼接
        elif way == 2:
            all_subsets = self.subset_of_where(sents_list)
            for subsets in all_subsets:
                sub = '\n'.join([subset[0] for subset in subsets])
                sorted_structures.append(sub)
        return sorted_structures

    def generate_cypher(self, structures):
        # 拼接除where以外的其他语句
        concat_match = self.concat_structures('\n'.join(structures['match']), "MATCH")[0]
        concat_return = self.concat_structures('\n'.join(structures['return']), "RETURN")[0]
        concat_order_by = '\n'.join([item for sublist in structures['order_by'] for item in sublist])
        concat_limit = '\n'.join(structures['limit'])
        # 对where进行特殊处理
        sorted_where = self.sort_where(structures['where'])
        concats = []
        for where in sorted_where:
            if where != '':
                concat_where_list = self.concat_structures(where, "WHERE")
            else:
                concat_where_list = ['']
            for concat_where in concat_where_list:
                if concat_where == '':
                    concat = concat_match + '\n' + concat_return + '\n' + concat_order_by + '\n' + concat_limit
                else:
                    concat = concat_match + '\n' + concat_where + '\n' + concat_return + '\n' + concat_order_by + '\n' + concat_limit
                concats.append(concat)
        return list(set(concats))

    def not_contained(self, str1, str2):
        # 判断str2第一个WHERE短句在str1中是否出现
        extracted_statement = ''
        match = re.search(r'WHERE\s+(.*?)(?:\s+AND|\s+WITH|\s*$)', str2)
        if match:
            extracted_statement = match.group(1)
            if extracted_statement in str1:
                return (False, extracted_statement)
            else:
                return (True, extracted_statement)
        else:
            return (True, extracted_statement)

    def concat_structures(self, str, keyword):
        # print('str', str)
        if keyword == "RETURN":
            concat_word = ","
        elif keyword == "WHERE":
            concat_word = "AND"
            str = "\nWHERE".join(str.split("WHERE")).strip()
        else:
            concat_word = "AND"
        sents = str.split("\n")
        prev = [""]
        cypher = [""]
        cyphers = [""]
        for sent in sents:
            if (not sent.strip()):
                continue
            # if keyword == "WHERE":
            #     print("语句：" + sent)
            head = sent.split()[0]
            list_len = len(prev)
            for i in range(list_len):
                if "WITH" not in sent:
                    if prev[i] == keyword and head == keyword:
                        cypher[i] = cypher[i] + " " + concat_word + " " + " ".join(sent.split()[1:])
                    else:
                        if cypher[i].strip():
                            cyphers[i] = cyphers[i] + "\n" + cypher[i].strip()
                        cypher[i] = sent
                    prev[i] = head
                else:
                    sent_list = sent.split()
                    # 前项不为where，不用拼接
                    if (prev[i] != "WHERE"):
                        if (cypher[i].strip()):
                            cyphers[i] = cyphers[i] + "\n" + cypher[i].strip()
                        prev[i] = "WITH"
                        # cypher[i] = sent
                        cyphers[i] = cyphers[i] + "\n" + sent
                        cypher[i] = ""
                    else:
                        # print(1)
                        # print(where_index[-1])
                        # print(total_index[-1])
                        prev.append("WHERE")
                        # WHERE开头WITH结尾
                        if (head == "WHERE"):
                            cypher.append(cypher[i])
                            cypher[i] = cypher[i] + " AND " + " ".join(sent_list[1:])
                            cyphers[i] = cyphers[i] + "\n" + cypher[i]
                            cyphers.append(cyphers[i])
                            cypher[i] = ""
                            prev[i] = "WITH"
                        # WITH开头WITH结尾
                        else:
                            cyphers[i] = cyphers[i] + "\n" + cypher[i] + " " + sent
                            cyphers.append(cyphers[i])
                            cypher.append(cypher[i])
                            cypher[i] = ""
                            prev[i] = "WITH"
            # for i in range(list_len):
            #     if "WITH" not in sent:
            #         if prev[i] == keyword and head == keyword:
            #             contain = self.not_contained(cypher[i], sent)
            #             if contain[0]:
            #                 cypher[i] = cypher[i] + " " + concat_word + " " + " ".join(sent.split()[1:])
            #             else:
            #                 cypher[i] = cypher[i] + " " + " ".join(sent.split()[1 + len(contain[1].split()):])
            #         else:
            #             if cypher[i].strip():
            #                 cyphers[i] = cyphers[i] + "\n" + cypher[i].strip()
            #             cypher[i] = sent
            #         prev[i] = head
            #     else:
            #         where_index = []
            #         total_index = []
            #         sent_list = sent.split()
            #         # 提取where的index
            #         for j in range(len(sent_list)):
            #             if sent_list[j] == "WHERE":
            #                 where_index.append(j)
            #                 total_index.append(j)
            #             elif sent_list[j] == "WITH":
            #                 total_index.append(j)
            #         if (prev[i] != "WHERE"):
            #             if (cypher[i].strip()):
            #                 cyphers[i] = cyphers[i] + "\n" + cypher[i].strip()
            #             # 如果以WITH结尾
            #             if (where_index == [] or where_index[-1] != total_index[-1]):
            #                 prev[i] = "WITH"
            #                 # cypher[i] = sent
            #                 cyphers[i] = cyphers[i] + "\n" + sent
            #                 cypher[i] = ""
            #             # 如果以WHERE结尾，将末尾WHERE语句作为新的cypher，前置内容直接push
            #             else:
            #                 prev[i] = "WHERE"
            #                 cyphers[i] = cyphers[i] + "\n" + " ".join(sent_list[:where_index[-1]])
            #                 cypher[i] = " ".join(sent_list[where_index[-1]:])
            #         else:
            #             # print(1)
            #             # print(where_index[-1])
            #             # print(total_index[-1])
            #             prev.append("WHERE")
            #             # WHERE开头WHERE结尾
            #             if (head == "WHERE" and where_index[-1] == total_index[-1]):
            #                 end_contain = self.not_contained(cypher[i], " ".join(sent_list[where_index[-1]:]))
            #                 if end_contain[0]:
            #                     cypher.append(cypher[i] + " AND " + " ".join(sent_list[where_index[-1] + 1:]))
            #                 else:
            #                     cypher.append(cypher[i] + " " + " ".join(
            #                         sent_list[where_index[-1] + 1 + len(end_contain[1].split()):]))
            #                 contain = self.not_contained(cypher[i], sent)
            #                 if contain[0]:
            #                     cypher[i] = cypher[i] + " AND " + " ".join(sent_list[1:where_index[-1]])
            #                 else:
            #                     cypher[i] = cypher[i] + " " + " ".join(
            #                         sent_list[1 + len(contain[1].split()):where_index[-1]])
            #                 cyphers[i] = cyphers[i] + "\n" + cypher[i]
            #                 cyphers.append(cyphers[i])
            #                 cypher[i] = " ".join(sent_list[where_index[-1]:])
            #                 prev[i] = "WHERE"
            #             # WHERE开头WITH结尾
            #             elif (head == "WHERE" and where_index[-1] != total_index[-1]):
            #                 cypher.append(cypher[i])
            #                 contain = self.not_contained(cypher[i], sent)
            #                 if contain[0]:
            #                     cypher[i] = cypher[i] + " AND " + " ".join(sent_list[1:])
            #                 else:
            #                     cypher[i] = cypher[i] + " " + " ".join(sent_list[1 + len(contain[1].split()):])
            #                 cyphers[i] = cyphers[i] + "\n" + cypher[i]
            #                 cyphers.append(cyphers[i])
            #                 cypher[i] = sent
            #                 prev[i] = "WITH"
            #             # WITH开头没有WHERE没有WITH结尾
            #             elif(head == "WITH" and len(where_index) == 0):
            #                 cyphers[i] = cyphers[i] + "\n" + cypher[i] + " " + sent
            #                 cyphers.append(cyphers[i])
            #                 cypher.append(cypher[i])
            #                 cypher[i] = sent
            #                 prev[i] = "WITH"
            #             # WITH开头WHERE结尾
            #             elif (head == "WITH" and where_index[-1] == total_index[-1]):
            #                 end_contain = self.not_contained(cypher[i], " ".join(sent_list[where_index[-1]:]))
            #                 if end_contain[0]:
            #                     cypher.append(cypher[i] + " AND " + " ".join(sent_list[where_index[-1] + 1:]))
            #                 else:
            #                     cypher.append(cypher[i] + " " + " ".join(
            #                         sent_list[where_index[-1] + 1 + len(end_contain[1].split()):]))
            #                 cyphers[i] = cyphers[i] + "\n" + cypher[i] + " " + " ".join(sent_list[:where_index[-1]])
            #                 cyphers.append(cyphers[i])
            #                 cypher[i] = " ".join(sent_list[where_index[-1]:])
            #                 prev[i] = "WHERE"
            #             # WITH开头WITH结尾
            #             else:
            #                 cyphers[i] = cyphers[i] + "\n" + cypher[i] + " " + sent
            #                 cyphers.append(cyphers[i])
            #                 cypher.append(cypher[i])
            #                 cypher[i] = sent
            #                 prev[i] = "WITH"

        for i in range(len(prev)):
            cyphers[i] = (cyphers[i] + "\n" + cypher[i]).strip()
        # print('cyphers', cyphers)
        return cyphers


if __name__ == '__main__':

    dic_string = {"match": ["MATCH (sd:stock_data)"],
                "where": [["WHERE sd.name == '国泰君安'\nWITH sd.opening_price as temp\nMATCH (sd:stock_data)\nWHERE sd.opening_price > temp"],
                          ["WHERE sd.date == date()"]],
                          "return": ["RETURN sd.name"],
                          "order_by": [],
                          "limit": []}
    dic_string = {'match': ['match (s:stock)-[hshd:has_stock_historical_data]->(sd:stock_data)'],
                  'where': [['WHERE sd.date == date()'],
                            # ['WHERE s.name == "国泰君安"'],
                            ['WHERE s.name == "国泰君安" WITH sd.opening_price as temp match (s:stock)-[hshd:has_stock_historical_data]->(sd:stock_data) WHERE sd.opening_price > temp']],
                  'return': ['RETURN s.name', 'RETURN s'], 'order_by': [], 'limit': []}
    dic_string = {'match': ['MATCH (s:stock)<-[ico:is_chairman_of]-(cm:chairman)'],
                  'where': [['WHERE s.name == "贺青"', 'WHERE cm.name == "贺青"']],
                  'return': ['RETURN s.name'],
                  'order_by': [], 'limit': []}
    dic_string = {'match': ['MATCH (pof:public_offering_fund)'],
                  'where': [],
                  'return': ['RETURN pof.name'],
                  'order_by': [['ORDER BY pof.net_worth DESC LIMIT 3']],
                  'limit': []}
    dic_string = {'match': ['MATCH (pof:public_offering_fund)'],
                  'where': [['WHERE pof.net_worth > 1'], ['WITH pof.net_worth > 1 as temp'], ['WHERE pof.name == "中欧医疗健康混合c"']],
                  'return': ['RETURN temp'],
                  'order_by': [], 'limit': []}
    dic_string = {'match': ['MATCH (s:stock)-[hsd:has_stock_data]->(sd:stock_data)'],
                  'where': [
                      ['WHERE s.name == "国泰君安" WITH sd.closing_price as temp1 MATCH (s:stock)-[hsd:has_stock_data]->(sd:stock_data) WHERE s.name == "中信证券" WITH sd.closing_price as temp2 WITH ABS(temp1-temp2) as temp'],
                      ['WHERE sd.date == date()']],
                  'return': ['RETURN temp'],
                  'order_by': [], 'limit': []}

    print(dic_string)
    start_time = time.time()
    generate_cypher_model = GenerateCypherModel()
    end_time = time.time()
    print('加载类时间：', end_time - start_time)

    start_time = time.time()
    cyphers = generate_cypher_model.generate_cypher(dic_string)
    end_time = time.time()
    print('解析时间：', end_time - start_time)
    print('解析结果')
    for cypher in cyphers:
        print(cypher)

    # str = 'WHERE sd.date == date()\nWHERE s.name == "国泰君安" WITH sd.opening_price as temp match (s:stock)-[hshd:has_stock_historical_data]->(sd:stock_data) WHERE sd.opening_price > temp\nWHERE s.name == "国泰君安"'
    # # str = 'WHERE s.name == "国泰君安" WITH sd.opening_price as temp match (s:stock)-[hshd:has_stock_historical_data]->(sd:stock_data) WHERE sd.opening_price > temp\nWHERE sd.date == date()'
    # print(concat_structures(str, "WHERE"))