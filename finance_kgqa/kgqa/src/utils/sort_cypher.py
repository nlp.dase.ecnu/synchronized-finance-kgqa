"""
该文件用于使用bert向量相似度，对启发式生成的多个cypher进行排序
（已弃用）
"""
import torch
from transformers import BertTokenizer, BertModel
from sklearn.metrics.pairwise import cosine_similarity
import os

basedir = os.path.dirname(__file__)
class SortCypherModel():
    """
    对启发式生成的多个cypher进行排序的类
    """
    def __init__(self):
        model_path = os.path.join(basedir, '../../resources/bert-base-multilingual-uncased')
        self.tokenizer = BertTokenizer.from_pretrained(model_path)
        self.model = BertModel.from_pretrained(model_path)

    def calculate_semantic(self, sentence):
        """
        对输入的sentence提取相应的bert向量
        :param sentence: 输入的语句
        :return:
            cls_vector: bert向量
        """
        inputs = self.tokenizer(sentence, return_tensors='pt', padding=True, truncation=True)
        with torch.no_grad():
            outputs = self.model(**inputs)
        cls_vector = outputs.last_hidden_state[0][0]
        return cls_vector

    def sort_cypher(self, query, cyphers):
        '''
        sort_cypher
        :param query: 问句
        :param cyphers: 生成的cypher列表
        :return:
            max_cypher: 最相似的cypher语句
            sorted_cyphers: 根据相似度从大到小排序好的cypher语句列表
        '''
        query_v = self.calculate_semantic(query)
        similarity = []
        for cypher in cyphers:
            cypher_v = self.calculate_semantic(cypher)
            sim = cosine_similarity([query_v], [cypher_v])[0][0]
            similarity.append(sim)
        max_value = max(similarity)
        max_index = similarity.index(max_value)
        max_cypher = cyphers[max_index]
        combined = list(zip(cyphers, similarity))
        sorted_cyphers = sorted(combined, key=lambda x: x[1], reverse=True)
        sorted_cyphers = [item[0] for item in sorted_cyphers]
        return max_cypher, sorted_cyphers


if __name__ == '__main__':
    query = '国泰君安的开盘价'
    cyphers = [
        '''
        match (s:stock)-[hsd:has_stock_data]->(sd:stock_data)
WHERE sd.date == date() AND s.name == "国泰君安"
RETURN sd.closing_price''',
        '''
        match (s:stock)-[hsd:has_stock_data]->(sd:stock_data)
WHERE s.name == "国泰君安" AND sd.date == date()
RETURN sd.closing_price
        '''
    ]
    sortcyphermodel = SortCypherModel()
    max_cypher, sorted_cyphers = sortcyphermodel.sort_cypher(query, cyphers)
    print(max_cypher)
    print(sorted_cyphers)
