"""
存放对query和cypher的后处理步骤：
    将生成的cypher中的日期与query的日期对齐，以应对llm的日期幻觉问题
"""
import os
import json
import re
import datetime

TODAY = datetime.date.today()
basedir = os.path.dirname(__file__)


class QueryCypherPostprocessModel:
    """
    对query和cypher的后处理类
    """

    def __init__(self):
        self.date_re_3 = {
            r'\d{4}年\d{1,2}月\d{1,2}': '%Y年%m月%d',
            r'\d{4}/\d{1,2}/\d{1,2}': '%Y/%m/%d'
        }
        self.date_re_2 = {
            r'\d{1,2}月\d{1,2}': '%m月%d',
            r'\d{1,2}/\d{1,2}': '%m/%d'
        }

    def convert_date_format_in_text(self, text):
        """
        对用户问题中的日期进行格式规整
        :param text: 用户问题
        :return:
            text: 日期格式化后的用户问题
        """
        # 用户问题中给出了年月日
        for pattern, format in self.date_re_3.items():
            match = re.search(pattern, text)
            if match:
                original_date = match.group()
                original_datetime = datetime.datetime.strptime(original_date, format)
                formatted_date = original_datetime.strftime('%Y-%m-%d')
                text = text.replace(original_date, formatted_date)
        # 用户问题中给出了月日，没有年
        for pattern, format in self.date_re_2.items():
            match = re.search(pattern, text)
            if match:
                original_date = match.group()
                original_datetime = datetime.datetime.strptime(original_date, format)
                formatted_date = original_datetime.strftime('%m-%d')
                nums = formatted_date.split("-")
                month, day = nums
                year = str(TODAY.year) if datetime.date(TODAY.year, int(month), int(day)) < TODAY else str(
                    TODAY.year - 1)
                base_date = str(year) + "-" + formatted_date
                text = text.replace(original_date, base_date)
        return text

    def find_dates(self, query):
        """
        从日期格式化的句子中提取日期
        :param query: 输入句子
        :return:
            dates: 提取出来的日期列表
        """
        dates = re.findall(r'\d{4}-\d{2}-\d{2}', query)
        return dates

    def filling_date_cypher(self, cypher, dates_cypher, dates_query):
        """
        将query中的日期填充到cypher中
        :param cypher: 待填充日期的cypher
        :param dates_cypher: cypher中存在的日期列表
        :param dates_query: query中存在的日期列表
        :return:
            cypher: 日期填充完成后的cypher
        """
        for i, j in zip(dates_cypher, dates_query):
            cypher = cypher.replace(i, j)
        return cypher

    def postprocess(self, query, cypher):
        """
        主方法：对query和cypher进行后处理
        :param query: 输入query
        :param cypher: 输入cypher
        :return:
            预处理后的cypher
        """
        modified_query = self.convert_date_format_in_text(query)
        dates_query = self.find_dates(modified_query)
        # print(dates_query)
        dates_cypher = self.find_dates(cypher)
        # print(dates_cypher)
        if len(dates_query) == len(dates_cypher):
            return self.filling_date_cypher(cypher, dates_cypher, dates_query)
        else:
            if len(dates_query) == 1 and len(dates_cypher) > 1:
                dates_query_ = [dates_query[0] for i in range(len(dates_cypher))]
                return self.filling_date_cypher(cypher, dates_cypher, dates_query_)
        return cypher


if __name__ == '__main__':
    query_cypher_postprocess_model = QueryCypherPostprocessModel()

    query = '国泰君安2023年5月20日的开盘价'
    cypher = '''MATCH (s:stock)-[hsd:has_stock_data]->(sd:stock_data) WHERE s.stock.name == '国泰君安' AND sd.stock_data.`date` == date('2023-08-18') RETURN s.stock.name, sd.stock_data.`date`, sd.stock_data.opening_price LIMIT 50'''
    cypher_new = query_cypher_postprocess_model.postprocess(query, cypher)
    print(cypher_new)

    query = '国泰君安2023/05/20的开盘价'
    cypher = '''MATCH (s:stock)-[hsd:has_stock_data]->(sd:stock_data) WHERE s.stock.name == '国泰君安' AND sd.stock_data.`date` == date('2023-08-18') RETURN s.stock.name, sd.stock_data.`date`, sd.stock_data.opening_price LIMIT 50'''
    cypher_new = query_cypher_postprocess_model.postprocess(query, cypher)
    print(cypher_new)

    query = '国泰君安5月20日的开盘价'
    cypher = '''MATCH (s:stock)-[hsd:has_stock_data]->(sd:stock_data) WHERE s.stock.name == '国泰君安' AND sd.stock_data.`date` == date('2023-08-18') RETURN s.stock.name, sd.stock_data.`date`, sd.stock_data.opening_price LIMIT 50'''
    cypher_new = query_cypher_postprocess_model.postprocess(query, cypher)
    print(cypher_new)
