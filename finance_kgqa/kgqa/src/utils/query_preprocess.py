"""
存放对query的预处理步骤：
    将query中的中文数字转为阿拉伯数字
"""
import os
import cn2an
import json
import re
import warnings

basedir = os.path.dirname(__file__)


class QueryPreprocessModel:
    """
    对query的预处理类
    """

    def __init__(self, dicts):
        self.entities = sorted(dicts["entities_preprocess"], key=lambda x: len(x), reverse=True)
        self.properties = sorted(dicts["properties_preprocess"], key=lambda x: len(x), reverse=True)
        self.special_number_words = ['单一', '统一', '唯一', '一致', '一样', '一下', '同一', '一些']

    # def __init__(self):
    #     with open("../../resources/parse/where_entities.json", "r", encoding="utf-8") as pf:
    #         entities_data = json.load(pf)
    #         flat_list = [item for sublist in entities_data.values() for item in sublist]
    #         sorted_list = sorted(flat_list, key=lambda x: len(x), reverse=True)
    #         self.entities = sorted_list
    #     with open("../../resources/parse/where_properties.json", "r", encoding="utf-8") as pf:
    #         properties_data = json.load(pf)
    #         flat_list = [item for sublist in properties_data.values() for item in sublist]
    #         sorted_list = sorted(flat_list, key=lambda x: len(x), reverse=True)
    #         self.properties = sorted_list
    #     self.special_number_words = ['单一', '统一', '唯一', '一致', '一样', '一下', '同一', '一些']

    def query_mask_entities(self, query):
        """
        将query中含有的实体名称和属性名称进行mask，并按照句中出现顺序存放在一个列表中
        :param query: 用户问题
        :return:
            masked_query: 实体名称和属性名称进行mask后的用户query
            mask_entity_list: query中含有的实体名称和属性名称列表
        """
        mask_entity_list = []
        masked_query = query
        for entity in self.entities + self.properties + self.special_number_words:
            if entity in masked_query:
                mask_entity_list.append(entity)
                masked_query = masked_query.replace(entity, "[MASK]", 1)
        assert len(mask_entity_list) == masked_query.count("[MASK]")
        # 按照句中出现顺序排序
        mask_entity_list = sorted(mask_entity_list, key=lambda x: query.index(x))
        return masked_query, mask_entity_list

    def masked_query_cn2an(self, masked_query, transfer_mode='cn2an'):
        """
        调用cn2an模块，将句子中的中文数字和阿拉伯数字进行转换
        :param masked_query: 输入句子
        :param transfer_mode: 转换方向
        :return:
            转换后的句子
        """
        return cn2an.transform(masked_query, transfer_mode)

    def query_mask_filling(self, masked_query, mask_entity_list):
        """
        将mask后的句子进行回填
        :param masked_query: mask后的句子
        :param mask_entity_list: mask的实体名称和属性名称列表
        :return:
            回填后的query
        """
        assert len(mask_entity_list) == masked_query.count("[MASK]")
        for entity in mask_entity_list:
            masked_query = masked_query.replace('[MASK]', entity, 1)
        return masked_query

    def special_replace(self, query):
        """
        对query进行特殊处理
        :param query: 输入query
        :return:
            特殊处理后的query
        """
        query = query.replace('个亿', '亿')
        query = query.replace('个点', '%')
        query = query.replace('两', '2')
        return query

    def preprocess(self, query):
        """
        主方法：对query进行预处理
        :param query: 输入query
        :return:
            预处理后的query
        """
        # masked_query, mask_entity_list = self.query_mask_entities(query)
        # masked_query = self.special_replace(masked_query)
        # # print('masked_query', masked_query)
        # masked_query_cn = self.masked_query_cn2an(masked_query, 'an2cn')
        # # print('masked_query_cn', masked_query_cn)
        # masked_query_an = self.masked_query_cn2an(masked_query_cn, 'cn2an')
        # query_preprocessed = self.query_mask_filling(masked_query_an, mask_entity_list)
        # return query_preprocessed
        try:
            with warnings.catch_warnings():
                warnings.simplefilter("error")  # 将警告转换为异常
                masked_query, mask_entity_list = self.query_mask_entities(query)
                masked_query = self.special_replace(masked_query)
                # print('masked_query', masked_query)
                masked_query_cn = self.masked_query_cn2an(masked_query, 'an2cn')
                # print('masked_query_cn', masked_query_cn)
                masked_query_an = self.masked_query_cn2an(masked_query_cn, 'cn2an')
                query_preprocessed = self.query_mask_filling(masked_query_an, mask_entity_list)
                return query_preprocessed
        except Warning as e:
            print('preprocess warning:', e)
            return query
        except Exception as e:
            print('preprocess error:', e)
            return query


if __name__ == '__main__':
    query_preprocess_model = QueryPreprocessModel()

    query = '二零零一年三月四日三一重工的开盘价高于三千七百万的股票五百万'
    query_preprocessed = query_preprocess_model.preprocess(query)
    print(query)
    print(query_preprocessed)
    print()

    query = '最新净值大于1且最新规模大于10的基金'
    query_preprocessed = query_preprocess_model.preprocess(query)
    print(query)
    print(query_preprocessed)
    print()

    query = '二零零一年三月四日三一重工的开盘价的广发沪深300ETF联接A高于三千七百万的股票500万注册资本大于10亿的20万股票3千万的1百二十元10个点，十个点，10个亿，百分之10，百分之三十，高于8.45。持有比例高于百分之三点一，两百亿'
    query_preprocessed = query_preprocess_model.preprocess(query)
    print(query)
    print(query_preprocessed)
    print()

    query = '开盘价高于1005000的股票'
    query_preprocessed = query_preprocess_model.preprocess(query)
    print(query)
    print(query_preprocessed)
    print()

    query = '开盘价高于1000500的股票'
    query_preprocessed = query_preprocess_model.preprocess(query)
    print(query)
    print(query_preprocessed)
    print()

    query = '开盘价高于10005000的股票'
    query_preprocessed = query_preprocess_model.preprocess(query)
    print(query)
    print(query_preprocessed)
    print()

    query = '汽车零部件行业历史最低开盘价'
    query_preprocessed = query_preprocess_model.preprocess(query)
    print(query)
    print(query_preprocessed)
    print()
