"""
变量对齐模块。流程：从 MATCH 子句中抽取出变量与 tag，抽取where解析式中的属性，映射 properties 对应的可能的 entities，对齐实体变量，回填 <property> 槽
"""

import copy
import datetime
import json
import os
import unicodedata

from kgqa.src.utils.review import get_today


class VariatesAlignmentModel:
    """
    变量对齐模块，用于对齐 WHERE 子句中变量和 MATCH 中的变量

    :param:
        dicts (dict): 对齐需要用到的字典，具体可参考 kgqa/src/dict.py
    """

    def __init__(self, dicts):
        self.entities_map = dicts["entities_map"]
        self.properties_map = dicts["properties_map"]
        self.properties_map_en = dicts["properties_map_en"]
        self.translate_zh2en = dicts["translate_zh2en"]
        self.where_properties_map = dicts["where_properties_map"]
        self.where_entities_map = dicts["where_entities_map"]
        self.tags = dicts["tags"]

    # def __init__(self):
    #     # 读取字典文件
    #     basedir = os.path.dirname(__file__)
    #     entities_map_json_file_path = os.path.join(basedir, '../../resources/parse/entities_map.json')
    #     with open(entities_map_json_file_path, 'r', encoding='utf-8') as file:
    #         self.entities_map = json.load(file)
    #     properties_map_json_file_path = os.path.join(basedir, '../../resources/parse/properties_map.json')
    #     with open(properties_map_json_file_path, 'r', encoding='utf-8') as file:
    #         self.properties_map = json.load(file)
    #     properties_map_en_json_file_path = os.path.join(basedir, '../../resources/parse/tags_properties_map_en.json')
    #     with open(properties_map_en_json_file_path, 'r', encoding='utf-8') as file:
    #         self.properties_map_en = json.load(file)
    #     translate_zh2en_json_file_path = os.path.join(basedir, '../../resources/parse/translate_zh2en.json')
    #     with open(translate_zh2en_json_file_path, 'r', encoding='utf-8') as file:
    #         self.translate_zh2en = json.load(file)

    def extract_entities_in_match(self, match_analytic_expressions):
        """
        从 MATCH 子句中抽取实体
        :param:
            match_analytic_expressions (string):
                MATCH 子句
                example: "MATCH (sfi:stock_financial_information)<-[hsfi:has_stock_financial_information]-(s:stock)-[bt:belong_to]->(t:trade)"
        :return:
            entities_in_match (list):
                抽取出的实体
                example: [{'type': 'vertex', 'variate': 'sfi', 'entity': 'stock_financial_information'}, {'type': 'edge', 'variate': 'hsfi', 'entity': 'has_stock_financial_information'}, {'type': 'vertex', 'variate': 's', 'entity': 'stock'}, {'type': 'edge', 'variate': 'bt', 'entity': 'belong_to'}, {'type': 'vertex', 'variate': 't', 'entity': 'trade'}]
        """

        # print("match_analytic_expressions: ", match_analytic_expressions)

        # 将 match 语句中的 entity 提出
        entities_in_match = []
        vertex_entity_flag = False
        edge_entity_flag = False
        colon_flag = False  # 区分 ”:“ 前后
        entity_tmp = {"type": "", "variate": "", "entity": ""}

        # 遍历字符，筛选出类型、变量和tag
        for i in range(len(match_analytic_expressions)):
            if match_analytic_expressions[i] == '(':
                vertex_entity_flag = True
                continue
            if match_analytic_expressions[i] == '[':
                edge_entity_flag = True
                continue
            if match_analytic_expressions[i] == ')' or match_analytic_expressions[i] == ']':
                vertex_entity_flag = False
                edge_entity_flag = False
                colon_flag = False
                entities_in_match.append(entity_tmp)
                entity_tmp = {"type": "", "variate": "", "entity": ""}
            if vertex_entity_flag:
                entity_tmp["type"] = "vertex"
                if match_analytic_expressions[i] == ':':
                    colon_flag = True
                    continue
                if not colon_flag:
                    entity_tmp["variate"] += match_analytic_expressions[i]
                else:
                    entity_tmp["entity"] += match_analytic_expressions[i]
            if edge_entity_flag:
                entity_tmp["type"] = "edge"
                if match_analytic_expressions[i] == ':':
                    colon_flag = True
                    continue
                if not colon_flag:
                    entity_tmp["variate"] += match_analytic_expressions[i]
                else:
                    entity_tmp["entity"] += match_analytic_expressions[i]
        # print('\nentities_in_match: ', entities_in_match)
        return entities_in_match

    def align_entity_for_where(self, entities_in_match, where_analytic_expressions, keywords):
        """
        根据 MATCH 对齐 WHERE 子句中的变量。
        :param:
            entities_in_match (string):
                从 MATCH 子句中抽取出的实体
                example: [{'type': 'vertex', 'variate': 'sfi', 'entity': 'stock_financial_information'}, {'type': 'edge', 'variate': 'hsfi', 'entity': 'has_stock_financial_information'}, {'type': 'vertex', 'variate': 's', 'entity': 'stock'}, {'type': 'edge', 'variate': 'bt', 'entity': 'belong_to'}, {'type': 'vertex', 'variate': 't', 'entity': 'trade'}]
            where_analytic_expressions (list):
                WHERE 子句
                example: ['WHERE n.name == <entity>', 'WHERE n.quarter == <number>', 'WHERE n.year == <number>']
            keywords (dict):
                抽取出的 keywords 字典
                example: {'entities': ['行业', '股票'], 'properties': ['净利润'], 'time': [], 'conditions': [{'condition': '<entity>', 'varieties': {'entity': ['零售']}}, {'condition': '第<number>季度', 'varieties': {'number': ['2']}}, {'condition': '<number>年', 'varieties': {'number': 'returns': []}
        :return:
            where_potential_results (list):
                识别出的意图
                example: [['WHERE t.trade.name == "零售"'], ['WHERE sfi.stock_financial_information.quarter == 2'], ['WHERE sfi.stock_financial_information.year == 2023']]
        """

        # print('\nwhere 语句对齐实体变量：')
        # print('\nalign_entity_for_where 输入参数：')
        # print("entities_in_match: ", entities_in_match)
        # print("where_analytic_expressions: ", where_analytic_expressions)
        # print("keywords: ", keywords)

        # 抽取where解析式中的属性（<properties>）和值（<entity>, <time>等）
        where_analytic_expressions_with_slots, tags_from_where_analytic_expressions, properties_from_where_analytic_expressions, values_from_where_analytic_expressions = self.extract_properties_from_where(
            where_analytic_expressions, keywords)

        # 映射 properties 对应的可能的 entities
        entities_mapping_propertities_from_where_analytic_expressions = self.map_entities_with_properties(tags_from_where_analytic_expressions, properties_from_where_analytic_expressions)

        # 对齐实体变量
        entities_to_align = self.align_variates(where_analytic_expressions_with_slots, keywords, entities_mapping_propertities_from_where_analytic_expressions,
                                                properties_from_where_analytic_expressions, entities_in_match)

        # 回填 <property> 槽
        where_potential_results = self.backfill_properties_slots(where_analytic_expressions_with_slots, entities_to_align,
                                                                 values_from_where_analytic_expressions)

        return where_potential_results

    def extract_properties_from_where(self, where_analytic_expressions, keywords):
        """
        将 where 语句中的 variate.property 提出
        ['WHERE n.name == <entity> WITH <property> as temp WHERE <property> > temp', 'WHERE n.date = date()']
        ——>
        ['WHERE <property> == <entity> WITH <property> as temp WHERE <property> > temp', 'WHERE <property> = date()']
        [["name", "opening_price", "opening_price"], ["date"]]
        :param:
            where_analytic_expressions (list):
                WHERE 子句
                example: ['WHERE n.name == <entity>', 'WHERE n.quarter == <number>', 'WHERE n.year == <number>']
            keywords (dict):
                抽取出的 keywords 字典
                example: {'entities': ['行业', '股票'], 'properties': ['净利润'], 'time': [], 'conditions': [{'condition': '<entity>', 'varieties': {'entity': ['零售']}}, {'condition': '第<number>季度', 'varieties': {'number': ['2']}}, {'condition': '<number>年', 'varieties': {'number': 'returns': []}
        :return:
            where_analytic_expressions_with_slots (list):
                挖出槽的 WHERE 子句
                example: ['WHERE <property> == <entity>', 'WHERE <property> == <number>', 'WHERE <property> == <number>']
            tags_from_where_analytic_expressions (list):
                从 keywords 字典中抽取出的 tags
                len(tags_from_where_analytic_expressions) == len(where_analytic_expressions_with_slots)
                example: [[''], [''], ['']]
            properties_from_where_analytic_expressions (list):
                从 keywords 字典中抽取出的 properties
                len(properties_from_where_analytic_expressions) == len(where_analytic_expressions_with_slots)
                example: [['name'], ['quarter'], ['year']]
            values_from_where_analytic_expressions (dict):
                从 keywords 字典中抽取出的 varieties，即 WHERE 子句中的 value 部分，按 <entities>，<numbers>，<time>，<timespan>，<tags>，<string> 存储
                len(values_from_where_analytic_expressions) == len(where_analytic_expressions_with_slots)
                example: {'entities': [['零售'], [], []], 'numbers': [[], ['2'], ['2023']], 'time': [[], [], []], 'timespan': [[], [], []], 'tags': [[], [], []], 'string': [[], [], []]}
        """

        where_analytic_expressions_with_slots = []
        tags_from_where_analytic_expressions = []
        properties_from_where_analytic_expressions = []
        values_from_where_analytic_expressions = {"entities": [], "numbers": [], "time": [], "timespan": [], "tags": [], "string": []}
        for i in range(len(where_analytic_expressions)):
            entities_num = 0
            # properties_num = 0
            where_analytic_expressions_with_slots.append([])
            tags_from_where_analytic_expressions.append([])
            properties_from_where_analytic_expressions.append([])
            where_analytic_expression_words = where_analytic_expressions[i].split()

            # 将 WHERE 子句挖槽
            for j in range(len(where_analytic_expression_words)):
                if where_analytic_expression_words[j] == '<entity>':
                    entities_num += 1
                # if where_analytic_expression_words[j] == '<property>':
                #     properties_num += 1
                if where_analytic_expression_words[j] == 'WHERE' or where_analytic_expression_words[j] == 'AND':
                    continue
                if where_analytic_expression_words[j] == '<property>':
                    # properties_num += 1
                    tags_from_where_analytic_expressions[i].append('')
                    properties_from_where_analytic_expressions[i].append(self.translate_zh2en[keywords['conditions'][i]['varieties']['property'][0]])
                elif '.' in where_analytic_expression_words[j] and '()' not in where_analytic_expression_words[j]:
                    tmp_words = where_analytic_expression_words[j].split('.')
                    if tmp_words[0] == "<tag>":
                        tags_from_where_analytic_expressions[i].append(self.translate_zh2en[keywords['conditions'][i]['varieties']['tag'][0]])
                    else:
                        tags_from_where_analytic_expressions[i].append('')
                    properties_from_where_analytic_expressions[i].append(".".join(tmp_words[1:]))
                    where_analytic_expression_words[j] = '<property>'
            where_analytic_expressions_with_slots[i] = ' '.join(where_analytic_expression_words)

            values_from_where_analytic_expressions['entities'].append([])
            values_from_where_analytic_expressions['numbers'].append([])
            values_from_where_analytic_expressions['time'].append([])
            values_from_where_analytic_expressions['timespan'].append([])
            values_from_where_analytic_expressions['tags'].append([])
            values_from_where_analytic_expressions['string'].append([])

            # 将 keywords 中的“varieties”按照“entities”，“numbers”，“time”，“timespan”，“tags”，“string”分类存储
            if 'varieties' in keywords['conditions'][i] and 'entity' in keywords['conditions'][i]['varieties']:
                for j in range(len(keywords['conditions'][i]['varieties']['entity'])):
                    if keywords['conditions'][i]['varieties']['entity'][j] not in self.tags:
                        values_from_where_analytic_expressions['entities'][-1].append(keywords['conditions'][i]['varieties']['entity'][j])
                # “condition”里的“entity”数量不足以给<entity>赋值，则复制“entity”
                if len(keywords['conditions'][i]['varieties']['entity']) < entities_num:
                    for j in range(entities_num - len(keywords['conditions'][i]['varieties']['entity'])):
                        values_from_where_analytic_expressions['entities'][-1].append(keywords['conditions'][i]['varieties']['entity'][0])
            if 'varieties' in keywords['conditions'][i] and 'number' in keywords['conditions'][i]['varieties']:
                if "<time>" in where_analytic_expressions[i]:
                    values_from_where_analytic_expressions['time'][-1].append(keywords['conditions'][i]['varieties']['number'])
                else:
                    for j in range(len(keywords['conditions'][i]['varieties']['number'])):
                        number_data = self.get_number_data(keywords['conditions'][i]['varieties']['number'][j])
                        print("number_data: ", number_data)
                        values_from_where_analytic_expressions['numbers'][-1].append(number_data)
            if 'varieties' in keywords['conditions'][i] and 'time' in keywords['conditions'][i]['varieties']:
                for j in range(len(keywords['conditions'][i]['varieties']['time'])):
                    year_month_day = self.get_year_month_day_from_str(keywords['conditions'][i]['varieties']['time'][j])
                    print("#################year_month_day####################")
                    print(year_month_day)
                    if year_month_day["month"] == "":
                        year_month_day["month"] = "01"
                    if year_month_day["day"] == "":
                        year_month_day["day"] = "01"
                    if year_month_day["year"] == "":
                        year_month_day["year"] = str(get_today().year)
                    # time = "-".join([year_month_day["year"], year_month_day["month"], year_month_day["day"]])
                    time = [year_month_day["year"], year_month_day["month"], year_month_day["day"]]
                    values_from_where_analytic_expressions['time'][-1].append(time)
            if 'varieties' in keywords['conditions'][i] and 'timespan' in keywords['conditions'][i]['varieties']:
                if type(keywords['conditions'][i]['varieties']['timespan']) == dict:
                    timespan_num = keywords['conditions'][i]['varieties']['timespan']['num']
                    timespan_unit = keywords['conditions'][i]['varieties']['timespan']['unit']
                else:
                    if any(word in keywords['conditions'][i]['varieties']['timespan'][0] for word in ["本", "此", "这", "该"]):
                        timespan_num = '1'
                    else:
                        timespan_num = '3'
                    if "天" in keywords['conditions'][i]['varieties']['timespan'][0]:
                        timespan_unit = '天'
                    elif "月" in keywords['conditions'][i]['varieties']['timespan'][0]:
                        timespan_unit = '月'
                    elif "年" in keywords['conditions'][i]['varieties']['timespan'][0]:
                        timespan_unit = '年'
                    else:
                        timespan_unit = '天'
                # 转换为天数
                if timespan_unit == "年":
                    timespan_num = str(int(timespan_num) * 365)
                elif timespan_unit == "月":
                    timespan_num = str(int(timespan_num) * 30)
                if "<time>" in where_analytic_expressions[i]:
                    current_date = get_today()
                    delta = datetime.timedelta(days=int(timespan_num))
                    days_ago = current_date - delta
                    values_from_where_analytic_expressions['time'][-1].append([str(days_ago.year), str(days_ago.month), str(days_ago.day)])
                    values_from_where_analytic_expressions['time'][-1].append([str(current_date.year), str(current_date.month), str(current_date.day)])
                else:
                    values_from_where_analytic_expressions['timespan'][-1].append(str(timespan_num))
            if 'varieties' in keywords['conditions'][i] and 'tag' in keywords['conditions'][i]['varieties']:
                values_from_where_analytic_expressions['tags'][-1].append(self.translate_zh2en[keywords['conditions'][i]['varieties']['tag'][0]])
            if 'varieties' in keywords['conditions'][i] and 'string' in keywords['conditions'][i]['varieties']:
                values_from_where_analytic_expressions['string'][-1].append(keywords['conditions'][i]['varieties']['string'][0])
        # print("where_analytic_expressions_with_slots: ", where_analytic_expressions_with_slots)
        # print("tags_from_where_analytic_expressions: ", tags_from_where_analytic_expressions)
        # print("properties_from_where_analytic_expressions: ", properties_from_where_analytic_expressions)
        # print("values_from_where_analytic_expressions: ", values_from_where_analytic_expressions)

        return where_analytic_expressions_with_slots, tags_from_where_analytic_expressions, properties_from_where_analytic_expressions, values_from_where_analytic_expressions

    def map_entities_with_properties(self, tags_from_where_analytic_expressions, properties_from_where_analytic_expressions):
        """
        通过字典，映射出where解析式中属性对应的tag
        :param:
            tags_from_where_analytic_expressions (list):
                从 keywords 字典中抽取出的 tags
                len(tags_from_where_analytic_expressions) == len(where_analytic_expressions_with_slots)
                example: [[''], [''], ['']]
            properties_from_where_analytic_expressions (list):
                从 keywords 字典中抽取出的 properties
                len(properties_from_where_analytic_expressions) == len(where_analytic_expressions_with_slots)
                example: [['name'], ['quarter'], ['year']]
        :return:
            entities_mapping_properties_from_where_analytic_expressions (list):
                WHERE 子句中属性对应的tag
                example: [[['stock', 'chairman', 'stockholder', 'fund_manager', 'trade', 'information', 'industry', 'public_offering_fund']], [['stock_financial_information']], [['stock_financial_information']]]
        """

        entities_mapping_properties_from_where_analytic_expressions = []
        for i in range(len(properties_from_where_analytic_expressions)):
            entities_mapping_properties_from_where_analytic_expressions.append([])
            for j in range(len(properties_from_where_analytic_expressions[i])):
                if type(properties_from_where_analytic_expressions[i][j]) != list:
                    properties_from_where_analytic_expressions_temp = [properties_from_where_analytic_expressions[i][j]]
                else:
                    properties_from_where_analytic_expressions_temp = properties_from_where_analytic_expressions[i][j]
                entities_mapping_properties_from_where_analytic_expressions[i].append([])
                for k in range(len(properties_from_where_analytic_expressions_temp)):
                    if tags_from_where_analytic_expressions[i][j] != '':
                        entities_mapping_properties_from_where_analytic_expressions[i][-1].append(tags_from_where_analytic_expressions[i][j])
                    else:
                        tmp_words = properties_from_where_analytic_expressions_temp[k].split(".")
                        entities_mapping = self.properties_map_en[tmp_words[0]]
                        entities_mapping_properties_from_where_analytic_expressions[i][-1] += [entity for entity in entities_mapping]
        # print('entities_mapping_properties_from_where_analytic_expressions: ', entities_mapping_properties_from_where_analytic_expressions)
        return entities_mapping_properties_from_where_analytic_expressions

    def align_variates(self, where_analytic_expressions_with_slots, keywords, entities_mapping_propertities_from_where_analytic_expressions,
                       properties_from_where_analytic_expressions, entities_in_match):
        """
        使 WHERE 子句中的变量对齐 MATCH 中的变量
        :param:
            where_analytic_expressions_with_slots (list):
                挖出槽的 WHERE 子句
                example: ['WHERE <property> == <entity>', 'WHERE <property> == <number>', 'WHERE <property> == <number>']
            keywords (dict):
                抽取出的 keywords 字典
                example: {'entities': ['行业', '股票'], 'properties': ['净利润'], 'time': [], 'conditions': [{'condition': '<entity>', 'varieties': {'entity': ['零售']}}, {'condition': '第<number>季度', 'varieties': {'number': ['2']}}, {'condition': '<number>年', 'varieties': {'number': 'returns': []}
            entities_mapping_properties_from_where_analytic_expressions (list):
                WHERE 子句中属性对应的tag
                example: [[['stock', 'chairman', 'stockholder', 'fund_manager', 'trade', 'information', 'industry', 'public_offering_fund']], [['stock_financial_information']], [['stock_financial_information']]]
            properties_from_where_analytic_expressions (list):
                从 keywords 字典中抽取出的 properties
                len(properties_from_where_analytic_expressions) == len(where_analytic_expressions_with_slots)
                example: [['name'], ['quarter'], ['year']]
            entities_in_match (string):
                从 MATCH 子句中抽取出的实体
                example: [{'type': 'vertex', 'variate': 'sfi', 'entity': 'stock_financial_information'}, {'type': 'edge', 'variate': 'hsfi', 'entity': 'has_stock_financial_information'}, {'type': 'vertex', 'variate': 's', 'entity': 'stock'}, {'type': 'edge', 'variate': 'bt', 'entity': 'belong_to'}, {'type': 'vertex', 'variate': 't', 'entity': 'trade'}]
        :return:
            entities_to_align (list):
                对齐后的 variate.tag.property
                example: [[['t.trade.name']], [['sfi.stock_financial_information.quarter']], [['sfi.stock_financial_information.year']]]
        """

        # print("entities_in_match: ", entities_in_match)

        # 对齐变量，将where解析式里的变量 与 match中的变量对齐
        entities_to_align = []
        for i in range(len(where_analytic_expressions_with_slots)):
            entities_to_align.append([])

            # 获取 condition 中的 entity
            entities_from_entity_in_where = []
            if 'varieties' in keywords["conditions"][i] and 'entity' in keywords["conditions"][i]['varieties']:
                for j in range(len(keywords['conditions'][i]['varieties']['entity'])):
                    entity_from_entity_in_where = keywords['conditions'][i]['varieties']['entity'][j]
                    entities_from_entity_in_where.append(self.translate_zh2en[self.entities_map[entity_from_entity_in_where]])
            # print(entities_from_entity_in_where)

            # 遍历每条“condition”里的 entity list
            for j in range(len(entities_mapping_propertities_from_where_analytic_expressions[i])):
                entities_to_align[i].append([])

                # 遍历所有可能的 entities ，选出 match 语句中存在的 entity
                types_existing_in_match = []
                variates_existing_in_match = []
                entities_existing_in_match = []
                for l in range(len(entities_in_match)):
                    cur_type = entities_in_match[l]['type']
                    cur_variate = entities_in_match[l]['variate']
                    cur_entity = entities_in_match[l]['entity']
                    if cur_entity in entities_mapping_propertities_from_where_analytic_expressions[i][j]:
                        types_existing_in_match.append(cur_type)
                        variates_existing_in_match.append(cur_variate)
                        entities_existing_in_match.append(cur_entity)
                mapped_types = types_existing_in_match
                mapped_variates = variates_existing_in_match
                mapped_entities = entities_existing_in_match
                # 匹配到match中的多个实体，则尝试用“condition”中的“entity”过滤
                if len(entities_existing_in_match) > 1:
                    filtrated_types_existing_in_match = []
                    filtrated_variates_existing_in_match = []
                    filtrated_entities_existing_in_match = []
                    for l in range(len(entities_existing_in_match)):
                        # 利用“condition”的“entity”过滤
                        if entities_existing_in_match[l] in entities_from_entity_in_where:
                            filtrated_types_existing_in_match.append(types_existing_in_match[l])
                            filtrated_variates_existing_in_match.append(variates_existing_in_match[l])
                            filtrated_entities_existing_in_match.append(entities_existing_in_match[l])
                    # 过滤完还存在匹配的entity
                    if len(filtrated_entities_existing_in_match) > 0:
                        mapped_types = filtrated_types_existing_in_match
                        mapped_variates = filtrated_variates_existing_in_match
                        mapped_entities = filtrated_entities_existing_in_match
                for l in range(len(mapped_variates)):
                    if mapped_types[l] == "vertex":
                        if not "indus1" == mapped_variates[l]:  # 上下游的变量默认选 indus 而非 indus1
                            entities_to_align[i][j].append(mapped_variates[l] + '.' + mapped_entities[l] + '.' + properties_from_where_analytic_expressions[i][j])
                    else:
                        if type(properties_from_where_analytic_expressions[i][j]) != list:
                            properties_from_where_analytic_expressions[i][j] = [properties_from_where_analytic_expressions[i][j]]
                        for m in range(len(properties_from_where_analytic_expressions[i][j])):
                            if mapped_entities[l] in self.properties_map_en[properties_from_where_analytic_expressions[i][j][m]]:
                                entities_to_align[i][j].append(mapped_variates[l] + '.' + properties_from_where_analytic_expressions[i][j][m])

                # for k in range(len(entities_mapping_propertities_from_where_analytic_expressions[i][j])):
                #     # 检索 match 语句中是否存在该 entity
                #     for l in range(len(entities_in_match)):
                #         cur_variate=entities_in_match[l]['variate']
                #         cur_entity=entities_in_match[l]['entity']
                #         if entities_mapping_propertities_from_where_analytic_expressions[i][j][k] == entities_in_match[l]['entity']:
                #             entities_existing_in_match.append(entities_in_match[l])
                #         # keywords 的 “condition” 里存在 “entity”，且匹配到实体
                #         if entities_from_entity_in_where != []:
                #             # 该 entity 存在于 match 语句中，且存在于 “condition”里的“entity”
                #             if entities_mapping_propertities_from_where_analytic_expressions[i][j][k] == entities_in_match[l]['entity'] and \
                #                     entities_mapping_propertities_from_where_analytic_expressions[i][j][k] in entities_from_entity_in_where:
                #                 entities_existing_in_match.append(entities_mapping_propertities_from_where_analytic_expressions[i][j][k])
                #                 entities_to_align[i][j].append(entities_in_match[l]['variate'] + '.' + properties_from_where_analytic_expressions[i][j])
                #         # keywords 的 “condition” 里不存在 “entity”
                #         else:
                #             # 该 entity 存在于 match 语句中
                #             if entities_mapping_propertities_from_where_analytic_expressions[i][j][k] == entities_in_match[l]['entity']:
                #                 entities_existing_in_match.append(entities_mapping_propertities_from_where_analytic_expressions[i][j][k])
                #                 entities_to_align[i][j].append(entities_in_match[l]['variate'] + '.' + properties_from_where_analytic_expressions[i][j])
                # print('match 语句中存在的 entities：', entities_existing_in_match)

        # print('entities_to_align: ', entities_to_align)

        return entities_to_align

    def backfill_properties_slots(self, where_analytic_expressions_with_slots, entities_to_align, values_from_where_analytic_expressions):
        """
        向 WHERE 子句中的 <property> 槽中回填“变量.属性”
        :param:
            where_analytic_expressions_with_slots (list):
                挖出槽的 WHERE 子句
                example: ['WHERE <property> == <entity>', 'WHERE <property> == <number>', 'WHERE <property> == <number>']
            entities_to_align (list):
                对齐后的 variate.tag.property
                example: [[['t.trade.name']], [['sfi.stock_financial_information.quarter']], [['sfi.stock_financial_information.year']]]
            values_from_where_analytic_expressions (dict):
                从 keywords 字典中抽取出的 varieties，即 WHERE 子句中的 value 部分，按 <entities>，<numbers>，<time>，<timespan>，<tags>，<string> 存储
                len(values_from_where_analytic_expressions) == len(where_analytic_expressions_with_slots)
                example: {'entities': [['零售'], [], []], 'numbers': [[], ['2'], ['2023']], 'time': [[], [], []], 'timespan': [[], [], []], 'tags': [[], [], []], 'string': [[], [], []]}
        :return:
            where_potential_results_without_None (list):
                回填后的 WHERE 子句
                example: [['WHERE t.trade.name == "零售"'], ['WHERE sfi.stock_financial_information.quarter == 2'], ['WHERE sfi.stock_financial_information.year == 2023']]
        """

        where_potential_results = []
        where_potential_results_without_None = []
        for i in range(len(where_analytic_expressions_with_slots)):
            property_align_num = 0
            entity_align_num = 0
            number_align_num = 0
            tag_align_num = 0
            time_align_num = 0
            timespan_align_num = 0
            string_align_num = 0
            # print('\nentities_to_align: ', entities_to_align[i])
            # print('where_analytic_expressions_with_slots: ', where_analytic_expressions_with_slots[i])
            # where_potential_results.append([''])
            where_potential_results.append([[]])
            words = where_analytic_expressions_with_slots[i].split()
            for j in range(len(words)):
                if words[j] == '<property>':
                    words_pre = copy.deepcopy(where_potential_results[i])
                    # print(words_pre)
                    # 将 entity.property 插入 where list 中的 <property> 槽
                    for k in range(len(entities_to_align[i][property_align_num])):
                        # print(entities_to_align[i][property_align_num][k])
                        if k == 0:
                            for l in range(len(words_pre)):
                                where_potential_results[i][l].append(entities_to_align[i][property_align_num][k])
                        else:
                            words_tmp = copy.deepcopy(words_pre)
                            for l in range(len(words_pre)):
                                words_tmp[l].append(entities_to_align[i][property_align_num][k])
                            where_potential_results[i] += words_tmp
                    property_align_num += 1
                elif words[j] == '<string>':
                    for k in range(len(where_potential_results[i])):
                        where_potential_results[i][k].append('"' + values_from_where_analytic_expressions['string'][i][string_align_num] + '"')
                    string_align_num += 1
                elif words[j] == '<tag>':
                    for k in range(len(where_potential_results[i])):
                        where_potential_results[i][k].append('"' + values_from_where_analytic_expressions['tags'][i][tag_align_num] + '"')
                    tag_align_num += 1
                elif words[j] == '<entity>':
                    for k in range(len(where_potential_results[i])):
                        entity_value = values_from_where_analytic_expressions['entities'][i][entity_align_num]
                        if entity_value in self.where_entities_map:   # todo: 将别名和 code 映射到 name
                            entity_value = self.where_entities_map[entity_value]
                        where_potential_results[i][k].append('"' + entity_value + '"')
                    entity_align_num += 1
                elif words[j] == '<time>':
                    for k in range(len(where_potential_results[i])):
                        time_words = "-".join(values_from_where_analytic_expressions['time'][i][time_align_num])
                        where_potential_results[i][k].append("'" + time_words + "'")
                    time_align_num += 1
                elif words[j] == '<timespan>':
                    for k in range(len(where_potential_results[i])):
                        where_potential_results[i][k].append(values_from_where_analytic_expressions['timespan'][i][timespan_align_num])
                    timespan_align_num += 1
                elif words[j] == '<number>':
                    for k in range(len(where_potential_results[i])):
                        # 如果keywords中”number“缺少数据，则给默认值3
                        if number_align_num >= len(values_from_where_analytic_expressions['numbers'][i]):
                            where_potential_results[i][k].append("3")
                        else:
                            where_potential_results[i][k].append(str(values_from_where_analytic_expressions['numbers'][i][number_align_num]))
                    number_align_num += 1
                elif words[j] == '\'<number>-<number>-<number>\'':
                    date_str = "'" + values_from_where_analytic_expressions['numbers'][i][number_align_num] + '-' + \
                               values_from_where_analytic_expressions['numbers'][i][
                                   number_align_num + 1] + '-' + values_from_where_analytic_expressions['numbers'][i][number_align_num + 2] + "'"
                    number_align_num += 3
                    for k in range(len(where_potential_results[i])):
                        where_potential_results[i][k].append(date_str)
                else:
                    for k in range(len(where_potential_results[i])):
                        where_potential_results[i][k].append(words[j])
            # print("where_potential_results: ", where_potential_results)
            for j in range(len(where_potential_results[i])):
                where_potential_results[i][j] = ' '.join(where_potential_results[i][j])
            # 去除找不到属性的变量而导致”WHERE ==“的情况
            if "WHERE ==" not in where_potential_results[i][0]:
                where_potential_results_without_None.append(where_potential_results[i])
        # print("where_potential_results_without_None: ", where_potential_results_without_None)

        return where_potential_results_without_None

    def get_number_data(self, number_str):
        """
        获取字符串中的数字部分
        :param:
            number_str (string):
                带数字的字符串
                example: '3%'
        :return:
            (string):
                数字部分
                example: '3'
        """

        if number_str[-1] == "%":
            number_str = number_str[:-1]
        if number_str[0] in ["+", "-"]:
            number_str_without_symbol = number_str[1:]
        else:
            number_str_without_symbol = number_str
        if "." in number_str_without_symbol:
            point_index = number_str_without_symbol.index(".")
            leftPart = number_str_without_symbol[:point_index]
            rightPart = number_str_without_symbol[point_index+1:]
            if leftPart.isdigit() and rightPart.isdigit():
                return number_str
            else:
                return '3'
        else:
            if number_str_without_symbol.isdigit():
                return number_str
            else:
                return '3'

    def get_year_month_day_from_str(self, str):
        """
        获取字符串中的日期部分
        :param:
            number_str (string):
                带数字的字符串
                example: '2023年12月12日'
        :return:
            (dict):
                日期部分
                example: {"year": 2023, "month": 12, "day": 12}
        """

        year = ""
        month = ""
        day = ""
        beginYear = True
        beginMonth = False
        beginDay = False
        for i in range(len(str)):
            if str[i] == "年":
                beginYear = False
                beginMonth = True
            elif str[i] == "月":
                beginMonth = False
                beginDay = True
            elif str[i] == "日":
                beginDay = False
                break
            elif str[i].isdigit() and beginDay:
                day += str[i]
            elif str[i].isdigit() and beginMonth:
                month += str[i]
            elif str[i].isdigit() and beginYear:
                year += str[i]
        return {"year": year, "month": month, "day": day}

    # def align_entity_for_return(self, entities_in_match, return_analytic_expression, keywords):
    #     # print('\nreturn 语句对齐实体变量：')
    #     # print('\nalign_entity_for_return 输入参数：')
    #     # print("entities_in_match: ", entities_in_match)
    #     # print("return_analytic_expression: ", return_analytic_expression)
    #     # print("keywords: ", keywords)
    #
    #     # 将 return 语句中的 variate.property 和 entity 提出
    #     # 'RETURN n, n.name'
    #     # ——>
    #     # 'RETURN <entity> <property>'
    #     # ['stock']
    #     # ['name']
    #     entities_from_return_analytic_expression = []
    #     if 'entity' in keywords['returns'][0]['varieties']:
    #         entities_from_return_analytic_expression.append(self.translate_zh2en[self.entities_map[keywords['returns'][0]['varieties']['entity'][0]]])
    #     else:
    #         entities_from_return_analytic_expression.append([])
    #     propertities_from_return_analytic_expression = []
    #     return_analytic_expression_words_with_slots = return_analytic_expression.split() if len(return_analytic_expression) > 0 else []
    #     for i in range(len(return_analytic_expression_words_with_slots)):
    #         if '.' in return_analytic_expression_words_with_slots[i] and '()' not in return_analytic_expression_words_with_slots[i]:
    #             property = return_analytic_expression_words_with_slots[i].split('.')[1] if return_analytic_expression_words_with_slots[i][-1] != ',' else \
    #                 return_analytic_expression_words_with_slots[i].split('.')[1][:-1]
    #             propertities_from_return_analytic_expression.append(property)
    #             return_analytic_expression_words_with_slots[i] = '<property>'
    #         # elif return_analytic_expression_words_with_slots[i] != 'RETURN':
    #         #     entity = translate_zh2en[entities_map[keywords['returns'][0]['vareities']['entity'][0]]]
    #         #     entities_from_return_analytic_expression.append(entity)
    #         #     return_analytic_expression_words_with_slots[i] = '<entity>'
    #         # 添加分隔符“,”
    #         if ',' in return_analytic_expression_words_with_slots[i]:
    #             return_analytic_expression_words_with_slots[i] += ','
    #     # return_analytic_expression_with_slots = ' '.join(return_analytic_expression_words)
    #     # print("\nreturn_analytic_expression_words_with_slots: ", return_analytic_expression_words_with_slots)
    #     # print("entities_from_return_analytic_expression: ", entities_from_return_analytic_expression)
    #     # print("propertities_from_return_analytic_expression: ", propertities_from_return_analytic_expression)
    #
    #     # 获取 keywords 内 return 解析式中 entities 涉及的变量
    #     variates_from_return_analytic_expression = []
    #     if len(entities_from_return_analytic_expression) > 0:
    #         for i in range(len(entities_in_match)):
    #             if entities_from_return_analytic_expression[0] == entities_in_match[i]['entity']:
    #                 variates_from_return_analytic_expression = [entities_in_match[i]['variate']]
    #                 break
    #
    #     # 检索变量，获取 variate.property
    #     property_num = 0
    #     potential_variate_property = []
    #     for i in range(len(return_analytic_expression_words_with_slots)):
    #         if return_analytic_expression_words_with_slots[i] == '<entity>':
    #             return_analytic_expression_words_with_slots[i] = variates_from_return_analytic_expression[0] if i == len(
    #                 return_analytic_expression_words_with_slots) - 1 else variates_from_return_analytic_expression[0] + ','
    #         elif return_analytic_expression_words_with_slots[i] == '<property>':
    #             potential_variate_property.append([])
    #             property = propertities_from_return_analytic_expression[property_num]
    #             # print(property)
    #             if len(variates_from_return_analytic_expression) > 0:
    #                 potential_variate_property[-1].append(variates_from_return_analytic_expression[0] + '.' + property)
    #             else:
    #                 # 潜在实体
    #                 potential_entities = self.properties_map_en[property]
    #                 # print(potential_entities)
    #                 # 检索潜在变量
    #                 for j in range(len(potential_entities)):
    #                     potential_variate_property[-1].append([])
    #                     for k in range(len(entities_in_match)):
    #                         if potential_entities[j] == entities_in_match[k]['entity']:
    #                             potential_variate_property[-1][j].append(entities_in_match[k]['variate'] + '.' + property)
    #             property_num += 1
    #     # print('potential_variate_property: ', potential_variate_property)
    #
    #     # 回填槽
    #     return_potential_results = [[]]
    #     property_align_num = 0
    #     for i in range(len(return_analytic_expression_words_with_slots)):
    #         if return_analytic_expression_words_with_slots[i] == '<property>':
    #             words_pre = copy.deepcopy(return_potential_results)
    #             # print(words_pre)
    #             # 将 entity.property 插入 where list 中的 <property> 槽
    #             for k in range(len(potential_variate_property[property_align_num])):
    #                 # print(potential_variate_property[property_align_num][k])
    #                 if k == 0:
    #                     for l in range(len(words_pre)):
    #                         return_potential_results[l].append(
    #                             potential_variate_property[property_align_num][k] if i == len(return_analytic_expression_words_with_slots) - 1 else
    #                             potential_variate_property[property_align_num][k] + ',')
    #                 else:
    #                     words_tmp = copy.deepcopy(words_pre)
    #                     for l in range(len(words_pre)):
    #                         words_tmp[l].append(
    #                             potential_variate_property[property_align_num][k] if i == len(return_analytic_expression_words_with_slots) - 1 else
    #                             potential_variate_property[property_align_num][k] + ',')
    #                     return_potential_results += words_tmp
    #             property_align_num += 1
    #         else:
    #             for j in range(len(return_potential_results)):
    #                 return_potential_results[j].append(return_analytic_expression_words_with_slots[i])
    #     print(return_potential_results)
    #     for j in range(len(return_potential_results)):
    #         return_potential_results[j] = ' '.join(return_potential_results[j])
    #     # print("return_potential_results: ", return_potential_results)
    #     return return_potential_results


if __name__ == "__main__":
    keywords = {"entities": ["股票"], "properties": ["开盘价"],
                "conditions": [{"condition": "<property>高于<entity>", "varieties": {"property": ["开盘价"], "entity": ["国泰君安"]}},
                               {"condition": "今日", "varieties": {}}],
                "returns": [{"return": "的<entity>", "varieties": {"entity": ["股票"]}}]}
    match_analytic_expression = 'MATCH (s:stock)-->(sd:stock_data)'
    return_analytic_expression = 'RETURN n.code, n.name'

    entities_in_match = extract_entities_in_match(match_analytic_expression)

    where_analytic_expressions = match_analytic_structure_for_where(["<property>高于<entity>", "今日"])
    where_analytic_expressions = splice_match_into_where_statement(match_analytic_expression, where_analytic_expressions)

    align_entity_for_where(entities_in_match, where_analytic_expressions, keywords)
    align_entity_for_return(entities_in_match, return_analytic_expression, keywords)

