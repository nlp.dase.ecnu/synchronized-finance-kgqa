"""
根据 keywords 解析出 WHERE 子句
"""


class Keywords2WhereModel:
    """
    WHERE 解析模块。根据 keywords 解析出 WHERE 子句

    :param:
        dicts (dict): 对齐需要用到的字典，具体可参考 kgqa/src/dict.py
    """

    def __init__(self, dicts):
        self.where_parse = dicts["where_parse"]

    # def __init__(self):
    #     basedir = os.path.dirname(__file__)
    #     where_parse_json_file_path = os.path.join(basedir, '../../resources/parse/where_parse.json')
    #     with open(where_parse_json_file_path, 'r', encoding='utf-8') as file:
    #         self.where_parse = json.load(file)

    def match_analytic_structure_for_where(self, conditions):
        """
        生成 WHERE 语句。本模块向 "match_analytic_structure" 模块提供 WHERE 语句。
        :param:
            conditions (list):
                用于匹配解析式的条件关键词。
                example: ["<property>高于<entity>", "今日"]
        :return:
            where_analytic_expressions (list):
                由 WHERE 解析式设计表解析的 WHERE 语句。
                example: ["WHERE sd.name == <value>
                           WITH sd.opening_price as temp
                           WHERE sd.opening_price >temp",
                          "WHERE sd.date == date()"]
        """

        where_analytic_expressions = []

        # 根据 where 解析字典匹配解析式
        for i in range(len(conditions)):
            cond = conditions[i]
            where_stat = self.where_parse[cond]
            where_analytic_expressions.append(where_stat)

        return where_analytic_expressions

    def splice_match_into_where_statement(self, match_statement, where_analytic_expressions):
        """
        拼接 MATCH 语句到 WHERE 语句中。
        :param:
            match_statement (string):
                用于拼接的 MATCH 语句。
                example: "MATCH (sd:stock_data)"
            where_analytic_expressions (list):
                待拼接的 WHERE 语句。
                    example: ["WHERE sd.name == <value>
                               WITH sd.opening_price as temp
                               WHERE sd.opening_price >temp",
                              "WHERE sd.date == date()"]
        :return:
            spliced_where_statement (list):
                拼接后的 WHERE 语句。
                example: ["WHERE sd.name == <value>
                           WITH sd.opening_price as temp
                           MATCH (sd:stock_data)
                           WHERE sd.opening_price >temp",
                          "WHERE sd.date == date()"]
        """

        # print('match_statement: ', match_statement)
        # print('where_analytic_expressions: ', where_analytic_expressions)

        spliced_where_statement = []
        for i in range(len(where_analytic_expressions)):
            # 将 where 表达式 string 转化为 words list
            where_analytic_expressions[i] = where_analytic_expressions[i].split('\n')
            where_analytic_expressions[i] = ' '.join(where_analytic_expressions[i])
            words = where_analytic_expressions[i].split(' ')
            # 检测 with 并补全 match 语句
            j = 0
            exist_with = False
            while j < len(words):
                if words[j] == 'WHERE' and exist_with:
                    words.insert(j, "\n" + match_statement)
                    j += 1
                if words[j] == 'WITH':
                    exist_with = True
                if (words[j] == 'MATCH' or words[j] == 'WHERE' or words[j] == 'RETURN') and j != 0:
                    words[j] = "\n" + words[j]
                j += 1
            spliced_where_statement.append(' '.join(words))

        return spliced_where_statement

    def add_default_time(self, entities_in_match, where_analytic_expressions):
        """
        拼接 MATCH 语句到 WHERE 语句中。
        :param:
            entities_in_match (string):
                从 MATCH 子句中抽取出的实体
                example: [{'type': 'vertex', 'variate': 'sfi', 'entity': 'stock_financial_information'}, {'type': 'edge', 'variate': 'hsfi', 'entity': 'has_stock_financial_information'}, {'type': 'vertex', 'variate': 's', 'entity': 'stock'}, {'type': 'edge', 'variate': 'bt', 'entity': 'belong_to'}, {'type': 'vertex', 'variate': 't', 'entity': 'trade'}]
            where_analytic_expressions (list):
                可能缺乏时间的 WHERE 语句。
                    example: ["WHERE sd.name == <value>
                               WITH sd.opening_price as temp
                               WHERE sd.opening_price >temp"]
        :return:
            where_analytic_expressions (list):
                添加默认时间的 WHERE 语句。
                example: ["WHERE sd.name == <value>
                           WITH sd.opening_price as temp
                           MATCH (sd:stock_data)
                           WHERE sd.opening_price >temp",
                          "WHERE sd.date == date()"]
        """

        for i in range(len(entities_in_match)):
            if entities_in_match[i]["entity"] in ["stock_data", "stock_financial_information", "trade_data"]:
                exist_time_flag = False
                for j in range(len(where_analytic_expressions)):
                    for k in range(len(where_analytic_expressions[j])):
                        if entities_in_match[i]["variate"] + "." + entities_in_match[i]["entity"] + ".`date`" in where_analytic_expressions[j][k] or entities_in_match[i][
                            "variate"] + "." + entities_in_match[i]["entity"] + ".year" in where_analytic_expressions[j][k] or entities_in_match[i]["variate"] + "." + \
                                entities_in_match[i]["entity"] + ".half_year" in where_analytic_expressions[j][k] or entities_in_match[i]["variate"] + "." + \
                                entities_in_match[i]["entity"] + ".quarter" in where_analytic_expressions[j][k]:    # 如果where表达式中已经包含了`data`或year或half_year或quarter，则不添加默认时间
                            exist_time_flag = True
                            break
                    if exist_time_flag:
                        break
                if not exist_time_flag:
                    where_analytic_expressions.append(["WHERE " + entities_in_match[i]["variate"] + "." + entities_in_match[i]["entity"] + ".`date` == date()"])
        return where_analytic_expressions

    def separate_order_by(self, where_analytic_expressions):
        """
        分离 WHERE 子句中的 ORDER BY 子句
        :param:
            where_analytic_expressions (list):
                待分离的 WHERE 语句。
                    example: ["WHERE s.stock.name == '国泰君安'", "ORDER BY sd.stock_data.opening_price"]
        :return:
            order_by_analytic_expressions (list):
                分离后的 WHERE 语句。
                example: ["ORDER BY sd.stock_data.opening_price"]
            where_analytic_expressions (list):
                分离后的 WHERE 语句。
                example: ["WHERE s.stock.name == '国泰君安'"]
        """

        # print("separate_order_by where_analytic_expressions: ", where_analytic_expressions)
        order_by_analytic_expressions = []
        i = 0
        while i < len(where_analytic_expressions):
            if "ORDER BY" in where_analytic_expressions[i][0]:
                order_by_analytic_expression = where_analytic_expressions.pop(i)
                i -= 1
                order_by_analytic_expressions.append(order_by_analytic_expression)
            i += 1
        return order_by_analytic_expressions, where_analytic_expressions
