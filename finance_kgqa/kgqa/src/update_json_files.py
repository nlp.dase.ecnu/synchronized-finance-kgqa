"""
从传入接口中更新json文件

request格式：
{
    "type": "VERTEX",
    "object": "stock_data",
    "vid": "stock_data_1",
    "data": {
        "opening_price": "14.54"
    }
}
"""

import csv
import os
import json
import time


basedir = os.path.dirname(__file__)


def update_json(request):
    """
    从request更新json文件
    :param request: 传入值
    :return:
        None
    """
    with open(os.path.join(basedir, "../resources/parse/entities.json"), "r", encoding="utf-8") as e, \
            open(os.path.join(basedir, "../resources/parse/entities_map.json"), "r", encoding="utf-8") as ef, \
            open(os.path.join(basedir, "../resources/parse/where_entities.json"), "r", encoding="utf-8") as wef, \
            open(os.path.join(basedir, "../resources/parse/where_entities_map.json"), "r", encoding="utf-8") as wem:
        entities = json.load(e)
        entities_map = json.load(ef)
        where_entities = json.load(wef)
        where_entities_map = json.load(wem)

    # 只更新TAG的request
    if (request["type"] == "EDGE"):
        return
    else:
        enumerate_map = {"stock": "股票", "chairman": "董事长", "stockholder": "股东", "trade": "行业",
                         "public_offering_fund": "公募基金", \
                         "fund_manager": "基金经理人", "industry": "产业"}
        if (request["object"] in enumerate_map.keys()):
            tag = enumerate_map[request["object"]]
            code = request["data"].get("code", "")
            name = request["data"].get("name", "")
            if (code != "" and entities_map.get(code, "") == ""):  # 更新了code字段并且更新的内容不在原始内容中
                entities_map[code] = tag
                entities[tag].append(code)
                where_entities[tag].append(code)
                where_entities_map[code] = code
            if (name != "" and entities_map.get(name, "") == ""):  # 更新了name字段并且更新的内容不在原始内容中
                entities_map[name] = tag
                entities[tag].append(name)
                where_entities[tag].append(name)
                where_entities_map[name] = name
            # 获取别名信息，并转换为别名词列表
            alias = request["data"].get("alias", "")
            if alias != "":
                alias_list = alias.split(',')  # 暂定alias列数据为以逗号分隔的string格式
                for a in alias_list:
                    if entities_map.get(a, "") == "":
                        entities_map[a] = tag
                        entities[tag].append(a)
                        where_entities[tag].append(a)
                        where_entities_map[a] = name

    with open(os.path.join(basedir, "../resources/parse/entities.json"), "r", encoding="utf-8") as e, \
            open(os.path.join(basedir, "../resources/parse/entities_map.json"), "w", encoding="utf-8") as ef, \
            open(os.path.join(basedir, "../resources/parse/where_entities.json"), "w", encoding="utf-8") as wef, \
            open(os.path.join(basedir, "../resources/parse/where_entities_map.json"), "w", encoding="utf-8") as wem:
        json.dump(entities, e, ensure_ascii=False)
        json.dump(entities_map, ef, ensure_ascii=False)
        json.dump(where_entities, wef, ensure_ascii=False)
        json.dump(where_entities_map, wem, ensure_ascii=False)

def update_user_dict(entities_path, properties_path, new_path):
    """
    从 entities.json 文件和 properties.json 文件更新 user_dict.txt
    :param entities_path: entities.json 的文件路径
    :param properties_path: properties.json 的文件路径
    :param new_path: 写入的 user_dict.txt 文件路径
    :return:
        None
    """
    with open(entities_path, "r", encoding="utf-8") as pf:
        dic_new = json.load(pf)
    with open(properties_path, "r", encoding="utf-8") as pf:
        dic_new2 = json.load(pf)
    words = []
    for k, v in dic_new.items():
        for i in v:
            if i not in words:
                words.append(i.upper())
    for k, v in dic_new2.items():
        for i in v:
            if i not in words:
                words.append(i.upper())

    # 对列表中的所有字符串，按照从长到短排序
    words = sorted(words, key=len, reverse=True)
    with open(new_path, "w", encoding="utf-8") as f:
        for word in words:
            f.write(word + "\n")

def add_stockNameCode_to_dict(path):
    """
        给当前json文件添加股票代码
        :param path: 输入文件路径
        :return:
            None
        """
    extend_data = []
    with open('stock.csv', 'r', encoding="utf-8") as file:
        # 创建 CSV 读取器
        csv_reader = csv.reader(file)
        for row in csv_reader:
            extend_data.append(row[1].upper())
            extend_data.append(row[1].lower())
            extend_data.append(row[1][2:])
            extend_data.append(row[2].upper())
            extend_data.append(row[2].upper())
    with open(path, 'r', encoding="utf-8") as file:
        data = json.load(file)
    for d in extend_data:
        if d not in data["股票"]:
            data["股票"].append(d)
    data["股票"] = list(set(data["股票"]))
    print(data)
    with open(path, 'w', encoding="utf-8") as file:
        json.dump(data, file, ensure_ascii=False)

def add_stockNameCode_to_map_dict(path, type):
    """
        给当前map文件添加股票代码
        :param path: 输入文件路径
        :param type: "entities_map" or "where_entities_map"
        :return:
            None
        """
    extend_data = {}
    with open('stock.csv', 'r', encoding="utf-8") as file:
        # 创建 CSV 读取器
        csv_reader = csv.reader(file)
        for row in csv_reader:
            if type == "entities_map":
                extend_data[row[1][2:]] = "股票"
                extend_data[row[1].upper()] = "股票"
                extend_data[row[1].lower()] = "股票"
            elif type == "where_entities_map":
                extend_data[row[1][2:]] = row[2].upper()
                extend_data[row[1].upper()] = row[2].upper()
                extend_data[row[1].lower()] = row[2].upper()

    with open(path, 'r', encoding="utf-8") as file:
        data = json.load(file)
    for k, v in extend_data.items():
        data[k] = v
    print(data)
    with open(path, 'w', encoding="utf-8") as file:
        json.dump(data, file, ensure_ascii=False)

if __name__ == '__main__':
    request = {
        "type": "VERTEX",
        "object": "stock",
        "vid": "stock_1",
        "data": {
            "name": "茅台",
            "code": "000001",
            "alias": "茅台国酒, 茅子"
        }
    }
    update_json(request)

    update_user_dict(os.path.join(basedir, "../resources/parse/entities.json"),
                         os.path.join(basedir, "../resources/parse/properties.json"),
                         os.path.join(basedir, "../resources/parse/user_dict.txt"))

    add_stockNameCode_to_dict('entities.json')
    add_stockNameCode_to_map_dict('entities_map.json', "entities_map")
    add_stockNameCode_to_dict('where_entities.json')
    add_stockNameCode_to_map_dict('where_entities_map.json', "where_entities_map")
