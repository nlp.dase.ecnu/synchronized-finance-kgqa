"""
nebula graph 数据库交互模块。包括数据库连接，cypher 语句查询，查询结果格式化等。
"""

from nebula3.data.DataObject import DateWrapper, ValueWrapper
from nebula3.gclient.net import ConnectionPool
from nebula3.Config import Config
import json

from kgqa.src.utils.format_response import result_to_df, print_resp, customized_cast_with_dict


def connect_graph():
    """
    连接 nebula graph
    :param:
    :return:
    """

    max_connection_pool_size = 300

    with open('kgqa/resources/config/nebula.json', 'r') as f:
        data = json.load(f)
        ip = data["ip"]
        port = data["port"]
        username = data["username"]
        password = data["password"]
        global spacename_prefix
        spacename_prefix = "Use " + data["spacename"] + ";"


    config = Config()
    config.max_connection_pool_size = max_connection_pool_size
    global connection_pool
    connection_pool = ConnectionPool()
    assert connection_pool.init([(ip, port)], config)


def get_connection_pool():
    """
    返回 nebula graph 连接池
    :param:
    :return:
        connection_pool (ConnectionPool): nebula graph 连接池
    """
    return connection_pool


def close_connection(self):
    """
    关闭 Nebula graph 连接池
    :param:
    :return:
    """

    self.connection_pool.close()


def execute_cypher(operation, statement):
    """
    执行 cypher 语句，并返回结果。
    :param:
        statement ('string'):
            cypher 语句。
            example: "match (n:stock) where n.stock.name=='stock_name_0' return n"
    :return:
        response ('ResultSet'):
            返回结果，能够被 ".utils.format_response.print_resp" 解析。
    """
    client = None
    try:
        client = connection_pool.get_session("root", "nebula")
        assert client is not None
        global spacename_prefix
        resp = client.execute(spacename_prefix + statement)
        assert resp.is_succeeded(), resp.error_msg()
        # if resp == "None":
        #     return "Error"
        if operation in ("insert", "delete"):
            return
        else:
            return format_results(resp, operation)
        # if operation == "query":
        #     return format_results(resp)
        # return resp
    except Exception as x:
        print(x)
        import traceback
        print(traceback.format_exc())
    finally:
        if client is not None:
            client.release()


def format_results(resp, operation="query"):
    """
    格式化输出
    :param resp: 数据库执行后的响应数据
    :return:
        result_dict (dict):
            n*m的表格，包含表头
    """

    ks = resp.keys()
    properties_list = []
    values_list = []

    assert resp.is_succeeded()

    result_dict = {}

    # 将“s.stock的列为字典”这类情况下的属性提出
    ks_num = 0
    if resp.row_size() > 0:
        for col in resp.row_values(0):
            val = customized_cast_with_dict(col)
            if type(val) == dict and not operation.startswith("insert"):
                for k, v in val.items():
                    properties_list.append(ks[ks_num] + "." + k)
                    result_dict[ks[ks_num] + "." + k] = []
                ks_num += 1
            else:
                properties_list.append(ks[ks_num])
                result_dict[ks[ks_num]] = []
                ks_num += 1
    else:
        properties_list = ks

    # print(result_dict)

    for recode in resp:
        value_list = []
        ks_num = 0
        for col in recode:
            val = customized_cast_with_dict(col)
            # 若值为字典，则分别提出再添加到列表
            if type(val) == dict and not operation.startswith("insert"):
                for k, v in val.items():
                    if (isinstance(v, ValueWrapper) and not v.is_string()) or isinstance(v, DateWrapper):
                        v = str(v)
                    value_list.append(v)
                    result_dict[ks[ks_num] + "." + k].append(v)
                ks_num += 1
            # 值为正常的单个值，直接添加到列表
            else:
                if (isinstance(val, ValueWrapper) and not val.is_string()) or isinstance(val, DateWrapper):
                    val = str(val)
                value_list.append(val)
                result_dict[ks[ks_num]].append(val)
                ks_num += 1
            # print(value_list)
        values_list.append(value_list)
        # print(values_list)
    # print([properties_list] + values_list)
    # return [properties_list] + values_list
    return result_dict
