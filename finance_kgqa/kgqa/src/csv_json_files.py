"""
从CSV初始化json文件

接受初始化的json文件：
    entities.json
    实体->实体名列表

    entities_map.json
    实体名->实体的映射，专用于Match
    股票：股票名称、股票代码(、股票名称的模糊匹配)
    董事长：姓名
    股东：公司名称
    行业：行业代码、行业名称
    公募基金：基金名称(、基金名称的模糊匹配)
    基金经理人：姓名
    产业：名称

    where_entities.json
    实体名->实体的映射，专用于Where和Return

    where_entities_map.json
    模糊匹配->全称的映射

property按照现有纲要设计，不考虑从CSV更新

"""

import csv
import json
import os

basedir = os.path.dirname(__file__)


def read_csv_columnname(path, columnname):
    """
    从csv文件中读取相应column的所有值
    :param path: 输入csv文件路径
    :param columnname: 读取的column名称
    :return:
        l: csv文件中相应column的所有值列表
    """
    l = []
    with open(path, 'r', encoding='GBK') as file:
        csv_reader = csv.DictReader(file)
        for row in csv_reader:
            l.append(row[columnname])
    return l


def flat_list(l):
    """
    将以","分隔的字符串列表拉伸为一个列表
    :param l: 输入列表
    :return:
        new_l: 拉伸后的输出列表
    """
    new_l = []
    for item in l:
        new_l.extend(item.split(', '))
    return new_l


def read_match_special_keywords(path=os.path.join(basedir, '../resources/parse/match_special_keywords.json')):
    """
    读取 match_special_keywords
    :param path: match_special_keywords.json 文件路径
    :return:
        special_keywords_dic: match_special_keywords字典
    """
    with open(path, 'r', encoding='utf-8') as file:
        special_keywords_dic = json.load(file)
    return special_keywords_dic


def initialize_json(stock_path=None, chairman_path=None, stockholder_path=None, trade_path=None,
                    public_offering_fund_path=None, fund_manager_path=None, industry_path=None):
    """
    从csv文件生成json文件
    :param stock_path: csv文件路径
    :param chairman_path: csv文件路径
    :param stockholder_path: csv文件路径
    :param trade_path: csv文件路径
    :param public_offering_fund_path: csv文件路径
    :param fund_manager_path: csv文件路径
    :param industry_path: csv文件路径
    :return:
        将字典写入 entities.json，entities_map.json，where_entities.json，where_entities_map.json
    """
    special_keywords_dic = read_match_special_keywords()
    entities = special_keywords_dic
    entities_map = {}
    where_entities = {}
    where_entities_map = {}

    if stock_path:
        stock_code = read_csv_columnname(stock_path, 'code')
        stock_name = read_csv_columnname(stock_path, 'name')
        stock_alias = read_csv_columnname(stock_path, 'alias')
        stock_alias_ = flat_list(stock_alias)
        where_entities['股票'] = []
        for i in stock_code + stock_name + stock_alias_:
            entities['股票'].append(i)
            entities_map[i] = '股票'
            where_entities['股票'].append(i)
        for name, alias in zip(stock_name, stock_alias):
            where_entities_map[name] = name  # 自己模糊匹配上自己
            if stock_alias != "":  # 如果提供了别名
                a = alias.split(', ')
                for i in a:
                    where_entities_map[i] = name
    if chairman_path:
        chairman_name = read_csv_columnname(chairman_path, 'name')
        where_entities['董事长'] = []
        for i in chairman_name:
            entities['董事长'].append(i)
            entities_map[i] = '董事长'
            where_entities['董事长'].append(i)
    if stockholder_path:
        stockholder_name = read_csv_columnname(stockholder_path, 'name')
        where_entities['股东'] = []
        for i in stockholder_name:
            entities['股东'].append(i)
            entities_map[i] = '股东'
            where_entities['股东'].append(i)
    if trade_path:
        trade_code = read_csv_columnname(trade_path, 'code')
        trade_name = read_csv_columnname(trade_path, 'name')
        where_entities['行业'] = []
        for i in trade_code + trade_name:
            entities['行业'].append(i)
            entities_map[i] = '行业'
            where_entities['行业'].append(i)
    if public_offering_fund_path:
        public_offering_fund_name = read_csv_columnname(public_offering_fund_path, 'name')
        public_offering_fund_alias = read_csv_columnname(public_offering_fund_path, 'alias')
        public_offering_fund_alias_ = flat_list(public_offering_fund_alias)
        where_entities['公募基金'] = []
        for i in public_offering_fund_name + public_offering_fund_alias_:
            entities['公募基金'].append(i)
            entities_map[i] = '公募基金'
            where_entities['公募基金'].append(i)
        for name, alias in zip(public_offering_fund_name, public_offering_fund_alias):
            where_entities_map[name] = name  # 自己模糊匹配上自己
            if public_offering_fund_alias != "":  # 如果提供了别名
                a = alias.split(', ')
                for i in a:
                    where_entities_map[i] = name
    if fund_manager_path:
        fund_manager_name = read_csv_columnname(fund_manager_path, 'name')
        where_entities['基金经理人'] = []
        for i in fund_manager_name:
            entities['基金经理人'].append(i)
            entities_map[i] = '基金经理人'
            where_entities['基金经理人'].append(i)
    if industry_path:
        industry_name = read_csv_columnname(industry_path, 'name')
        where_entities['产业'] = []
        for i in industry_name:
            entities['产业'].append(i)
            entities_map[i] = '产业'
            where_entities['产业'].append(i)

    with open(os.path.join(basedir, "../resources/parse/entities.json"), "w", encoding="utf-8") as e, \
            open(os.path.join(basedir, "../resources/parse/entities_map.json"), "w", encoding="utf-8") as ef, \
            open(os.path.join(basedir, "../resources/parse/where_entities.json"), "w", encoding="utf-8") as wef, \
            open(os.path.join(basedir, "../resources/parse/where_entities_map.json"), "w", encoding="utf-8") as wem:
        json.dump(entities, e, ensure_ascii=False)
        json.dump(entities_map, ef, ensure_ascii=False)
        json.dump(where_entities, wef, ensure_ascii=False)
        json.dump(where_entities_map, wem, ensure_ascii=False)

    # with open(os.path.join(basedir, "entities_map.json"), "w", encoding="utf-8") as ef, \
    #         open(os.path.join(basedir, "where_entities.json"), "w", encoding="utf-8") as wef, \
    #         open(os.path.join(basedir, "where_entities_map.json"), "w", encoding="utf-8") as wem:
    #     json.dump(entities_map, ef, ensure_ascii=False)
    #     json.dump(where_entities, wef, ensure_ascii=False)
    #     json.dump(where_entities_map, wem, ensure_ascii=False)


def initialize_user_dict(entities_path, properties_path, new_path):
    """
    从 entities.json 文件和 properties.json 文件更新 user_dict.txt
    :param entities_path: entities.json 的文件路径
    :param properties_path: properties.json 的文件路径
    :param new_path: 写入的 user_dict.txt 文件路径
    :return:
        None
    """
    with open(entities_path, "r", encoding="utf-8") as pf:
        dic_new = json.load(pf)
    with open(properties_path, "r", encoding="utf-8") as pf:
        dic_new2 = json.load(pf)
    words = []
    for k, v in dic_new.items():
        for i in v:
            if i not in words:
                words.append(i.upper())
    for k, v in dic_new2.items():
        for i in v:
            if i not in words:
                words.append(i.upper())

    # 对列表中的所有字符串，按照从长到短排序
    words = sorted(words, key=len, reverse=True)
    with open(new_path, "w", encoding="utf-8") as f:
        for word in words:
            f.write(word + "\n")


if __name__ == '__main__':
    initialize_json(stock_path='工作簿1.csv')
    initialize_user_dict(os.path.join(basedir, "../resources/parse/entities.json"),
                         os.path.join(basedir, "../resources/parse/properties.json"),
                         os.path.join(basedir, "../resources/parse/user_dict.txt"))
