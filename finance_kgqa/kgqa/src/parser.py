"""
启发式解析器，将查询文本解析为 cypher 语句
"""

import os
import time

# from utils.utils import *
from kgqa.src.utils.extract_query_element import ExtractQueryElementModel
from kgqa.src.utils.link_completion import *
from kgqa.src.utils.keywords2match import *
from kgqa.src.utils.keywords2return import *
from kgqa.src.utils.special_question_process import SpecialQuestionProcessor
from kgqa.src.utils.generate_cypher import GenerateCypherModel
from kgqa.src.utils.keywords2where import Keywords2WhereModel
from kgqa.src.utils.variates_alignment import VariatesAlignmentModel
from kgqa.src.utils.review import *
# from kgqa.src.utils.sort_cypher import SortCypherModel
from kgqa.src.utils.intent_recognition import intent_recognition_from_cypher

basedir = os.path.dirname(__file__)

class Parser:
    """
    解析式类。将自然语言解析为 cypher 语句。
    :param:
        dicts (dict):
            kgqa/src/dict.py 中的预定义字典
    """

    def __init__(self, dicts):
        self.dicts = dicts
        self.translate_en2zh = dicts["translate_en2zh"]

        # 加载所需类
        start_time = time.time()
        self.generate_cypher_model = GenerateCypherModel()
        self.extract_query_elemen_model = ExtractQueryElementModel(dicts)
        self.return_mapper = ReturnMapper(dicts)
        self.match_mapper = MatchMapper(dicts)
        self.link_builder = LinkBuilder(dicts)
        self.keywords2where_model = Keywords2WhereModel(dicts)
        self.variates_alignment_model = VariatesAlignmentModel(dicts)
        self.spq_processor = SpecialQuestionProcessor(dicts)
        self.reviewer = SelfReviewer()
        # self.sortcyphermodel = SortCypherModel()
        end_time = time.time()
        print('加载类时间：', end_time - start_time)

    def parse(self, query):
        """
        将查询文本解析为 cypher 语句。本模块为顶层处理模块，调用子模块以实现 cypher 语句生成。
        :param:
            query (string):
                    自然语言问题。
                    example: "今日开盘价高于国泰君安的股票"
        :return:
            cypher_statement (list):
                cypher 语句。每一个 MATCH 子句对应一组 cypher，每组 cypher 对应不同的 WHERE 子句组合
                example: [["match (n:stock) where n.stock.name=='国泰君安' return n.stock"]]
            keywords (dict):
                抽取出的 keywords 字典
        """

        # phrases=split_query_text(self.query)
        start_time = time.time()
        spq_cypher = self.spq_processor.match(query)
        if spq_cypher:
            return [[spq_cypher]], None
        keywords=self.identify_keywords(query)
        print("keywords: ", keywords)
        structures=self.match_analytic_structure(keywords)
        # intent = intent_recognition(query, keywords, self.dicts["intent_map"])

        cypher_statements=[]
        for i in range(len(structures)):
            cypher_statement = self.combine_structures_into_cypher(structures[i])
            cypher_statement = self.splice_match_into_where_statement(structures[i]['match'][0], cypher_statement)
            cypher_statement = self.reviewer.forward(cypher_statement)
            cypher_statements.append(cypher_statement)

        end_time = time.time()
        print('cypher语句：', cypher_statements)
        print('解析keywords时间：', end_time - start_time)

        # cypher语句排序，排序结果尚未return，按需使用
        # max_cypher, sorted_cyphers = self.sort_cypher_with_similarity(query, cypher_statements[0])
        # print('排序好的cypher语句：', sorted_cyphers)
        # print('排序好的cypher语句一句：', max_cypher)

        # return cypher_statements, keywords, intent
        return cypher_statements, keywords

    def identify_keywords(self, query):
        """
        从分割的短语中识别关键词。搜索字典以匹配关键词。
        :param:
            phrases (list):
                由查询文本分割出的短语。
                example: ["今日", "开盘价", "高于", "国泰君安", "的", "股票"]
        :return:
            keywords (dict):
                根据字典匹配出的关键词。
                example: {"entities": ["股票"], "properties": ["开盘价"],
                        "conditions": [{"condition":"<property>高于<entity>", "vareities":{"property":["开盘价"], "entity":["国泰君安"]}},
                                        {"condition":"今日"}],
                        "returns": [{"return":"的<entity>", "vareities":{"entity":["股票"]}}]}
        """
        keywords = self.extract_query_elemen_model.extract_query_element(query)
        return keywords

    def match_analytic_structure(self, keywords):
        """
        搜索解析式设计表以匹配解析式结构。本模块涉及变量对齐（e.g. s:stock 和 n.name 如何对齐 s 和 n）。
        :param:
            keywords (dict):
                用于匹配解析式设计表的关键词。
                example: {"entities": ["股票"], "properties": ["开盘价"], "conditions": ["<property>高于<entity>", "今日"], "returns": ["的<entity>"]}
        :return:
            structures (dict):
                由解析式设计表解析出的 cypher 结构。
                example: {"match": ["MATCH (sd:stock_data)"],
                          "where": ["WHERE sd.name == <value>
                                     WITH sd.opening_price as temp
                                     MATCH (sd:stock_data)
                                     WHERE sd.opening_price >temp",
                                    "WHERE sd.date == date()"],
                          "return": ["RETURN sd.name"],
                          "order_by": [],
                          "limit": []}
        """

        # 匹配 match 解析语句
        match_analytic_expression = self.match_analytic_structure_for_match(keywords['entities'], keywords['properties'], keywords['time'])
        structures = []
        for i in range(len(match_analytic_expression)):
            # 抽取 match 语句中的变量与实体
            entities_in_match = self.variates_alignment_model.extract_entities_in_match(match_analytic_expression[i])

            # 匹配 where 解析语句
            where_analytic_expressions = self.match_analytic_structure_for_where(keywords['conditions'])
            # 若有 with，拼接 match 语句
            # where_analytic_expressions = self.splice_match_into_where_statement(match_analytic_expression[i], where_analytic_expressions)
            # 对齐 where 语句中的变量
            where_potential_results = self.variates_alignment_model.align_entity_for_where(entities_in_match, where_analytic_expressions, keywords)
            # 对涉及 ["stock_data", "stock_financial_information", "trade_data"] 属性的查询，添加默认时间
            where_analytic_expressions=self.keywords2where_model.add_default_time(entities_in_match, where_potential_results)
            # 从 where 解析式中分离 order by
            order_by_analytic_expressions, where_analytic_expressions = self.keywords2where_model.separate_order_by(where_analytic_expressions)

            # 匹配 return 解析语句
            # print("parser keywords['returns']: ", keywords['returns'])
            return_analytic_expression = self.match_analytic_structure_for_return(keywords['returns'], match_analytic_expression[i])
            # print("parser return_analytic_expression: ", return_analytic_expression)
            # 对齐 return 语句中的变量
            # return_potential_results = align_entity_for_return(entities_in_match, return_analytic_expression[0], keywords)

            # 组合解析式结构
            structure = {}
            structure['match'] = [match_analytic_expression[i]]
            structure['where'] = where_analytic_expressions
            structure['return'] = return_analytic_expression
            structure['order_by'] = order_by_analytic_expressions
            structure['limit'] = []
            structures.append(structure)

        print("structures: ", structures)

        return structures

    # def complete_values_into_structures(self, structures, values):
    #     """
    #     补全 cypher 结构的值。本模块涉及属性和值的对齐。
    #     :param:
    #         structures ('dict'):
    #             由解析式设计表解析出的 cypher 结构。
    #             example: {"match": ["MATCH (sd:stock_data)"],
    #                       "where": ["WHERE sd.name == <value>
    #                                  WITH sd.opening_price as temp
    #                                  MATCH (sd:stock_data)
    #                                  WHERE sd.opening_price >temp",
    #                                 "WHERE sd.date == date()"],
    #                       "return": ["RETURN sd.name"],
    #                       "order_by": [],
    #                       "limit": []}
    #         values ('dict'):
    #             属性的值。
    #             example: {"sd.name": "国泰君安"}
    #     :return:
    #         completed_structures ('dict'):
    #             补全的 cypher 结构。
    #             example: {"match": ["MATCH (sd:stock_data)"],
    #                       "where": ["WHERE sd.name == '国泰君安'
    #                                  WITH sd.opening_price as temp
    #                                  MATCH (sd:stock_data)
    #                                  WHERE sd.opening_price >temp",
    #                                 "WHERE sd.date == date()"],
    #                       "return": ["RETURN sd.name"],
    #                       "order_by": [],
    #                       "limit": []}
    #     """
    #     return completed_structures

    def combine_structures_into_cypher(self, structures):
        """
        合并 cypher 结构为 cypher 语句。本模块涉及多种组合如何选择，以及组合的语句能否执行。
        :param:
            structures (dict):
                补全的 cypher 结构。
                example: {"match": ["MATCH (sd:stock_data)"],
                          "where": ["WHERE sd.name == '国泰君安'
                                     WITH sd.opening_price as temp
                                     MATCH (sd:stock_data)
                                     WHERE sd.opening_price > temp",
                                    "WHERE sd.date == date()"],
                          "return": ["RETURN sd.name"],
                          "order_by": [],
                          "limit": []}
        :return:
            cypher_statement (string):
                待执行的 cypher 语句。
                example: "MATCH (sd:stock_data)
                          WHERE sd.name == '国泰君安' and sd.date == date()
                          WITH sd.opening_price as temp
                          MATCH (sd:stock_data)
                          WHERE sd.opening_price >temp and sd.date == date()",
                          RETURN sd.opening_price"
        """
        return self.generate_cypher_model.generate_cypher(structures)



    def link_completion(self, tags):
        """
        链路补全。
        :param:
            tags (list):
                query 中涉及的 tags。
                example: ["fund_manager", "stock"]
        :return:
            completed_tags (list):
                补全的 tags。
                example: ["fm:fund_manager", "pof:public_offering_fund", "s:stock"]
        """
        completed_tags = self.link_builder.link_completion_(tags)
        return completed_tags

    # def detect_with_statement(self, statement):
    #     """
    #     检测生成的 WHERE 语句中是否存在 WITH 语句。
    #     :param:
    #         statement ('string'):
    #             匹配得到的 WHERE 解析式。
    #             example: "WHERE sd.date = date() AND sd.name =“国泰君安”
    #                       WITH sd.closing_price as temp
    #                       WHERE sd.date = date() AND sd.closing_price > temp"
    #     :return:
    #         exist_with ('bool'):
    #             检测是否存在 WITH 语句的结果。
    #             example: True
    #     """
    #     return exist_with

    def splice_match_into_where_statement(self, match_statement, where_analytic_expressions):
        """
        拼接 MATCH 语句到 WHERE 语句中。
        :param:
            match_statement (string):
                用于拼接的 MATCH 语句。
                example: "MATCH (sd:stock_data)"
            where_analytic_expressions (list):
                待拼接的 WHERE 语句。
                    example: ["WHERE sd.name == <value>
                               WITH sd.opening_price as temp
                               WHERE sd.opening_price >temp",
                              "WHERE sd.date == date()"]
        :return:
            spliced_where_statement (list):
                拼接后的 WHERE 语句。
                example: ["WHERE sd.name == <value>
                           WITH sd.opening_price as temp
                           MATCH (sd:stock_data)
                           WHERE sd.opening_price >temp",
                          "WHERE sd.date == date()"]
        """

        # print('match_statement: ', match_statement)
        # print('where_analytic_expressions: ', where_analytic_expressions)

        spliced_where_statements = self.keywords2where_model.splice_match_into_where_statement(match_statement, where_analytic_expressions)

        return spliced_where_statements

    def match_analytic_structure_for_match(self, entities, properties, time):
        """
        生成 MATCH 语句。本模块向 "match_analytic_structure" 模块提供 MATCH 语句。
        :param:
            entities (list):
                用于匹配解析式的实体关键词。
                example: ["股票"]
            properties (list):
                用于匹配解析式的属性关键词。The properties used to match keywords.
                example: ["开盘价"]
            time (list):
                用于匹配解析式的时间关键词。
                example: ["今日"]
        :return:
            match_analytic_expressions (list):
                由 MATCH 解析式设计表解析的 MATCH 语句。
                example: ["MATCH (sd:stock_data)"]
        """
        # 生成match语句
        match_analytic_expressions = self.match_mapper.keywords2match(entities, properties, time)
        tags = self.match_mapper.extract_tags_for_match(match_analytic_expressions)
        tags = self.link_completion(tags)
        match_analytic_expressions = self.match_mapper.tags2match(
            tags, self.link_builder.entities_map, self.link_builder.graph)
        return match_analytic_expressions

    def match_analytic_structure_for_where(self, conditions):
        """
        生成 WHERE 语句。本模块向 "match_analytic_structure" 模块提供 WHERE 语句。
        :param:
            conditions (list):
                用于匹配解析式的条件关键词。
                example: ["<property>高于<entity>", "今日"]
        :return:
            where_analytic_expressions (list):
                由 WHERE 解析式设计表解析的 WHERE 语句。
                example: ["WHERE sd.name == <value>
                           WITH sd.opening_price as temp
                           WHERE sd.opening_price >temp",
                          "WHERE sd.date == date()"]
        """

        conds = []
        for i in range(len(conditions)):
            conds.append(conditions[i]["condition"])

        where_analytic_expressions = self.keywords2where_model.match_analytic_structure_for_where(conds)

        return where_analytic_expressions

    def match_analytic_structure_for_return(self, returns, match_analytic_expression):
        """
        生成 RETURN 语句。本模块向 "match_analytic_structure" 模块提供 RETURN 语句。
        :param:
            returns (list):
                用于匹配解析式的返回关键词。
                example: ["的<entity>"]
            match_analytic_expressions (string):
                MATCH 子句
                example: "MATCH (sfi:stock_financial_information)<-[hsfi:has_stock_financial_information]-(s:stock)-[bt:belong_to]->(t:trade)"
        :return:
            return_analytic_expressions (list):
                由 RETURN 解析式设计表解析的 RETURN 语句。
                example: ["RETURN sd.name"]
        """

        return_analytic_expressions = self.return_mapper.keywords2return(returns, match_analytic_expression)
        return return_analytic_expressions

    def sort_cypher_with_similarity(self, query, cyphers):
        """
        根据相似度排序 cypher。
        :param:
            query (string):
                用户问题。
            cyphers (list):
                待排序的 cypher 语句
        :return:
            max_cypher: 最相似的cypher语句
            sorted_cyphers: 根据相似度从大到小排序好的cypher语句列表
        """
        max_cypher, sorted_cyphers = self.sortcyphermodel.sort_cypher(query, cyphers)
        return max_cypher, sorted_cyphers

    def get_intent(self, cypher):
        """
        根据 cypher 语句获取意图
        :param:
            cypher (string):
                cypher 语句。
        :return:
            intent_dict (dict):
                识别出的意图
                example: {'conditions': ['股票财务信息更新季度等于2.00', '行业名称等于零售', '股票财务信息更新年份等于2023.00'], 'returns': ['股票财务信息更新年份', '股票名称', '股票财务信息利润', '股票财务信息更新季度']}
        """
        return intent_recognition_from_cypher(cypher, self.translate_en2zh)

    def find_all_entities2dict(self, query):
        """
        找到所有实体字典
        :param:
            query (string):
                用户问题。
        :return:
            entities_dict (dict):
                实体字典
        """
        return self.extract_query_elemen_model.find_all_entities2dict(query)

if __name__ == "__main__":
    keywords = {"entities": ["股票"], "properties": ["开盘价"], "time": ["今日"],
                "conditions": [{"condition": "<property>高于<entity>", "varieties": {"property": ["开盘价"], "entity": ["国泰君安"]}},
                               {"condition": "今日"}],
                "returns": [{"return": "<entity>", "varieties": {"entity": ["股票"]}}]}

    # query="今日开盘价高于国泰君安的股票"
    # query="中欧医疗健康混合c和易方达蓝筹精选混合的销售状态和最新净值"
    # query="董事长为贺青的上市公司"
    # query="张坤管理的股票有哪些"
    # query="国泰君安的三日内累计成交量"

    # KBQA语料测试
    # query = "国泰君安"
    # query = "易方达蓝筹精选混合"
    # query = "国泰君安的收盘价"
    # query = "易方达蓝筹精选混合的净值"
    # query = "2021年9月23日国泰君安的收盘价"
    # query = "2021年9月23日易方达蓝筹精选混合的净值"
    # query = "生态园林的现价和最高价"
    # query = "易方达蓝筹精选混合的收益率和净值"
    # query = "2021年9月23日国泰君安的开盘价和收盘价"
    # query = "2021年9月23日易方达蓝筹精选混合的收益率和净值"
    # query = "国泰君安和中信证券的开盘价和收盘价和3日累计涨幅"
    # query = "中欧医疗健康混合c和易方达蓝筹精选混合的收益率和净值"
    # query = "2021年9月23日国泰君安和中信证券的开盘价和收盘价和3日平均成交额"
    # query = "2021年9月23日中欧医疗健康混合c和易方达蓝筹精选混合的收益率和净值"
    # query = "国泰君安和中信证券的3日累计涨幅"
    # query = "中欧医疗健康混合c和易方达蓝筹精选混合的净值"
    # query = "2021年9月23日国泰君安和中信证券的3日平均成交额"
    # query = "2021年9月23日中欧医疗健康混合c和易方达蓝筹精选混合的净值"
    # query = "收盘价大于15的股票有哪些"
    # query = "董事长为贺青的上市公司"
    # query = "上市时间早于2022年的上市公司"
    # query = "净值大于1的基金"
    # query = "昨日最低价小于20且昨日开盘价小于30的股票有哪些"
    # query = "净值大于1且收益率大于10%的基金"
    # query = "收盘价前10的股票有哪些"
    # query = "净值前3的基金"
    # query = "收盘价最大的股票是哪个"
    # query = "净值最高的基金"
    # query = "最近国泰君安的收盘价"
    # query = "最近易方达蓝筹精选混合的净值"
    # query = "涨幅较大的股票"
    # query = "绩优股有哪些"
    # query = "前几天国泰君安的涨幅情况"
    # query = "国泰君安的基本情况"
    # query = "收盘价靠前的股票"
    # query = "市盈率大于10的券商行业股票"
    # query = "券商行业的成份股"
    # query = "国泰君安和中信证券的收盘价差多少？"
    # query = "中欧医疗健康混合c和易方达蓝筹精选混合的净值差多少"
    # query = "国泰君安的收盘价大于10元吗？"
    # query = "中欧医疗健康混合c的净值大于1吗"
    # query = "国泰君安的净利润同比"
    # query = "易方达蓝筹精选混合的净值环比"
    # query = "持有东方财富且持股比例大于3%的基金？"
    # query = "易方达蓝筹持仓比例大于3%的股票"
    # query = "社保基金持股比例大于3%的股票所属的行业？"
    # query = "市值高于国泰君安的股票"
    # query = "净值大于易方达蓝筹精选混合的基金"
    # query = "同时属于互联网行业和国产软件行业的股票"
    # query = "和同花顺相同行业的股票有哪些？"
    # query = "去年国泰君安第一季度净利润"
    # query = "去年国泰君安净利润"
    # query = "收盘价大于100"
    # query = "收盘价1-2元"
    # query = "锂电池涂布机的下游产业有哪些"
    # query = "张坤管理的股票"
    # query = "比亚迪经营产业关联的股票"
    # query = "国泰君安涨跌幅行业排名"
    # query = "科大讯飞为什么大涨"

    # 未解答问题
    # query = "明日新上市股"
    # query = "量比1~3的股票有哪些"
    # query = "最近三个交易日连续下跌"
    # query = "4月10日有什么新股上市?"
    # query = "波动率上升的股票"
    # query = "最近一周涨跌幅≥30%的股票"
    # query = "北向资金流入个股"
    # query = "今天成交量比昨天成交量小一倍的股"
    # query = "涨幅大于3%小于5%,换手率大5%小于10%,均线多头发散,量比大于1"
    # query = "每股收益大于0元,上市时间超过1年,所属中证200成分股,市值从小到大排名?"
    # query = "收盘价小于等于8,今日的涨跌幅大于3%小于等于9.5%,市场上海A股或市场深圳A股,剔除ST股,上周五成交量手数大于上周四成交量手数3倍以上,非创业板"
    # query = "神火股份什么时候发股息?"
    # query = "10日内2次涨停，市值千亿以上"
    # query = "首次涨停的股票"
    # query = "10天内最高点创25周新高,连续20天10日均线大于20日均线,10天内5日均线上穿10日均线,5天区间内振幅大于5%,连续20天20日均线大于30日均线,周线5周均线大于10周均线大于20周均线大于30周均线,连续3周收盘价大于10周均线,现价小于30,非创业板,非科创板"
    # query = "立昂微5月18日召开股东大会吗"
    # query = "南山铝业2023年除权除息是什么时间"
    # query = "近一年股东人数减少50%一80%的股票"
    # query = "股票名字含有动物的股票"
    # query = "今天摘帽复牌的股票"
    # query = "打5折的股票有哪些"
    # query = "九点15到九点19竞价跌幅大于15%的股票"
    # query = "华泰证券涨停连板天数"
    # query = "近一个月交易额最高的股票?"
    # query = "百字开头的股票？"
    # query = "如何查看当日全部涨停股票"
    # query = "阳包阴的股票"
    # query = "选出十天内涨停过的股票"
    # query = "5至7元的股票"
    # query = "10天内首次涨停的股票"
    # query = "同时属于浙江板块和软件行业的股票"
    # query = "净利润大于1百万的股票"
    # query = "和国泰君安相同城市的股票"
    query = "上游产业是锂电池专用生产设备的公司"
    # query = "宁德时代本月的涨幅"
    # query = "本周最新净值最小的基金"
    # query = "不包含天赐材料的基金"
    # query = "停牌的股票有哪些"
    # query = "2022基金收益率第三名的基金"
    # query = "一年内涨幅最大的股票"
    # query = "中欧基金是葛兰管理的吗"
    # query = "502000和519602近五年是否获得过金牛基金奖"
    # query = "横盘6年的股票"
    # query = "上影线高开，5日涨幅前50，剔除涨停板"
    # query = "昨日首板涨停"
    # query = "全面注册制利好哪些板块"
    # query = "上市公司毛利率排行"
    # query = "三线穿阴的票"
    # query = "连续8天阴线的股票"
    # query = "沿五日均线上行的股票"

    # query = "年涨跌幅大于20%的股票"
    # query = "601211的发行价格"

    parser=Parser()
    cyphers=parser.parse(query)
    # print('cypher statement: ', cyphers)
    for cypher in cyphers:
        for c in cypher:
            print(c)