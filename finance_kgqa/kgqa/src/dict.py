"""
定义系统需要的字典
"""

import json
import re
import pandas as pd


def init_dicts():
    """
    初始化系统用到的字典
    :param:
    :return:
    """

    global initial_dicts
    initial_dicts = {}
    # ExtractQueryElementModel 字典初始化
    # 导入实体map以及属性map，遍历分词list判断是否存在实体/属性
    with open("kgqa/resources/parse/entities_map.json", "r", encoding="utf-8") as ef, \
            open("kgqa/resources/parse/properties_map.json", "r", encoding="utf-8") as pf:
        initial_dicts["entities_map"] = json.load(ef)
        initial_dicts["properties_map"] = json.load(pf)
    # 取出属性名列表
    # properties = ["注册资本", "上市板块", "成立日期", "省份", "开盘价", "收盘价", "最高价", "最低价", "涨跌幅", "成交量", "涨停连板天数", "涨停", "红三兵", "净利润", "存货周转率"]
    with open("kgqa/resources/parse/where_properties.json", "r", encoding="utf-8") as pf:
        properties_data = json.load(pf)
        flat_list = [re.escape(item) for sublist in properties_data.values() for item in sublist]
        initial_dicts["properties"] = flat_list
    # 取出实体名列表
    with open("kgqa/resources/parse/where_entities.json", "r", encoding="utf-8") as pf:
        entities_data = json.load(pf)
        flat_list = [re.escape(item) for sublist in entities_data.values() for item in sublist]
        # self.entities = set(flat_list)
        initial_dicts["entities"] = flat_list
    # 取出时间列表
    with open("kgqa/resources/parse/times.json", "r", encoding="utf-8") as pf:
        time_data = json.load(pf)
        flat_list = [item for sublist in time_data.values() for item in sublist]
        initial_dicts["times"] = flat_list
    # 取出时间span列表
    with open("kgqa/resources/parse/timespans.json", "r", encoding="utf-8") as pf:
        timespan_data = json.load(pf)
        flat_list = [item for sublist in timespan_data.values() for item in sublist]
        initial_dicts["timespans"] = set(flat_list)
    # 取出实体和属性的模糊词映射
    with open("kgqa/resources/parse/where_entities_map.json", "r", encoding="utf-8") as pf:
        # self.entities = set(flat_list)
        initial_dicts["where_entities_map"] = json.load(pf)
    with open("kgqa/resources/parse/where_properties_map.json", "r", encoding="utf-8") as pf:
        # self.entities = set(flat_list)
        initial_dicts["where_properties_map"] = json.load(pf)
    # 根据where字典进行正则表达式映射，判断是否属于该where表达式
    with open("kgqa/resources/parse/where_regular.json", "r", encoding="utf-8") as wr:
        initial_dicts["where_regular"] = json.load(wr)
    with open("kgqa/resources/parse/where_regular_special.json", "r", encoding="utf-8") as wrs:
        initial_dicts["where_regular_special"] = json.load(wrs)
    # 根据return字典进行正则表达式映射，判断是否属于该return表达式
    with open("kgqa/resources/parse/return_regular.json", "r", encoding="utf-8") as rr:
        initial_dicts["return_regular"] = json.load(rr)
    # 读入return专用的entity字典
    with open("kgqa/resources/parse/where_entities.json", "r", encoding="utf-8") as ren:
        initial_dicts["return_entities_dict"] = json.load(ren)
        # self.return_entities = [item for sublist in self.return_entities_dict.values() for item in sublist]
        initial_dicts["return_entities"] = [re.escape(item) for sublist in initial_dicts["return_entities_dict"].values() for item in sublist]
    # 读入tag字典
    with open("kgqa/resources/parse/tags.json", "r", encoding="utf-8") as pf:
        tag_data = json.load(pf)
        flat_list = [item for sublist in tag_data.values() for item in sublist]
        initial_dicts["tags"] = flat_list
    # 读入连接词字典
    with open("kgqa/resources/parse/conjunctions.json", "r", encoding="utf-8") as pf:
        conjunction_data = json.load(pf)
        flat_list = [item for sublist in conjunction_data.values() for item in sublist]
        initial_dicts["conjunction"] = flat_list
    # 读入否定词字典
    with open("kgqa/resources/parse/negative_words.json", "r", encoding="utf-8") as pf:
        negative_data = json.load(pf)
        flat_list = [item for sublist in negative_data.values() for item in sublist]
        initial_dicts["negative"] = flat_list
    # 读入布尔值字典
    with open("kgqa/resources/parse/bool_properties.json", "r", encoding="utf-8") as pf:
        bool_data = json.load(pf)
        flat_list = [item for sublist in bool_data.values() for item in sublist]
        initial_dicts["bool"] = flat_list

    # MatchMapper 字典初始化
    df = pd.read_csv("kgqa/resources/parse/match.csv", encoding="utf8")
    df = df.fillna("-")
    initial_dicts["match"] = df

    # LinkBuilder 字典初始化
    with open("kgqa/resources/parse/schema.json", "r", encoding="utf-8") as f:
        initial_dicts["schema_entities"], initial_dicts["schema_relations"] = json.load(f)

    # ReturnMapper 字典初始化
    with open("kgqa/resources/parse/return_map.json", "r", encoding="utf8") as f:
        initial_dicts["mapping"] = json.load(f)
    with open("kgqa/resources/parse/translate_zh2en.json", "r", encoding="utf8") as f:
        initial_dicts["translator"] = json.load(f)
    with open("kgqa/resources/parse/entities_map.json", "r", encoding="utf8") as f:
        initial_dicts["ent_map"] = json.load(f)
    with open("kgqa/resources/parse/properties_map_en.json", "r", encoding="utf8") as f:
        initial_dicts["pro_map"] = json.load(f)

    # Keywords2WhereModel 字典初始化
    with open("kgqa/resources/parse/where_parse.json", 'r', encoding='utf-8') as file:
        initial_dicts["where_parse"] = json.load(file)

    # VariatesAlignmentModel 字典初始化
    with open("kgqa/resources/parse/entities_map.json", 'r', encoding='utf-8') as file:
        initial_dicts["entities_map"] = json.load(file)
    with open("kgqa/resources/parse/properties_map.json", 'r', encoding='utf-8') as file:
        initial_dicts["properties_map"] = json.load(file)
    with open("kgqa/resources/parse/properties_map_en.json", 'r', encoding='utf-8') as file:
        initial_dicts["properties_map_en"] = json.load(file)
    with open("kgqa/resources/parse/translate_zh2en.json", 'r', encoding='utf-8') as file:
        initial_dicts["translate_zh2en"] = json.load(file)
    with open("kgqa/resources/parse/translate_en2zh.json", 'r', encoding='utf-8') as file:
        initial_dicts["translate_en2zh"] = json.load(file)

    # intent_recognition 字典初始化
    with open("kgqa/resources/parse/intent_map.json", 'r', encoding='utf-8') as file:
        initial_dicts["intent_map"] = json.load(file)

    # llm entities 字典初始化
    with open("kgqa/resources/parse/entities.json", 'r', encoding='utf-8') as file:
        initial_dicts["llm_parse_entities"] = json.load(file)
    # llm properties 字典初始化
    with open("kgqa/resources/parse/properties.json", 'r', encoding='utf-8') as file:
        initial_dicts["llm_parse_properties"] = json.load(file)

    # # llm entities 字典初始化
    # with open("kgqa/resources/llm_parse/entities.json", 'r', encoding='utf-8') as file:
    #     initial_dicts["llm_parse_entities"] = json.load(file)
    # # llm tags_properties_map_en 字典初始化
    # with open("kgqa/resources/llm_parse/tags_properties_map_en.json", 'r', encoding='utf-8') as file:
    #     initial_dicts["llm_parse_tags_properties_map_en"] = json.load(file)
    # # llm properties 字典初始化
    # with open("kgqa/resources/llm_parse/properties.json", 'r', encoding='utf-8') as file:
    #     initial_dicts["llm_parse_properties"] = json.load(file)

    with open("kgqa/resources/parse/where_properties.json", "r", encoding="utf-8") as pf:
        properties_data = json.load(pf)
        flat_list = [item for sublist in properties_data.values() for item in sublist]
        initial_dicts["properties_preprocess"] = flat_list
    # 取出实体名列表
    with open("kgqa/resources/parse/where_entities.json", "r", encoding="utf-8") as pf:
        entities_data = json.load(pf)
        flat_list = [item for sublist in entities_data.values() for item in sublist]
        initial_dicts["entities_preprocess"] = flat_list
    # 读取特殊问法模板
    with open("kgqa/resources/parse/special_question_regular.json", "r", encoding="utf-8") as pf:
        regular_data = json.load(pf)
        initial_dicts["special_question"] = regular_data 


def get_dicts():
    """
    获取系统用到的字典
    :param:
    :return:
        initial_dicts (dict):
            系统用到的字典
    """
    return initial_dicts
