"""
大模型解析器，为大模型抽取 relevant schema
"""

import jieba

from kgqa.src.utils.link_completion import *
from kgqa.src.utils.schema_related_func import *
from kgqa.src.utils.intent_recognition import intent_recognition_from_cypher

class LLM_parser:
    """
    LLM解析式类。将自然语言解析为 cypher 语句。
    :param:
        dicts (dict):
            kgqa/src/dict.py 中的预定义字典
    """

    def __init__(self, dicts):
        self.llm_parse_entities = dicts["llm_parse_entities"]
        self.llm_parse_properties = dicts["llm_parse_properties"]
        self.translate_zh2en = dicts["translate_zh2en"]
        self.translate_en2zh = dicts["translate_en2zh"]

        self.link_builder = LinkBuilder(dicts)
        self.special_hold_token = ['持股比例', '持股']
        self.special_scale_token = ['最新规模', '规模']
        # self.special_information_token = ['涨', '跌']

    def special_hold(self, related_tags):
        """
        判断问题中的“持股比例”指的是 public_offering_fund 还是 stockholder，并添加到 related tags。
        :param:
            related_tags (list):
                当前抽取到的相关 tags
                example: ["stock", "stock_data"]
        :return:
            related_tags (list):
                添加后的相关 tags
                example: ["stock", "stock_data", "stockholder"]
        """
        if 'public_offering_fund' in related_tags and 'stockholder' in related_tags:
            return related_tags
        if 'public_offering_fund' in related_tags:
            return related_tags
        if 'stockholder' in related_tags:
            return related_tags
        return related_tags.append('stockholder')

    def special_scale(self, related_tags):
        """
        判断问题中的“规模”指的是 public_offering_fund 还是 industry，并添加到 related tags。
        :param:
            related_tags (list):
                当前抽取到的相关 tags
                example: ["stock", "stock_data"]
        :return:
            related_tags (list):
                添加后的相关 tags
                example: ["stock", "stock_data", "public_offering_fund"]
        """
        if 'public_offering_fund' in related_tags and 'industry' in related_tags:
            return related_tags
        if 'public_offering_fund' in related_tags:
            return related_tags
        if 'industry' in related_tags:
            return related_tags
        related_tags.append('public_offering_fund')
        return related_tags


    def gen_prompt(self, question):
        """
        判断问题中的“规模”指的是 public_offering_fund 还是 industry，并添加到 related tags。
        :param:
            question (string):
                自然语言问题。
                example: "零售行业的股票2023年第二季度的净利润是多少"
        :return:
            prompt (string):
                提供给 llm 的 prompt
                example: "你是一位 NebulaGraph Cypher 专家，请根据给定的图 Schema 和问题，写出Cypher查询语句，属性和边只能用schema中给定的字段。
                          schema 如下：
                          ---
                          "Node_properties": {{'tag': 'stock', 'properties': [('code', 'string'), ('name', 'string'), ('chinese_spelling', 'string'), ('registered_capital', 'float'), ('listed_sector', 'string'), ('establishment_date', 'date'), ('province', 'string')]},{'tag': 'stock_financial_information', 'properties': [('type', 'string'), ('date', 'date'), ('year', 'int64'), ('half_year', 'int64'), ('quarter', 'int64'), ('net_profit', 'float'), ('inventory_turnover', 'float')]},{'tag': 'trade', 'properties': [('code', 'string'), ('name', 'string')]}}, "Edge_properties": {{'edge': 'belong_to', 'start_tag': 'stock', 'end_tag': 'trade', 'properties': []},{'edge': 'has_stock_financial_information', 'start_tag': 'stock', 'end_tag': 'stock_financial_information', 'properties': []}}
                          ---
                          问题如下：
                          ---
                          零售行业的股票2023年第2季度的净利润是多少
                          ---
                          下面写出Cypher查询语句：
                          ---"
            pure_completed_related_tags (list):
                相关 tags
                example: ['trade', 'stock', 'stock_financial_information']
            related_edgeTypes (dict):
                相关 edgeTypes
                example: {'belong_to': {'edge': 'belong_to', 'start_tag': 'stock', 'end_tag': 'trade', 'properties': []}, 'has_stock_financial_information': {'edge': 'has_stock_financial_information', 'start_tag': 'stock', 'end_tag': 'stock_financial_information', 'properties': []}}
        """
        related_tags = []

        phrases = jieba.lcut(question)
        print("phrases: ", phrases)

        # 将 llm_parse_entities 字典（即 kgqa/resources/parse/entities.json）涉及 question 中实体的 tag 抽取出来。跳过需要特殊判断的 token
        for phrase in phrases:
            for k, v in self.llm_parse_entities.items():
                if phrase in v and phrase not in self.special_hold_token and phrase not in self.special_scale_token:
                    related_tags.append(self.translate_zh2en[k])
        # print("related_tags: ", related_tags)

        # 由于股票和行业存在大量相同的属性名，所以判断 question 中提到属性属于哪个 tag
        for k, v in self.llm_parse_properties.items():
            for i in range(len(v)):   # 遍历当前tag下所有properties
                if v[i] in question and k in self.translate_zh2en and self.translate_zh2en[k] not in related_tags:
                    if self.translate_zh2en[k] in ["stock_data", "trade_data", "stock_financial_information"]:  # 这三个 tag 下的属性才需要判断
                        if self.translate_zh2en[k].split("_")[0] in related_tags:   # 若 trade 已在 related_tags 中，则添加 trade_data。stock 同理
                            related_tags.append(self.translate_zh2en[k])
                        elif "stock" not in related_tags and "trade" not in related_tags:   # 若都不存在，则都添加
                            related_tags.append(self.translate_zh2en[k])
                            related_tags.append(self.translate_zh2en[k].split("_")[0])

        # 需要特殊判断的 token
        if any([i in question for i in self.special_hold_token]):
            related_tags = self.special_hold(related_tags)
        if any([i in question for i in self.special_scale_token]):
            related_tags = self.special_scale(related_tags)

        # 判断“涨”和“跌”
        if "为什么" in question and ("涨" in question or "跌" in question):
            related_tags.append("information")
        elif "涨" in question or "跌" in question:
            if "stock" in related_tags:
                related_tags.append("stock_data")
            if "trade" in related_tags:
                related_tags.append("trade_data")

        # 链路补全
        related_tags = list(set(related_tags))
        completed_related_tags = self.link_builder.link_completion_(related_tags)
        pure_completed_related_tags = []
        for tag in completed_related_tags:
            pure_completed_related_tags.append(tag.split(':')[1])
        pure_completed_related_tags = list(set(pure_completed_related_tags))

        # 首尾 tag 都在 pure_completed_related_tags 中，则添加该 edgeType
        related_edgeTypes = {}
        for k, v in schema_dic["Edge_properties"].items():
            if v["start_tag"] in pure_completed_related_tags and v["end_tag"] in pure_completed_related_tags:
                related_edgeTypes[k] = v

        # 对自环的 edgeType 单独判断
        if "affect" in related_edgeTypes and "上游" not in question and "下游" not in question:
            del related_edgeTypes["affect"]

        print("extracted tags: ", pure_completed_related_tags)
        print("extracted edgeTypes: ", related_edgeTypes)

        prompt = get_prompt_relative(question, pure_completed_related_tags, related_edgeTypes)
        print("prompt: ", prompt)

        return prompt, pure_completed_related_tags, related_edgeTypes

    def get_intent(self, cypher):
        return intent_recognition_from_cypher(cypher, self.translate_en2zh)
