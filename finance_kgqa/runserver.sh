if [ "$1" = "detach" ]; then
    nohup python3 manage.py runserver 0.0.0.0:8001 > nohup.out  2>&1 &
else
    nohup python3 manage.py runserver 0.0.0.0:8001
fi
