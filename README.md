# Question answering system synchronizing with dynamic knowledge graph in Finance

Synchronized Finance KGQA is a text-to-cql system that is applied in financial scenarios. To enhance its reliability, two sub-systems are constructed and respond to user's questions. The system ultimately determines the final response based on confidence and related factors, ensuring that the answers have a high upper bound and a sufficient lower bound.

## Finance KGQA

Directory 'finance_kgqa' contains the source code of a heuristically cold-start system, which can give answers to financial questions of natural language according to predefined principles. Furthermore, this system can also integrate with LLM response to produce reliable answers. You can move to the directory for detailed information.
```
cd finance_kgqa
```

## LLM Tuning

Directory 'LLM-Tuning' involves an LLM-based system that obtain answers by prompting a fine-tuned LLM with questions. Additonally, codes of fine-tuning and data are also included, namely you can fine-tune your own language models with the materials. For more information, move to this directory with following command.
```
cd LLM-Tuning
```

## Cite our papers
```bibtex
@InProceedings{10.1007/978-3-031-70371-3_32,
    author="Tao, Wenbiao
    and Zhu, Hanlun
    and Tan, Keren
    and Wang, Jiani
    and Liang, Yuanyuan
    and Jiang, Huihui
    and Yuan, Pengcheng
    and Lan, Yunshi",
    editor="Bifet, Albert
    and Daniu{\v{s}}is, Povilas
    and Davis, Jesse
    and Krilavi{\v{c}}ius, Tomas
    and Kull, Meelis
    and Ntoutsi, Eirini
    and Puolam{\"a}ki, Kai
    and {\v{Z}}liobait{\.{e}}, Indr{\.{e}}",
    title="FinQA: A Training-Free Dynamic Knowledge Graph Question Answering System in Finance with LLM-Based Revision",
    booktitle="Machine Learning and Knowledge Discovery in Databases. Research Track and Demo Track",
    year="2024",
    publisher="Springer Nature Switzerland",
    address="Cham",
    pages="418--423",
    abstract="Knowledge graph question answering (KGQA) in the finance domain aims to answer questions based on a dynamic knowledge graph (KG), which suffers from frequent updates. Moreover, the lack of high-quality annotated data renders data-driven and training-dependent approaches ineffective. To bridge the gap, we develop FinQA, which is a training-free dynamic knowledge graph question answering system in finance with large language model based (LLM-based) revision. Specifically, FinQA gives considerations to the following aspects: (1) constructing a dynamic finance knowledge graph partitioned based on data update frequencies; (2) proposing a training-free question-answering (QA) system to parse natural language to graph query language (NL2GQL) and achieving high-efficient coordination with the dynamic KG; (3) integrating the QA system with an open-source LLM to further boost the accuracy.",
    isbn="978-3-031-70371-3"
}


