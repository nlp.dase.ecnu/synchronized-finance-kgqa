import argparse
import json
from tqdm import tqdm
import datasets
import transformers
from transformers import AutoTokenizer, LlamaTokenizer
import os,shutil

 
parser = argparse.ArgumentParser()
parser.add_argument("--model_path", type=str, help="checkpoint, like `THUDM/chatglm-6b`")
parser.add_argument("--input_file", type=str,
                    help="Instruction 数据文件地址，文件中每一行都是json格式，包含一个输出和一个输出")
parser.add_argument("--prompt_key", type=str, default=f"prompt", help="你的jsonl文件里，Instruction 的输入字段是什么")
parser.add_argument("--target_key", type=str, default=f"target", help="你的jsonl文件里，Instruction 的输出字段是什么")
parser.add_argument("--save_name", type=str, default=f"temp", help="经过tokenize之后的数据集的存放位置")
parser.add_argument("--max_seq_length", type=int, default=2040, help="最长输入限制")
parser.add_argument("--skip_overlength", type=bool, default=False, help="是否跳过超长句")
parser.add_argument("--prompt_type", type=str, default=f"original",
                    help="定义prompt的样式: ['original', 'original_schema', 'original_schema_relative', 'original_schema_relative_chinese']")
args = parser.parse_args()
model_checkpoint = args.model_path


# model_checkpoint = "THUDM/chatglm-6b"
# model_checkpoint = "baichuan-inc/baichuan-7B"

schema_str='''{"Node properties": 
 [     {'tag': 'chairman', 
                  'properties': [('name', 'string'),
                                          ('sex', 'string'),
                                          ('appointment_date', 'string'), 
                                          ('departure_date', 'string')
                                         ]}, 
       {'tag': 'fund_manager', 'properties': [('name', 'string'), 
                                              ('sex', 'string'), 
                                              ('degree', 'string')]},
       {'tag': 'industry', 'properties': [('name', 'string'),
                                          ('scale', 'float')]}, 
       {'tag': 'information', 'properties': [('id', 'string'),
                                             ('name', 'string')]},
       {'tag': 'new_stock', 'properties': [('code', 'string'),
                                           ('amount', 'int64'),
                                           ('online_release_date', 'string')]}, 
       {'tag': 'public_offering_fund', 'properties': [('code', 'string'), 
                                                      ('name', 'string'), 
                                                      ('sales_status', 'string'),
                                                      ('management_fee', 'float'), 
                                                      ('scale', 'float'), 
                                                      ('subscription_start_time', 'datetime'), 
                                                      ('subscription_end_time', 'datetime'),
                                                      ('latest_net_worth', 'float')]}, 
       {'tag': 'stock', 'properties': [('code', 'string'), 
                                       ('name', 'string'), 
                                       ('chinese_spelling', 'string'), 
                                       ('registered_capital', 'float'),
                                       ('listed_sector', 'string'),
                                       ('establishment_date', 'date'), 
                                       ('province', 'string')]}, 
       {'tag': 'stock_data', 'properties': [('date', 'date'),#
                                            ('opening_price', 'float'),
                                            ('closing_price', 'float'), 
                                            ('maximum_price', 'float'),
                                            ('minimum_price', 'float'), 
                                            ('range', 'float'), 
                                            ('volume', 'int64'), 
                                            ('raising_limit_days', 'int64'), 
                                            ('raising_limit', 'bool'), 
                                            ('hong_san_bing', 'bool')]}, 
       {'tag': 'stock_financial_information', 'properties': [('type', 'string'),
                                                             ('date', 'date'),
                                                             ('year', 'int64'),
                                                             ('half_year', 'int64'),
                                                             ('quarter', 'int64'),
                                                             ('net_profit', 'float'),
                                                             ('inventory_turnover', 'float')]},
       {'tag': 'stockholder', 'properties': [('name', 'string')]}, 
       {'tag': 'trade', 'properties': [('code', 'string'),
                                       ('name', 'string')]}, 
       {'tag': 'trade_data', 'properties': [('date', 'date'), 
                                            ('opening_price', 'float'), 
                                            ('closing_price', 'float'), 
                                            ('maximum_price', 'float'),
                                            ('minimum_price', 'float'),
                                            ('range', 'float')]}
  ],
  "Edge properties": [ {'edge': 'originate_from','start_tag':'stock','end_tag':'new_stock', 'properties': ['marketing_time']},
                       {'edge': 'is_chairman_of', 'start_tag':'chairman','end_tag':'stock','properties': []},
                       {'edge': 'is_stockholder_of','start_tag':'stockholder','end_tag':'stock', 'properties': ['shareholding_ratio','circulation_shareholding_ratio']},
                       {'edge': 'belong_to', 'start_tag':'stock','end_tag':'trade','properties': []},
                       {'edge': 'hold','start_tag':'public_offering_fund','end_tag':'stock', 'properties': ['position_ratio','position_amount']},
                       {'edge': 'manage','start_tag':'fund_manager','end_tag':'public_offering_fund', 'properties': []}, 
                       {'edge': 'involve', 'start_tag':'stock','end_tag':'information','properties': ['tendency']},
                       {'edge': 'involve', 'start_tag':'public_offering_fund','end_tag':'information','properties': ['tendency']},
                       {'edge': 'associate', 'start_tag':'stock','end_tag':'industry','properties': []},
                       {'edge': 'affect', 'start_tag':'industry','end_tag':'industry','properties': []}, 
                       {'edge': 'has_stock_data', 'start_tag':'stock','end_tag':'stock_data','properties': []},
                       {'edge': 'has_stock_financial_information','start_tag':'stock','end_tag':'stock_financial_information', 'properties': []}, 
                       {'edge': 'has_trade_data','start_tag':'trade','end_tag':'trade_data', 'properties': []},   
                     ]
}'''

prompt_str='''
question : {%s};
cypher : 
'''

prompt_str_new='''你是一位 NebulaGraph Cypher 专家，请根据给定的知识图谱的Schema 和问题，直接写出对应的Cypher查询语句。
schema 如下：
---
%s
---
问题如下：
---
%s
---
下面写出Cypher查询语句：
---
'''



# print(prompt_str_new % (schema_str,"国泰君安"))

schema_dic = {
    "Node_properties": {
        'chairman': {'tag': 'chairman', 'properties': [('name', 'string'),
                                          ('sex', 'string'),
                                          ('appointment_date', 'string'),
                                          ('departure_date', 'string')
                                         ]},
        'fund_manager': {'tag': 'fund_manager', 'properties': [('name', 'string'),
                                              ('sex', 'string'),
                                              ('degree', 'string')]},
        'industry': {'tag': 'industry', 'properties': [('name', 'string'),
                                          ('scale', 'float')]},
        'information': {'tag': 'information', 'properties': [('id', 'string'),
                                             ('name', 'string')]},
        'new_stock': {'tag': 'new_stock', 'properties': [('code', 'string'),
                                           ('amount', 'int64'),
                                           ('online_release_date', 'string')]},
        'public_offering_fund': {'tag': 'public_offering_fund', 'properties': [('code', 'string'),
                                                      ('name', 'string'),
                                                      ('sales_status', 'string'),
                                                      ('management_fee', 'float'),
                                                      ('latest_scale', 'float'),
                                                      ('subscription_start_time', 'datetime'),
                                                      ('subscription_end_time', 'datetime'),
                                                      ('latest_net_worth', 'float')]},
        'stock': {'tag': 'stock', 'properties': [('code', 'string'),
                                       ('name', 'string'),
                                       ('chinese_spelling', 'string'),
                                       ('registered_capital', 'float'),
                                       ('listed_sector', 'string'),
                                       ('establishment_date', 'date'),
                                       ('province', 'string')]},
        'stock_data': {'tag': 'stock_data', 'properties': [('date', 'date'),
                                            ('opening_price', 'float'),
                                            ('closing_price', 'float'),
                                            ('maximum_price', 'float'),
                                            ('minimum_price', 'float'),
                                            ('range', 'float'),
                                            ('volume', 'int64'),
                                            ('raising_limit_days', 'int64'),
                                            ('raising_limit', 'bool'),
                                            ('hong_san_bing', 'bool')]},
        'stock_financial_information': {'tag': 'stock_financial_information', 'properties': [('type', 'string'),
                                                             ('date', 'date'),
                                                             ('year', 'int64'),
                                                             ('half_year', 'int64'),
                                                             ('quarter', 'int64'),
                                                             ('net_profit', 'float'),
                                                             ('inventory_turnover', 'float')]},
        'stockholder': {'tag': 'stockholder', 'properties': [('name', 'string')]},
        'trade': {'tag': 'trade', 'properties': [('code', 'string'),
                                       ('name', 'string')]},
        'trade_data': {'tag': 'trade_data', 'properties': [('date', 'date'),
                                            ('opening_price', 'float'),
                                            ('closing_price', 'float'),
                                            ('maximum_price', 'float'),
                                            ('minimum_price', 'float'),
                                            ('range', 'float')]}
    },
  "Edge_properties": {
      'originate_from': {'edge': 'originate_from','start_tag':'stock','end_tag':'new_stock', 'properties': ['marketing_time']},
      'is_chairman_of': {'edge': 'is_chairman_of', 'start_tag':'chairman','end_tag':'stock','properties': []},
      'is_stockholder_of': {'edge': 'is_stockholder_of','start_tag':'stockholder','end_tag':'stock', 'properties': ['shareholding_ratio','circulation_shareholding_ratio']},
      'belong_to': {'edge': 'belong_to', 'start_tag':'stock','end_tag':'trade','properties': []},
      'hold': {'edge': 'hold','start_tag':'public_offering_fund','end_tag':'stock', 'properties': ['position_ratio','position_amount']},
      'manage': {'edge': 'manage','start_tag':'fund_manager','end_tag':'public_offering_fund', 'properties': []},
      'involve_stock': {'edge': 'involve', 'start_tag':'stock','end_tag':'information','properties': ['tendency']},
      'involve_fund': {'edge': 'involve', 'start_tag':'public_offering_fund','end_tag':'information','properties': ['tendency']},
      'associate': {'edge': 'associate', 'start_tag':'stock','end_tag':'industry','properties': []},
      'affect': {'edge': 'affect', 'start_tag':'industry','end_tag':'industry','properties': []},
      'has_stock_data': {'edge': 'has_stock_data', 'start_tag':'stock','end_tag':'stock_data','properties': []},
      'has_stock_financial_information': {'edge': 'has_stock_financial_information','start_tag':'stock','end_tag':'stock_financial_information', 'properties': []},
      'has_trade_data': {'edge': 'has_trade_data','start_tag':'trade','end_tag':'trade_data', 'properties': []}
  }
}

schema_chinese_dic = {
    "Node_properties": {
        'chairman': "{'tag': 'chairman'#董事长, 'properties': [('name', 'string')#姓名, ('sex', 'string')#性别, ('appointment_date', 'string')#任职日期, ('departure_date', 'string')#离职日期]}",
        'fund_manager': "{'tag': 'fund_manager'#基金经理人, 'properties': [('name', 'string')#姓名, ('sex', 'string')#性别, ('degree', 'string')#学历]}",
        'industry': "{'tag': 'industry'#产业, 'properties': [('name', 'string')#名称, ('scale', 'float')#规模]}",
        'information': "{'tag': 'information'#资讯, 'properties': [('id', 'string')#id, ('name', 'string')#名称]}",
        'new_stock': "{'tag': 'new_stock'#新股, 'properties': [('code', 'string')#发行代码, ('amount', 'int64')#发行数量, ('online_release_date', 'string')#网上发行日期]}",
        'public_offering_fund': "{'tag': 'public_offering_fund'#公募基金, 'properties': [('code', 'string')#基金代码, ('name', 'string')#基金名称, ('sales_status', 'string')#销售状态, ('management_fee', 'float')#管理费, ('scale', 'float')#最新规模, ('subscription_start_time', 'datetime')#认购开始时间, ('subscription_end_time', 'datetime')#认购结束时间, ('latest_net_worth', 'float')#最新净值]}",
        'stock': "{'tag': 'stock'#股票, 'properties': [('code', 'string')#股票代码, ('name', 'string')#股票名称, ('chinese_spelling', 'string')#股票名称拼音, ('registered_capital', 'float')#注册资本, ('listed_sector', 'string')#上市板块, ('establishment_date', 'date')#成立日期, ('province', 'string')#省份]}",
        'stock_data': "{'tag': 'stock_data'#股票数据, 'properties': [('date', 'date')#更新时间, ('opening_price', 'float')#开盘价, ('closing_price', 'float')#收盘价, ('maximum_price', 'float')#最高价, ('minimum_price', 'float')#最低价, ('range', 'float')#涨跌幅, ('volume', 'int64')#成交量, ('raising_limit_days', 'int64')#涨停连板天数, ('raising_limit', 'bool')#涨停, ('hong_san_bing', 'bool')#红三兵]}",
        'stock_financial_information': "{'tag': 'stock_financial_information'#股票财务信息, 'properties': [('type', 'string')#类型, ('date', 'date')#更新时间, ('year', 'int64')#更新年份, ('half_year', 'int64')#更新半年, ('quarter', 'int64')#更新季度, ('net_profit', 'float')#净利润, ('inventory_turnover', 'float')#存货周转率]}",
        'stockholder': "{'tag': 'stockholder'#股东, 'properties': [('name', 'string')#公司名称]}",
        'trade': "{'tag': 'trade'#行业, 'properties': [('code', 'string')#行业代码, ('name', 'string')#行业名称]}",
        'trade_data': "{'tag': 'trade_data'#行业数据, 'properties': [('date', 'date')#更新时间, ('opening_price', 'float')#开盘价, ('closing_price', 'float')#收盘价, ('maximum_price', 'float')#最高价, ('minimum_price', 'float')#最低价, ('range', 'float')#涨跌幅]}"
    },
  "Edge_properties": {
      'originate_from': "{'edge': 'originate_from'#来自,'start_tag':'stock'#股票,'end_tag':'new_stock'#新股, 'properties': ['marketing_time'#上市时间]}",
      'is_chairman_of': "{'edge': 'is_chairman_of'#是董事长, 'start_tag':'chairman'#董事长,'end_tag':'stock'#股票,'properties': []}",
      'is_stockholder_of': "{'edge': 'is_stockholder_of'#是股东,'start_tag':'stockholder'#股东,'end_tag':'stock'#股票, 'properties': ['shareholding_ratio'#持股比例,'circulation_shareholding_ratio'#流通持股比例]}",
      'belong_to': "{'edge': 'belong_to'#属于, 'start_tag':'stock'#股票,'end_tag':'trade'#行业,'properties': []}",
      'hold': "{'edge': 'hold'#持有,'start_tag':'public_offering_fund'#基金,'end_tag':'stock'#股票, 'properties': ['position_ratio'#持仓比例,'position_amount'#持仓金额]}",
      'manage': "{'edge': 'manage'#管理,'start_tag':'fund_manager'#基金经理人,'end_tag':'public_offering_fund'#基金, 'properties': []}",
      'involve_stock': "{'edge': 'involve'#涉及股票, 'start_tag':'stock'#股票,'end_tag':'information'#资讯,'properties': ['tendency'#趋势]}",
      'involve_fund': "{'edge': 'involve'#涉及基金, 'start_tag':'public_offering_fund'#基金,'end_tag':'information'#资讯,'properties': ['tendency'#趋势]}",
      'associate': "{'edge': 'associate'#关联, 'start_tag':'stock'#股票,'end_tag':'industry'#产业,'properties': []}",
      'affect': "{'edge': 'affect'#影响, 'start_tag':'industry'#（上游）产业,'end_tag':'industry'#（下游）产业,'properties': []}",
      'has_stock_data': "{'edge': 'has_stock_data'#有股票数据, 'start_tag':'stock'#股票,'end_tag':'stock_data'#股票数据,'properties': []}",
      'has_stock_financial_information': "{'edge': 'has_stock_financial_information'#有股票财务信息,'start_tag':'stock'#股票,'end_tag':'stock_financial_information'#股票财务信息, 'properties': []}",
      'has_trade_data': "{'edge': 'has_trade_data'#有行业数据,'start_tag':'trade'#行业,'end_tag':'trade_data'#行业数据, 'properties': []}"
  }
}

def judge_edge(key, value):
    """
    对 involve edge 的特殊处理
    :param key:
    :param value:
    :return:
    """
    if key == 'involve':
        if 'public_offering_fund' in value:
            return 'involve_fund'
        else:
            return 'involve_stock'
    else:
        return key

def schema_relative(example):
    """
    提取 related schema
    :param example:
    :return:
    """
    tags = example['tags']
    edges_ = example['edges']
    edges = []
    for key, _ in edges_.items():
        edges.append(judge_edge(key, tags))

    #排序
    tags = sorted(tags)
    edges= sorted(edges)
    
    schema = '"Node_properties": {'
    for t in tags:
        schema += str(schema_dic['Node_properties'][t])
        schema += ','
    if schema[-1] == ',':
        schema = schema[:-1]
    schema += '}, "Edge_properties": {'   
    for e in edges:
        schema += str(schema_dic['Edge_properties'][e])
        schema += ','
    if schema[-1] == ',':
        schema = schema[:-1]
    schema += '}'
    return schema

def schema_relative_chinese(example):
    """
    提取 related schema chinese
    :param example:
    :return:
    """
    tags = example['tags']
    edges_ = example['edges']
    
    tags = sorted(tags)
    edges= sorted(edges)
    
    edges = []
    for key, value in edges_.items():
        edges.append(judge_edge(key, value))
    
    schema = '"Node_properties": {'
    for t in tags:
        schema += str(schema_chinese_dic['Node_properties'][t])
        schema += ','
    if schema[-1] == ',':
        schema = schema[:-1]
    schema += '}, "Edge_properties": {'   
    for e in edges:
        schema += str(schema_chinese_dic['Edge_properties'][e])
        schema += ','
    if schema[-1] == ',':
        schema = schema[:-1]
    schema += '}'
    return schema

def preprocess(tokenizer, config, example, max_seq_length, prompt_key, target_key, prompt_type):
    """
    为输入的句子提取prompt模板并填充，
    使用tokenizer对输入进行分词
    :param tokenizer:
    :param config:
    :param example:
    :param max_seq_length:
    :param prompt_key:
    :param target_key:
    :param prompt_type:
    :return:
        字典: {"input_ids": input_ids, "seq_len": len(prompt_ids)}
    """
    if prompt_type == 'original':
        prompt = prompt_str % example[prompt_key]
    elif prompt_type == 'original_schema':
        prompt = prompt_str_new % (schema_str, example[prompt_key])
    elif prompt_type == 'original_schema_relative':
        schema = schema_relative(example)
        prompt = prompt_str_new % (schema, example[prompt_key])
    elif prompt_type == 'original_schema_relative_chinese':
        schema = schema_relative_chinese(example)
        prompt = prompt_str_new % (schema, example[prompt_key])
    
    # print(prompt)
    # exit()
    
    target = example[target_key]
    prompt_ids = tokenizer.encode(prompt, max_length=max_seq_length, truncation=True)
    target_ids = tokenizer.encode(target, max_length=max_seq_length, truncation=True, add_special_tokens=False)
    # 最终还是将instruction的输入输出都拼在一起，使用经典的causal-LM的next word prediction方式来训练
    input_ids = prompt_ids + target_ids + [config.eos_token_id]
    return {"input_ids": input_ids, "seq_len": len(prompt_ids)}


def read_jsonl(path, max_seq_length, prompt_key, target_key, prompt_type, skip_overlength=False):
    """
    主方法：入参均为argparse中定义的参数
    :param path:
    :param max_seq_length:
    :param prompt_key:
    :param target_key:
    :param prompt_type:
    :param skip_overlength:
    :return:
        None
    """
    if 'llama' in model_checkpoint.lower() or 'alpaca' in model_checkpoint.lower():
        tokenizer = LlamaTokenizer.from_pretrained(
            model_checkpoint, trust_remote_code=True)
    else:
        tokenizer = AutoTokenizer.from_pretrained(
            model_checkpoint, trust_remote_code=True)
    config = transformers.AutoConfig.from_pretrained(
        model_checkpoint, trust_remote_code=True, device_map='auto')
    print("read data")
    with open(path, "r",encoding="utf8") as f:
        print("process data=================================")
        for example in tqdm(json.load(f)):
            # example = json.loads(line)
            feature = preprocess(tokenizer, config, example, max_seq_length, prompt_key, target_key, prompt_type)
            if skip_overlength and len(feature["input_ids"]) > max_seq_length:
                continue
            feature["input_ids"] = feature["input_ids"][:max_seq_length]
            yield feature


# 输入文件统一放在 data 文件夹下
# 输出文件统一放在 data/tokenized_data 文件夹下
input_file_path = f'{args.input_file}'
save_path = f"data/tokenized_data/{args.save_name}"
print("save_path",save_path)



if os.path.exists(save_path):
    # 删除文件夹及其内容
    shutil.rmtree(save_path)

# 清除huggingface的缓存
cache_dir = os.path.expanduser("/home/yuanyuan/.cache/huggingface/datasets")
# 如果缓存目录存在，递归删除它
if os.path.exists(cache_dir):
    shutil.rmtree(cache_dir)

    
dataset = datasets.Dataset.from_generator(
    lambda: read_jsonl(input_file_path, args.max_seq_length, args.prompt_key, args.target_key, args.prompt_type, args.skip_overlength)
)
dataset.save_to_disk(save_path)


