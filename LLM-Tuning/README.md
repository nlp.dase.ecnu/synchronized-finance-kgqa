# LLM-Tuning

## Preparation

### System configuration

Before reproducing this repository, please check whether your environment configuration is up to standard.
* python >= 3.11
* NVIDIA driver >= 545.23.06

### Dependency

To initialize the LLM system, you should install all dependencies by running
```
pip install -r requirements.txt
```

### Download model from huggingface

The backbone used by this system is chatglm3-6b, you can down load the weight files from [this page](https://huggingface.co/THUDM/chatglm3-6b).

After downloading all files, Create a folder named 'chatglm3-6b' in the root directory of the project and place the downloaded content inside like this:
```
├─chatglm3-6b
│  ├─<downloaded content>
├─finance_kgqa
│  ├─finance_KGQA
│  ├─kgqa
│  └─templates
└─LLM-Tuning
    ├─data
    ├─runs
    ├─server
    ├─weight
    └─<This README.md file>
```

## Getting start

### Fine-Tuning text-to-cql models with financial questions

You can fine-tune your models with the dataset we provide by running
```
sh run_all_chatglm3_schema.sh
```

### Set the path of your LoRA checkpoint

Modify the line 9 file server/http_server.py:
```
7  device = torch.device(0)
8  model_name_or_path = "../../chatglm3-6b"
9  weight_path = "../weight/train_0524_original_schema_relative/checkpoint-3200" # replace this path with your weight's path
```

### Launch the LLM backend

Run the following command. If you see the hint of 'Starting server on port 8008...', the LLM backend is successfully running.
```
cd server
python http_server.py
```

Now you've launched the LLM system, you can jump to ../finance_kgqa to see the next installation step.