CUDA_VISIBLE_DEVICES=0 python tokenize_dataset_rows.py \
    --model_path ../chatglm3-6b \
    --input_file ./data/datasets.json  \
    --prompt_key q \
    --target_key a \
    --save_name train_0524_original_schema_relative \
    --max_seq_length 3000 \
    --skip_overlength False \
    --prompt_type original_schema_relative


CUDA_VISIBLE_DEVICES=0  python chatglm3_lora_tuning.py \
    --model_path ../chatglm3-6b \
    --tokenized_dataset train_0524_original_schema_relative \
    --lora_rank 8 \
    --per_device_train_batch_size 10 \
    --gradient_accumulation_steps 1 \
    --max_steps 3200 \
    --save_steps 100 \
    --save_total_limit 400 \
    --learning_rate 1e-4 \
    --fp16 \
    --remove_unused_columns false \
    --logging_steps 50 \
    --output_dir weight/train_0524_original_schema_relative

