from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import torch
from peft import PeftModel
from transformers import AutoTokenizer, AutoModel

device = torch.device(0)
model_name_or_path = "../../chatglm3-6b"
weight_path = "../weight/train_0524_original_schema_relative/checkpoint-3200"
# 加载原始 LLM
tokenizer = AutoTokenizer.from_pretrained(model_name_or_path, trust_remote_code=True)
model = AutoModel.from_pretrained(model_name_or_path, trust_remote_code=True).half()
# 给原始 LLM 安装上你的 LoRA tool
model = PeftModel.from_pretrained(model, weight_path).half().to(device)


def model_output(model, tokenizer, test_text, device):
    """
    调用 model 的 generate 方法对输入句子进行预测
    :param model: 调用的 model
    :param tokenizer: 匹配的分词器
    :param test_text: 输入query
    :param device: gpu-id
    :return:
        res: 模型输出
    """
    inputs = tokenizer(test_text, return_tensors="pt")
    inputs = inputs.to(device)
    pred = model.generate(**inputs, max_new_tokens=2000, repetition_penalty=1.1)
    res = tokenizer.decode(pred.cpu()[0], skip_special_tokens=True).replace(test_text, "").replace("[gMASK]sop  ", "")
    return res


class MyRequestHandler(BaseHTTPRequestHandler):
    """Handle HTTP POST requests."""
    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)

        # Parse JSON data
        try:
            data = json.loads(post_data.decode('utf-8'))
            query = data.get('query', '')
            print("query:",query)
   
            answer = model_output(model, tokenizer, query, device).lstrip()
            print("answer:",answer)
            # Process data (you can perform any desired operations here)
            response_data = {'result': answer}
            response_json = json.dumps(response_data).encode('utf-8')
            
            # Send response
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.send_header('Content-length', len(response_json))
            self.end_headers()
            self.wfile.write(response_json)
        except json.JSONDecodeError as e:
            self.send_error(400, f'Error decoding JSON: {e}')
            return


def run_server(port=8008):
    """Run the server."""
    server_address = ('', port)
    httpd = HTTPServer(server_address, MyRequestHandler)
    print(f'Starting server on port {port}...')
    httpd.serve_forever()


if __name__ == '__main__':
    run_server()
